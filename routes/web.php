<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::group(['middleware'=>['auth','1']],function(){

Route::get('/admin/home', 'HomeController@index');
Route::get('/admin/utilisateur', 'UserController@index');
Route::post('/admin/ajoutUtilisateur','UserController@store');
Route::get('/admin/modification-utilisateur/{id}', 'UserController@update');
Route::put('admin/update-utilisateur-saving/{id}', 'UserController@updateSaving');
Route::post('/admin/deleteUser/{id}', 'UserController@delete');
Route::post('/admin/activerUser/{id}', 'UserController@activer');
Route::get('/admin/details-utilisateur/{id}', 'UserController@details');


//carte

Route::get('/admin/cartographie', function () {
    return view('admin.carte');
});



//declaration
Route::get('/admin/declaration', 'DeclarationController@index');
Route::post('/admin/ajoutDeclaration','DeclarationController@store');
Route::get('/admin/modification-declaration/{id}', 'DeclarationController@update');
Route::put('admin/update-declaration-saving/{id}', 'DeclarationController@updateSaving');
Route::get('/admin/details-declaration/{id}', 'DeclarationController@details');
});

Route::get('admin/choixreg/{reg}', 'ChoixController@choixReg');
Route::get('admin/choixdept/{dept}', 'ChoixController@choixDept');
Route::get('admin/choixcomm/{comm}', 'ChoixController@choixComm');



Route::group(['middleware'=>['auth','3']],function(){

Route::get('/declarant/declaration', 'DeclarationController@index');
Route::post('/declarant/ajoutDeclaration','DeclarationController@storeDeclarant');
Route::get('/declarant/details-declaration/{id}', 'DeclarationController@details');
Route::get('/declarant/modification-declaration/{id}', 'DeclarationController@update');
Route::put('declarant/update-declaration-saving/{id}', 'DeclarationController@updateSavingDeclarant');


});


Route::group(['middleware'=>['auth','4']],function(){

	Route::get('/agent/declaration', 'DeclarationController@index');
	
	Route::get('/agent/details-declaration/{id}', 'DeclarationController@details');
	Route::get('/agent/modification-declaration/{id}', 'DeclarationController@update');
	Route::put('agent/update-declaration-saving/{id}', 'DeclarationController@updateSavingAgent');


});

Route::group(['middleware'=>['auth','5']],function(){

	Route::get('/officier/declaration', 'DeclarationController@index');
	
	Route::get('/officier/details-declaration/{id}', 'DeclarationController@details');
	Route::get('/officier/modification-declaration/{id}', 'DeclarationController@update');
	Route::put('officier/update-declaration-saving/{id}', 'DeclarationController@updateSavingOfficier');

	Route::get('/officier/pdf/{id}', 'DeclarationController@pdf');


});



Route::get('/region', 'RegionController@index')->name('region.index');
Route::get('/region/create', 'RegionController@create')->name('region.create');
Route::post('/region/store', 'RegionController@store')->name('region.store');
Route::get('/region/{region}', 'RegionController@show')->name('region.show');
Route::get('/region/edit/{region}', 'RegionController@edit')->name('region.edit');
Route::put('/region/update/{region}', 'RegionController@update')->name('region.update');
Route::get('/region/delete/{region}','RegionController@destroy')->name('region.delete');

//locatité
Route::get('/mairie', 'MairieController@index')->name('mairie.index');
Route::get('/mairie/create', 'MairieController@create')->name('mairie.create');
Route::post('/mairie/store', 'MairieController@store')->name('mairie.store');
Route::get('/mairie/{mairie}', 'MairieController@show')->name('mairie.show');
Route::get('/mairie/edit/{mairie}', 'MairieController@edit')->name('mairie.edit');
Route::put('/mairie/update/{mairie}', 'MairieController@update')->name('mairie.update');
Route::get('/mairie/delete/{mairie}','MairieController@destroy')->name('mairie.delete');

//enfants
Route::get('/children', 'ChildrenController@index')->name('children.index');
Route::get('/children/create', 'ChildrenController@create')->name('children.create');
Route::post('/children/store', 'ChildrenController@store')->name('children.store');
Route::get('/children/{children}', 'ChildrenController@show')->name('children.show');
Route::get('/children/edit/{children}', 'ChildrenController@edit')->name('children.edit');
Route::put('/children/update/{children}', 'ChildrenController@update')->name('children.update');
Route::get('/children/delete/{children}','ChildrenController@destroy')->name('children.delete');



Auth::routes();


