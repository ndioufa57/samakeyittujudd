@extends('layouts.master')

@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Modification déclaration'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach
                        @endif
                        <div class="card-title mb-3">Modification  déclaration </div>

                        <form method="POST" action="/declarant/update-declaration-saving/{{ $declaration->id }}" enctype="multipart/form-data" >

                            @csrf
                            @method('put')
                            
                           
                            <div class="form-group row">

                                    <div class="col-md-6">
                                        <label for="mairie" class="col-form-label">Genre</label>
                                        <select name="genre" class="form-control ">

                                           <option value="Homme" <?php if ($declaration->genre=='Homme') { echo "selected"; } ?>>Homme</option>
                                           <option value="Femme" <?php if ($declaration->genre=='Femme') { echo "selected"; } ?>>Femme</option>
                                        </select>
                                    </div>


                                    <div class="col-md-6 ">
                                        <label class="col-form-label">Téphone Mère</label>
                                        <input type="number" name="telephone_mere" class="form-control" value="{{$declaration->telephone_mere}}"/>
                                    </div>
                            </div>

                              
                              <div class="form-group row">
                                    
                                    <div class=" col-md-6 ">
                                      <label for="" class="col-form-label" >Region</label>
                                      <select class="form-control" name="region" id="region">
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_r}}" >  Région actuelle : {{$dataLoc->nom_r}}</option>
                                        @endforeach
                                          <?php use App\Models\Region; $reg=Region::all(); ?>



                                          @foreach($reg as $dataReg)
                                            <option value="{{$dataReg->id}}">
                                              {{$dataReg->nom}}
                                            </option>
                                          @endforeach
                                      </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Departement</label>
                                      <select class="form-control" name="departement" id="departement" >
                                         
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_d}}">  Departement actuel : {{$dataLoc->nom_d}}</option>
                                        @endforeach
                                    </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Commune</label>
                                      <select class="form-control" name="commune" id="commune" >
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_c}}">  Commune actuelle : {{$dataLoc->nom_c}} </option>
                                        @endforeach
                                      </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Localité</label>
                                      <select class="form-control" name="localite" id="localite" >
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_l}}">  Localité actuelle : {{$dataLoc->nom_l}} </option>
                                        @endforeach
                                    </select>
                                    </div>
                                    
                                    
                            </div>
                              
                              
                            
                            <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Hopital </label>
                                        <input type="text" name="hopital" class="form-control" value="{{$declaration->hopital}}"/>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Prenom du père </label>
                                        <input type="text" name="prenom_pere" class="form-control" value="{{$declaration->prenom_pere}}"/>
                                    </div>

                                    
                                    
                                    
                            </div>
                            <div class="form-group row">
                                    
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Nom du père </label>
                                        <input type="text" name="nom_pere" class="form-control" value="{{$declaration->nom_pere}}"/>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Prénom de la mère </label>
                                        <input type="text" name="prenom_mere" class="form-control" value="{{$declaration->prenom_mere}}"/>
                                    </div>

                                    
                                    
                                    
                            </div>
                            <div class="form-group row">
                                    
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Nom de la mère </label>
                                        <input type="text" name="nom_mere" class="form-control" value="{{$declaration->nom_mere}}"/>
                                    </div>

                                   
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="certificat">Certificat d'accouchement</label>
                                        <input name="certificat" type="file" class="form-control form-control-rounded" id="certificat" placeholder="Web address">
                                    </div>
                                    
                                    
                            </div>
                            <div class="form-group row">
                                    

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_mere">CNI Mère</label>
                                        <input type="number" name="cni_mere" class="form-control" value="{{$declaration->cni_mere}}"/>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_pere">CNI Père</label>
                                        <input type="number" name="cni_pere" class="form-control" value="{{$declaration->cni_pere}}"/>
                                    </div>
                                    
                                    
                            </div>
                            
                            

                            <a href="/declarant/declaration" class="btn btn-danger" data-dismiss="modal">retour à la liste</a>
                            <button type="submit" class="btn btn-primary">Enregistrer</button>

                           

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/acat.js')}}"></script>
    <script src="{{asset('assets/js/choix.js')}}"></script>
@endsection