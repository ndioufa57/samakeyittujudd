
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="" style="background-color: #969696;">
  <div class="wrapper ">
    
    <div class="main-panel  " style=" padding-right: 200px;">
      
      <!-- Navbar -->
      
      <!-- End Navbar -->

     <div class="content" >



  <div class="container-fluid">
   
    <div class="row">

      <div class="col-md-12 ">
        <center style="margin-left: 250PX"> 
          <br><br><br>
        <u style="color: white;" style="margin-left: 300px" ><h1 style="text-align: center; color: white; text-transform: uppercase;">Aperçu du document </h1></u>

        
          <button class="btn btn-primary" id="download" type="submit"> TELECHARGER  </button>

          <br><br>

        </center> 
<form method="post" action="/admin/ajoutModifEtudeProjet/" autocomplete="off"  class="form-horizontal">
            @csrf

            <center style="margin-left: 250PX"  >
            <div class="card " id="testpdf"  style="border: none;">
              <div class="card-header card-header-primary">
                <h4 class="card-title"></h4>
                <!-- <div class="d-inline-flex justify-content-end position-fixed  col-md-9">
                <button class="btn btn-rounded btn-success" type="submit"><i class="material-icons">save</i></button></div> -->
            </div>

              <div class="card-body">


                <form method="POST" action="/officier/update-declaration-saving/{{ $declaration->id }}" enctype="multipart/form-data" id="testpdf" >

                      @csrf
                      @method('put')
                      
                      <div class="form-group row 2 ">
                              <div class="col-md-6 " id="prenom">
                                  <label for="" class="col-form-label">Registre</label>
                                  
                                  <div  class="form-control">{{$declaration->registre}} </div>
                              </div>
                              <div class="col-md-6">
                                  <label for="" class="col-form-label">Prénom</label>
                                  
                                  <div  class="form-control">{{$declaration->prenom}} </div>
                              </div>
                      </div>
                      <div class="form-group row">

                              
                              <div class="col-md-6">
                                  <label for="" class="col-form-label">Nom</label>
                                  <div  class="form-control"> {{$declaration->nom}} </div>
                              </div>

                              <div class="col-md-6">
                                  <label for="mairie" class="col-form-label">Genre</label>
                                  

                                    @if($declaration->genre=='Homme')
                                      <div  class="form-control"> Homme </div>
                                    @endif

                                    @if($declaration->genre=='Femme')
                                      <div  class="form-control"> Femme </div>
                                    @endif

                                     
                              </div>
                      </div>

                        
                        <div class="form-group row">
                              
                              <div class=" col-md-6 ">
                                <label for="" class="col-form-label" >Region</label>

                                <?php $region1=DB::table('ml_region')->where('id', $declaration->region)->first(); ?>
                                      <div  class="form-control">{{  $region1->nom }}</div>
                                
                              </div>

                              <div class="col-md-6">
                                <label for="" class="col-form-label">Departement</label>
                                <?php $departement1=DB::table('ml_departement')->where('id', $declaration->departement)->first(); ?>
                                      <div  class="form-control">{{  $departement1->nom }}</div>
                              </div>

                              <div class="col-md-6">
                                <label for="" class="col-form-label">Commune</label>
                                <?php $commune1=DB::table('ml_commune')->where('id', $declaration->commune)->first(); ?>
                                      <div  class="form-control">{{  $commune1->nom }}</div>
                              </div>

                              <div class="col-md-6">
                                <label for="" class="col-form-label">Localité</label>
                                <?php $localite1=DB::table('ml_localite')->where('id', $declaration->localite)->first(); ?>
                                      <div  class="form-control">{{  $localite1->nom }}</div>
                              </div>
                              
                              
                      </div>
                        
                        
                      
                      <div class="form-group row">
                              <div class="col-md-6">
                                  <label for="" class="col-form-label">Hopital </label>
                                  
                                  <div  class="form-control">{{$declaration->hopital}}</div>
                              </div>

                              <div class="col-md-6">
                                  <label for="" class="col-form-label">Prenom du père </label>
                                  
                                   <div  class="form-control">{{$declaration->prenom_pere}}</div>
                              </div>

                              
                              
                              
                      </div>
                      <div class="form-group row">
                              
                              <div class="col-md-6">
                                  <label for="" class="col-form-label">Nom du père </label>
                                 
                                  <div  class="form-control">{{$declaration->nom_pere}}</div>
                              </div>

                              <div class="col-md-6">
                                  <label for="" class="col-form-label">Prénom de la mère </label>
                                  
                                  <div  class="form-control">{{$declaration->prenom_mere}}</div>
                              </div>

                              
                              
                              
                      </div>
                      <div class="form-group row">
                              
                              <div class="col-md-6">
                                  <label for="" class="col-form-label">Nom de la mère </label>
                                  
                                  <div  class="form-control">{{$declaration->nom_mere}}</div>

                              </div>

                              
                              
                              <div class="col-md-6 form-group mb-3">
                                  <label for="cni_mere">CNI Mère</label>
                                  

                                  <div  class="form-control">{{$declaration->cni_mere}}</div>
                              </div>
                              
                      </div>
                      <div class="form-group row">
                              

                              

                              <div class="col-md-6 form-group mb-3">
                                  <label for="cni_pere">CNI Père</label>
                                  
                                  <div  class="form-control">{{$declaration->cni_pere}}</div>
                              </div>


                              <div class="col-md-6 form-group mb-3">
                                  <label for="cni_mere">Téphone Mère</label>
                                  

                                  <div  class="form-control">{{$declaration->telephone_mere}}</div>
                              </div>
                              
                              
                      </div>
                      <div class="form-group row">


                              <div class="col-md-6 form-group mb-3">
                                  <label for="cni_mere">District</label>
                                  
                                  <div  class="form-control">{{$declaration->district}}</div>
                              </div>

                              <div class="col-md-6 form-group mb-3">
                                  <label for="cni_mere">Date de délivrance</label>
                                  
                                  <div  class="form-control">{{$declaration->delivrance}}</div>
                              </div>
                              

                              
                              
                              
                      </div>


                      
                  </form>
               

                  

                  
              
              </div>


              </div>
              <center>

            <!-- <p  href="/admin/financement" class="btn btn-danger" data-dismiss="modal">retour à la liste</p>
 -->                <button type="submit" class="btn btn-primary ml-3" style="display: none;">Enregistrer</button>
                  <?php if(isset($pdf->id)){$identifiant=$pdf->id+1;}else {$identifiant=1;} ?>

                
                
            
            </form>

           



            

            

</div>
      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            
          </nav>
         <!--  <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">MLouma</a> 
          </div> -->
        </div>
      </footer>
    </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
     <script >
       window.onload = function () {
    document.getElementById("download")
        .addEventListener("click", () => {
            const invoice = this.document.getElementById("testpdf");
            console.log(invoice);
            console.log(window);
            var opt = {
                margin: 1,
                filename: 'docskj.pdf',
                image: { type: 'jpeg', quality: 0.98 },
                html2canvas: { scale: 3 ,dpi: 300, letterRendering:true },
                jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' },

                // image:        { type: 'jpeg', quality: 1 },
                //html2canvas:  { dpi: 300, letterRendering: true, width: 10000}
                // jsPDF:        { unit: 'mm', format: 'a4', orientation: 'portrait' },
                // pagebreak:    { mode: ['avoid-all', 'css'] }


            };
            html2pdf().from(invoice).set(opt).save();
        })
}
//alert("fsddfsdf");
     </script>


  @yield('scripts')
</body>

</html>