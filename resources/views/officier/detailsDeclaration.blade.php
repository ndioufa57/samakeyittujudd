@extends('layouts.master')

@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Modification déclaration'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach
                        @endif
                        <div class="card-title mb-3">Détails  déclaration </div>


                        
                        <form method="POST" action="/officier/update-declaration-saving/{{ $declaration->id }}" enctype="multipart/form-data" id="testpdf" >

                            @csrf
                            @method('put')
                            
                            <div class="form-group row 2 ">
                                    <div class="col-md-6 " id="prenom">
                                        <label for="" class="col-form-label">Registre</label>
                                        <input type="text" name="registre" class="form-control" value="{{$declaration->registre}}"  disabled />
                                    </div>
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Prénom</label>
                                        <input type="text" name="prenom" class="form-control" value="{{$declaration->prenom}}" disabled/>
                                    </div>
                            </div>
                            <div class="form-group row">

                                    
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Nom</label>
                                        <input type="text" name="nom" class="form-control" value="{{$declaration->nom}}" disabled/>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="mairie" class="col-form-label">Genre</label>
                                        <select name="genre" class="form-control " disabled>

                                           <option value="Homme" <?php if ($declaration->genre=='Homme') { echo "selected"; } ?>>Homme</option>
                                           <option value="Femme" <?php if ($declaration->genre=='Femme') { echo "selected"; } ?>>Femme</option>
                                        </select>
                                    </div>
                            </div>

                              
                              <div class="form-group row">
                                    
                                    <div class=" col-md-6 ">
                                      <label for="" class="col-form-label" >Region</label>
                                      <select class="form-control" name="region" id="region" disabled>
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_r}}" >  Région actuelle : {{$dataLoc->nom_r}}</option>
                                        @endforeach
                                          <?php use App\Models\Region; $reg=Region::all(); ?>



                                          @foreach($reg as $dataReg)
                                            <option value="{{$dataReg->id}}">
                                              {{$dataReg->nom}}
                                            </option>
                                          @endforeach
                                      </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Departement</label>
                                      <select class="form-control" name="departement" id="departement" disabled>
                                         
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_d}}">  Departement actuel : {{$dataLoc->nom_d}}</option>
                                        @endforeach
                                    </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Commune</label>
                                      <select class="form-control" name="commune" id="commune" disabled>
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_c}}">  Commune actuelle : {{$dataLoc->nom_c}} </option>
                                        @endforeach
                                      </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Localité</label>
                                      <select class="form-control" name="localite" id="localite" disabled>
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_l}}">  Localité actuelle : {{$dataLoc->nom_l}} </option>
                                        @endforeach
                                    </select>
                                    </div>
                                    
                                    
                            </div>
                              
                              
                            
                            <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Hopital </label>
                                        <input type="text" name="hopital" class="form-control" value="{{$declaration->hopital}}" disabled/>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Prenom du père </label>
                                        <input type="text" name="prenom_pere" class="form-control" value="{{$declaration->prenom_pere}}" disabled/>
                                    </div>

                                    
                                    
                                    
                            </div>
                            <div class="form-group row">
                                    
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Nom du père </label>
                                        <input type="text" name="nom_pere" class="form-control" value="{{$declaration->nom_pere}}" disabled/>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Prénom de la mère </label>
                                        <input type="text" name="prenom_mere" class="form-control" value="{{$declaration->prenom_mere}}" disabled/>
                                    </div>

                                    
                                    
                                    
                            </div>
                            <div class="form-group row">
                                    
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Nom de la mère </label>
                                        <input type="text" name="nom_mere" class="form-control" value="{{$declaration->nom_mere}}" disabled/>
                                    </div>

                                    
                                    
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_mere">CNI Mère</label>
                                        <input type="number" name="cni_mere" class="form-control" value="{{$declaration->cni_mere}}" disabled/>
                                    </div>
                                    
                            </div>
                            <div class="form-group row">
                                    

                                    

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_pere">CNI Père</label>
                                        <input type="number" name="cni_pere" class="form-control" value="{{$declaration->cni_pere}}" disabled/>
                                    </div>


                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_mere">Téphone Mère</label>
                                        <input type="number" name="telephone_mere" class="form-control" value="{{$declaration->telephone_mere}}" disabled/>
                                    </div>
                                    
                                    
                            </div>
                            <div class="form-group row">


                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_mere">District</label>
                                        <input type="text" name="district" class="form-control" value="{{$declaration->district}}" disabled />
                                    </div>
                                    

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_pere">Statut</label>
                                        <select name="statut" class="form-control" disabled>
                                          <option {{ $declaration->statut == 0 ? 'selected' : '' }} value="0">En traitement</option>
                                          <option {{ $declaration->statut == 1 ? 'selected' : '' }} value="1">Validé (Agent)</option>
                                          <option {{ $declaration->statut == 2 ? 'selected' : '' }} value="2">Validé (Officier d'état civil)</option>
                                        </select>
                                    </div>
                                    
                                    
                            </div>

                            <div class="form-group row">


                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_mere">Date de délivrance</label>
                                        <input type="text" name="district" class="form-control" value="{{$declaration->delivrance}}" disabled />
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_pere">Certificat d'accouchement</label>

                                        <?php if ( !empty($declaration->certificat)) {?>

                                            <br>

                                            <img src="{{asset('assets/images/certificats/'.$declaration->certificat) }}" width="400" height="450">

                                       <?php }

                                        else{?>

                                            

                                            <div class="alert alert-primary" role="alert">
                                              Le certificat de naissance n'as pas enore été ajouté
                                            </div>

                                        <?php }

                                        ?>
                                        
                                        
                                    </div>
                                    
                                    
                            </div>

                            <a href="/officier/declaration" class="btn btn-danger" data-dismiss="modal">retour à la liste</a>

                            
                        </form>

                        <div id="editor"></div>

                    </div>
                </div>
            </div>

        </div>
    </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
     <script >
       window.onload = function () {
    document.getElementById("download")
        .addEventListener("click", () => {
            const invoice = this.document.getElementById("testpdf");
            console.log(invoice);
            console.log(window);
            var opt = {
                margin: 1,
                filename: 'docskj.pdf',
                image: { type: 'jpeg', quality: 0.98 },
                html2canvas: { scale: 3 ,dpi: 300, letterRendering:true },
                jsPDF: { unit: 'mm', format: 'a3', orientation: 'portrait' },

                // image:        { type: 'jpeg', quality: 1 },
                //html2canvas:  { dpi: 300, letterRendering: true, width: 10000}
                // jsPDF:        { unit: 'mm', format: 'a4', orientation: 'portrait' },
                // pagebreak:    { mode: ['avoid-all', 'css'] }


            };
            html2pdf().from(invoice).set(opt).save();
        })
}
//alert("fsddfsdf");
     </script>

<script >

</script>

    
@endsection
@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/acat.js')}}"></script>
    <script src="{{asset('assets/js/choix.js')}}"></script>


    
@endsection