<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
           

                <li class="nav-item " >
                    <a class="nav-item-hold" href="/admin/home">
                        <i class="nav-icon i-Home1"></i>
                        <span class="nav-text">Dashboard</span>
                    </a>
                    <div class="triangle"></div>
                </li>
           
                <li class="nav-item " >
                    <a class="nav-item-hold" href="/admin/utilisateur">
                        <i class="nav-icon i-Conference"></i>
                        <span class="nav-text">Utilisateur</span>
                    </a>
                    <div class="triangle"></div>
                </li>
           
            <li class="nav-item " >
                <a class="nav-item-hold" href="/admin/declaration">
                    <i class="nav-icon i-Baby"></i>
                    <span class="nav-text">Déclarer</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item " >
                <a class="nav-item-hold" href="/admin/cartographie">
                    <i class="nav-icon i-Map"></i>
                    <span class="nav-text">Région</span>
                </a>
                <div class="triangle"></div>
            </li>
            <!-- <li class="nav-item {{ request()->is('mairie*') ? 'active' : '' }}" data-item="mairie">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Building"></i>
                    <span class="nav-text">Mairie</span>
                </a>
                <div class="triangle"></div>
            </li> -->
        </ul>
    </div>

    <!-- <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        
        <ul class="childNav" data-parent="user">
            <li class="nav-item">
                <a class=""
                href="/admin/utilisateur">
                    <i class="nav-icon i-Book"></i>
                    <span class="item-name">Liste des utilisateurs</span>
                </a>
            </li>
            <li class="nav-item">
                <a class=""
                href="">
                    <i class="nav-icon i-Add"></i>
                    <span class="item-name">Ajouter un utilisateur</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="children">
            <li class="nav-item">
                <a class=""
                href="">
                    <i class="nav-icon i-Book"></i>
                    <span class="item-name">Liste des enfants</span>
                </a>
            </li>
            <li class="nav-item">
                <a class=""
                href="{{route('children.create')}}">
                    <i class="nav-icon i-Add"></i>
                    <span class="item-name">Ajouter une enfant</span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="region">
            <li class="nav-item">
                <a class=""
                href="{{route("region.index")}}">
                    <i class="nav-icon i-Book"></i>
                    <span class="item-name">Liste des régions</span>
                </a>
            </li>
            <li class="nav-item">
                <a class=""
                href="{{route('region.create')}}">
                    <i class="nav-icon i-Add"></i>
                    <span class="item-name">Ajouter une région</span>
                </a>
            </li>
        </ul>

        <ul class="childNav" data-parent="mairie">
            <li class="nav-item">
                <a class=""
                href="{{route("mairie.index")}}">
                    <i class="nav-icon i-Book"></i>
                    <span class="item-name">Liste des mairies</span>
                </a>
            </li>
            <li class="nav-item">
                <a class=""
                href="{{route('mairie.create')}}">
                    <i class="nav-icon i-Add"></i>
                    <span class="item-name">Ajouter une mairie</span>
                </a>
            </li>
        </ul>
    </div> -->
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->
