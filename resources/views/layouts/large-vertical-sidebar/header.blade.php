<div class="main-header">
    <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <div class="logo">
        <img src="{{asset('assets/images/logo.jpg')}}" alt="">
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="d-flex align-items-center">

        <!-- / Mega menu -->
        {{--  <div class="search-bar">
            <input type="text" placeholder="Search">
            <i class="search-icon text-muted i-Magnifi-Glass1"></i>
        </div>  --}}
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">

        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div class="user col align-self-end">
               
                <img src="{{asset('assets/images/faces/'.Auth::user()->photo)}}" id="userDropdown" alt="" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    
                    {{--  <a class="dropdown-item">Profil</a>  --}}
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Déconnexion</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                            style="display: none;">
                                    @csrf
                                </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- header top menu end -->
