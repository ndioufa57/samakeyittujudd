@extends('layouts.master')

@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Modification déclaration'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach
                        @endif
                        <div class="card-title mb-3">Modification  déclaration </div>

                        <form method="POST" action="/agent/update-declaration-saving/{{ $declaration->id }}" enctype="multipart/form-data" >

                            @csrf
                            @method('put')
                            
                            <div class="form-group row 2 ">
                                    <div class="col-md-6 " id="prenom">
                                        <label for="" class="col-form-label">Registre</label>
                                        <input type="text" name="registre" class="form-control" value="{{$declaration->registre}}" />
                                    </div>
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Prénom</label>
                                        <input type="text" name="prenom" class="form-control" value="{{$declaration->prenom}}"/>
                                    </div>
                            </div>
                            <div class="form-group row">

                                    
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Nom</label>
                                        <input type="text" name="nom" class="form-control" value="{{$declaration->nom}}"/>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Date de délivrance</label>
                                        <input type="date" name="delivrance" class="form-control" value="{{$declaration->delivrance}}"/>
                                    </div>

                                    
                            </div>

                            
                            
                            
                            <div class="form-group row">
                                    

                                <div class="col-md-6 form-group mb-3">
                                    <label for="cni_mere">District</label>
                                    <input type="text" name="district" class="form-control" value="{{$declaration->district}}"/>
                                </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="cni_pere">Statut</label>
                                        <select name="statut" class="form-control">
                                          <option {{ $declaration->statut == 0 ? 'selected' : '' }} value="0">En traitement</option>
                                          <option {{ $declaration->statut == 1 ? 'selected' : '' }} value="1">Validé </option>
                                        </select>
                                    </div>
                                    
                                    
                            </div>

                           

                            <a href="/agent/declaration" class="btn btn-danger" data-dismiss="modal">retour à la liste</a>
                            <button type="submit" class="btn btn-primary">Enregistrer</button>

                           

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/acat.js')}}"></script>
    <script src="{{asset('assets/js/choix.js')}}"></script>
@endsection