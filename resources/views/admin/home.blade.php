@extends('layouts.master')

@section('main-content')
    @include('admin.includes.breadcrumb',[
        'title' => 'Home'])

    <div class="row">

        <?php 


        ?>
        <!-- ICON BG -->
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                <div class="card-body text-center">
                    <i class="i-Baby"></i>
                    <div class="content">
                        <h3 class="text-muted mt-2 mb-0">Enfants</h3>
                        <p class="text-primary text-35 line-height-1 mb-2"> {{$nombreEnfants}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                <div class="card-body text-center">
                    <i class="i-Conference"></i>
                    <div class="content">
                        <h3 class="text-muted mt-2 mb-0">Utilisateur</h3>
                        <p class="text-primary text-35 line-height-1 mb-2"> {{$nombreUtilisateurs}} </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                <div class="card-body text-center">
                    <i class="i-Map"></i>
                    <div class="content">
                        <p class="text-muted mt-2 mb-0">Région</p>
                        <p class="text-primary text-24 line-height-1 mb-2"></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                <div class="card-body text-center">
                    <i class="i-Building"></i>
                    <div class="content">
                        <p class="text-muted mt-2 mb-0">Mairie</p>
                        <p class="text-primary text-24 line-height-1 mb-2"></p>
                    </div>
                </div>
            </div>
        </div> -->

    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card o-hidden mb-4">
                <div class="card-header">
                    <h3 class="w-50 float-left card-title m-0">Liste des utilisateurs</h3>
                    <div class="dropdown dropleft text-right w-50 float-right">
                                   <button class="btn bg-gray-100" type="button" id="dropdownMenuButton_table1"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="nav-icon i-Gear-2"></i>
                                </button>
                        <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_table1">
                            <a class="dropdown-item" href="">Ajouter</a>
                            <a class="dropdown-item" href="">Voir tout</a>
                        </div> -->
                    </div>

                </div>
                <div class="card-body">

                    <div class="table-responsive">

                        <table id="datatableid2" class="display table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Statut</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($admins->get() as $admin)
                                <tr>
                                    <th scope="row">{{ $admin->prenom .' '. $admin->name }}</th>
                                    <td>{{ $admin->email }}</td>
                                    <td>
                                        @if ($admin->active)
                                            <span class="badge badge-success">Active</span>
                                        @else
                                        <span class="badge badge-danger">Inactive</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="" class="text-success mr-2"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('modif-form-{{ $admin->id }}').submit();">
                                                    <i class="nav-icon i-Pen-2 font-weight-bold"></i>Modifier
                                                    <form id="modif-form-{{ $admin->id }}" action="{{ url('admin/modification-utilisateur/'.$admin->id)}}" method="get" style="display: none;">
                                                       
                                                    </form>
                                                </a>
                                         @if ($admin->active==0)
                                                    <a href="" class="text-warning mr-2"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('delete-form-{{ $admin->id }}').submit();">
                                                    <i class="nav-icon i-Close-Window font-weight-bold"></i>Activer
                                                    <form id="delete-form-{{ $admin->id }}" action="/admin/activerUser/{{$admin->id}}" onsubmit="return confirm('Confirmez-vous la désactivation');" method="post" style="display: none;">
                                                        @csrf @method('post')
                                                    </form>
                                                </a>
                                                @else
                                                    <a href="" class="text-danger mr-2"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('delete-form-{{ $admin->id }}').submit();">
                                                    <i class="nav-icon i-Close-Window font-weight-bold"></i>Désactiver
                                                    <form id="delete-form-{{ $admin->id }}" action="/admin/deleteUser/{{$admin->id}}" onsubmit="return confirm('Confirmez-vous la désactivation');" method="post" style="display: none;">
                                                        @csrf @method('post')
                                                    </form>
                                                </a>
                                                @endif
                                    </td>
                                </tr>
                                @break($loop->index == 2)
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card o-hidden mb-4">
                <div class="card-header">
                    <h3 class="w-50 float-left card-title m-0">Les enfants</h3>
                    <div class="dropdown dropleft text-right w-50 float-right">
                                   <button class="btn bg-gray-100" type="button" id="dropdownMenuButton_table1"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="nav-icon i-Gear-2"></i>
                                </button>
                        <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_table1">
                            <a class="dropdown-item" href="{{ route('children.create') }}">Ajouter</a>
                            <a class="dropdown-item" href="{{ route('children.index') }}">Voir tout</a>
                        </div> -->
                    </div>

                </div>

                <div class="card-body">

                    <div class="table-responsive">

                        <table id="datatableid" class="display table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Prenom etNom</th>
                                
                                <th>Lieu</th>
                                <th>Pére</th>
                                <th>Mére</th>

                               

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($declaration as $data)
                                <tr>
                                    <td>{{ $data->id}}</td>
                                    <td>{{ $data->prenom .' '. $data->nom }}</td>
                                   
                                    <td>
                                      <?php $localite=DB::table('ml_localite')->where('id', $data->localite)->first(); ?>
                                      {{  $localite->nom }}
                                    </td>
                                    <td>{{ $data->prenom_pere .' '. $data->nom_pere }}</td>
                                    <td>{{ $data->prenom_mere .' '. $data->nom_mere }}</td>

                                    
                                    <td>
                                        <a href="" class="text-warning mr-2"
                                            onclick="event.preventDefault();
                                            document.getElementById('details-form-{{ $data->id }}').submit();">
                                            <i class="icon-file-text"></i>Détails
                                            <form id="details-form-{{ $data->id }}" action="{{ url('admin/details-declaration/'.$data->id)}}" method="get" style="display: none;">
                                               
                                            </form>
                                        </a>
                                        <a href="" class="text-success mr-2"
                                            onclick="event.preventDefault();
                                            document.getElementById('modif-form-{{ $data->id }}').submit();">
                                            <i class="nav-icon i-Pen-2 font-weight-bold"></i>Modifier
                                            <form id="modif-form-{{ $data->id }}" action="{{ url('admin/modification-declaration/'.$data->id)}}" method="get" style="display: none;">
                                               
                                            </form>
                                        </a>
                                        <!-- <a href="{{ route('children.delete',$data) }}" class="text-danger mr-2">
                                            <i class="nav-icon i-Close-Window font-weight-bold"></i>Supprimer
                                        </a> -->
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection
@section('page-js')

<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/acat.js')}}"></script>
    <script src="{{asset('assets/js/choix.js')}}"></script>
    <script type="text/javascript">
    
    $(document).ready( function () {
      $('#datatableid2').DataTable({
        "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
          },
      });

    } );

    $(document).ready( function () {
      $('#datatableid').DataTable({
        "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
          },
      });

    } );

 
  </script>

@endsection