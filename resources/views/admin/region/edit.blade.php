@extends('layouts.master')

@section('main-content')
    @include('admin.includes.breadcrumb',[
        'title' => 'Les régions'
        ])
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach

                        @endif
                        <div class="card-title mb-3">Editer une région</div>
                        <form action="{{ route('region.update',$region) }}" method="POST">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-md-12 form-group mb-3">
                                    <label for="name">Nom de la région</label>
                                    <input  name="name" type="text" class="form-control form-control-rounded"
                                    id="name" value="{{ $region->name  }}">
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary">Modifier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
