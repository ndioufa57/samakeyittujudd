@extends('layouts.master')

@section('page-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection
@section('main-content')
    @include('admin.includes.breadcrumb',[
        'title' => 'Les enfants'])

        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach

                        @endif
                        <div class="card-title mb-3">Ajouter un enfant</div>
                        <form action="{{ route('children.update',$children) }}" method="POST" >
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-md-6 form-group mb-3">
                                    <label for="firstname">Prénom</label>
                                    <input value="{{ $children->firstname }}" name="firstname" type="text" class="form-control" placeholder="Entrez le prénom de l'enfant">
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label for="lastname">Nom</label>
                                    <input value="{{ $children->lastname }}" name="lastname" type="text" class="form-control"  placeholder="Enter le  nom de l'enfant">
                                </div>
                                <div class="col-md-12 form-group mb-3">
                                    <label for="father_name">Prénom du pére</label>
                                    <input value="{{ $children->father_name }}" name="father_name" type="text" class="form-control"placeholder="Entrez le prénom du pere">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="mother_firstname">Prénom de la mére</label>
                                    <input value="{{ $children->mother_firstname }}" name="mother_firstname" type="text" class="form-control"  placeholder="Entrez le prénom de la mére">
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label for="lastname">Nom de la mére</label>
                                    <input value="{{ $children->mother_lastname }}" name="mother_lastname" type="text" class="form-control" placeholder="Entrez le nom de la mere">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="picker2">Date de naissance</label>
                                    <div class="input-group">
                                        <input value="{{ $children->date }}"id="picker2" type="date" class="form-control" placeholder="yyyy-mm-dd" name="dp" >
                                    </div>
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="location">Lieu de naissance</label>
                                    <input value="{{ $children->location }}" name="location" type="text" class="form-control" id="location" placeholder="Le lieu de naissance">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="gender">Genre</label>
                                    <select name="gender" class="form-control">
                                        <option value="masculin">Masculin</option>
                                        <option value="féminin">Féminin</option>
                                    </select>
                                </div>


                                <div class="col-md-6 form-group mb-3">
                                    <label for="hospital">Hopital</label>
                                    <input value="{{ $children->hospital }}" name="hospital" class="form-control" id="hospital" placeholder="le nom de l'hopital">
                                </div>

                                <div class="col-md-12">
                                     <button class="btn btn-primary">Modifier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
@endsection


@section('bottom-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>


@endsection
