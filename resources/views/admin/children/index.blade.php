@extends('layouts.master')


@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    @include('admin.includes.breadcrumb',[
        'title' => 'Les enfants'])

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-header text-right bg-transparent">
                <a href="{{ route('children.create') }}" type="button" class="btn btn-primary btn-md m-1">
                    <i class="i-Add text-white mr-2"></i> Ajouter un enfant</a>
            </div>
            <div class="card-body">
                <h4 class="card-title mb-3">Registre</h4>

                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Prenom etNom</th>
                                <th>Date de naissance</th>
                                <th>Lieu</th>
                                <th>Pére</th>
                                <th>Mére</th>

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($childrens as $children)
                                <tr>
                                    <td>{{ $children->firstname .' '. $children->lastname }}</td>
                                    <td>{{ date('d-m-Y', strtotime($children->date) )}}</td>
                                    <td>{{  $children->location }}</td>
                                    <td>{{ $children->father_name }}</td>
                                    <td>{{ $children->mother_firstname .' '. $children->mother_lastname }}</td>
                                    <td>
                                        <a href="{{ route('children.edit',$children) }}" class="text-success mr-2">
                                            <i class="nav-icon i-Pen-2 font-weight-bold"></i>Editer
                                        </a>
                                        <a href="{{ route('children.delete',$children) }}" class="text-danger mr-2">
                                            <i class="nav-icon i-Close-Window font-weight-bold"></i>Supprimer
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
