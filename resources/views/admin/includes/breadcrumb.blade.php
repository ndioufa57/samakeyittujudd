<div class="breadcrumb">
    <h1>{{ $title }}</h1>
    <ul>
        <li><a href="">Dashboard</a></li>
        <li>{{ $title }}</li>
    </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
