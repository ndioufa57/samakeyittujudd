@extends('layouts.master')


@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    @include('admin.includes.breadcrumb',[
        'title' => 'Déclaration'])



<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="/admin/ajoutDeclaration" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Ajout d'une déclaration </h4>
              </div>
              <div class="card-body ">
                <div class="form-group row 2 ">
                        <div class="col-md-6 " id="prenom">
                            <label for="" class="col-form-label">Registre</label>
                            <input type="text" name="registre" class="form-control" />
                        </div>
                        <div class="col-md-6">
                            <label for="" class="col-form-label">Prénom</label>
                            <input type="text" name="prenom" class="form-control" />
                        </div>
                </div>
                <div class="form-group row">

                        
                        <div class="col-md-6">
                            <label for="" class="col-form-label">Nom</label>
                            <input type="text" name="nom" class="form-control" />
                        </div>

                        <div class="col-md-6">
                            <label for="mairie" class="col-form-label">Genre</label>
                            <select name="genre" class="form-control ">
                               <option value="Homme">Homme</option>
                               <option value="Femme">Femme</option>
                            </select>
                        </div>
                </div>

               
                  
                  
                <div class="form-group row ">
                        
                        <div class=" col-md-6 ">
                          <label for="" class="col-form-label" >Region</label>
                          <select class="form-control" name="region" id="region">
                              <option value="">--Sélectionner la région</option>

                              <?php use App\Models\Region; $reg=Region::all(); ?>

use Illuminate\Support\Facades\DB;



                              @foreach($reg as $dataReg)
                                <option value="{{$dataReg->id}}">
                                  {{$dataReg->nom}}
                                </option>
                              @endforeach
                          </select>
                        </div>

                        <div class="col-md-6">
                          <label for="" class="col-form-label">Departement</label>
                          <select class="form-control" name="departement" id="departement" >
                            <option value="">--Sélectionnez le département</option>
                        </select>
                        </div>
                 </div>
                 <div class="form-group row ">

                        <div class="col-md-6">
                          <label for="" class="col-form-label">Commune</label>
                          <select class="form-control" name="commune" id="commune" >
                            <option value="">--Sélectionnez la commune</option>
                          </select>
                        </div>

                        <div class="col-md-6">
                          <label for="" class="col-form-label">Localité</label>
                          <select class="form-control" name="localite" id="localite" >
                            <option value="">--Choisissez la localité</option>
                        </select>
                        </div>
                        
                </div>
                <div class="form-group row">
                        <div class="col-md-6">
                            <label for="" class="col-form-label">Hopital </label>
                            <input type="text" name="hopital" class="form-control" />
                        </div>

                        <div class="col-md-6">
                            <label for="" class="col-form-label">Prenom du père </label>
                            <input type="text" name="prenom_pere" class="form-control" />
                        </div>

                        
                        
                        
                </div>
                <div class="form-group row">
                        
                        <div class="col-md-6">
                            <label for="" class="col-form-label">Nom du père </label>
                            <input type="text" name="nom_pere" class="form-control" />
                        </div>

                        <div class="col-md-6">
                            <label for="" class="col-form-label">Prénom de la mère </label>
                            <input type="text" name="prenom_mere" class="form-control" />
                        </div>

                        
                        
                        
                </div>
                <div class="form-group row">
                        
                        <div class="col-md-6">
                            <label for="" class="col-form-label">Nom de la mère </label>
                            <input type="text" name="nom_mere" class="form-control" />
                        </div>

                        <!-- <div class="col-md-6">
                            <label for="mairie" class="col-form-label">Mairie</label>
                            <select name="mairie" class="form-control ">
                                @foreach ($mairies as $mairie)
                                    <option value="{{ $mairie->id }}" >{{ $mairie->name}}</option>
                                @endforeach
                            </select>
                        </div> -->

                        <div class="col-md-6 form-group mb-3">
                            <label for="certificat">Certificat d'accouchement</label>
                            <input name="certificat" type="file" class="form-control form-control-rounded" id="certificat" placeholder="Web address">
                        </div>
                        
                        
                </div>
                <div class="form-group row">
                        

                        <div class="col-md-6 form-group mb-3">
                            <label for="cni_mere">CNI Mère</label>
                            <input type="number" name="cni_mere" class="form-control" />
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="cni_pere">CNI Père</label>
                            <input type="number" name="cni_pere" class="form-control" />
                        </div>
                        
                        
                </div>
                <div class="form-group row">
                        

                        <div class="col-md-6 form-group mb-3">
                            <label for="cni_mere">Téphone Mère</label>
                            <input type="number" name="telephone_mere" class="form-control" />
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="cni_mere">District </label>
                            <input type="text" name="district" class="form-control" />
                        </div>
                        
                        
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <a class="btn btn-danger" data-dismiss="modal">Annuler</a>
                <button type="submit" class="btn btn-primary">Enregistrer</button>
              </div>
            </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-header text-right bg-transparent">
                <a href="#adddeclaaration" >
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                             <i class="i-Add text-white mr-2"></i> Ajouter une déclaration
                            </button>
                        </a>
            </div>
            <div class="card-body">
                <h4 class="card-title mb-3">Déclaration</h4>

                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Prenom etNom</th>
                                <th>Date de naissance</th>
                                <th>Lieu</th>
                                <th>Pére</th>
                                <th>Mére</th>

                                <th>Statut</th>

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($declaration as $data)
                                <tr>
                                    <td>{{ $data->id}}</td>
                                    <td>{{ $data->prenom .' '. $data->nom }}</td>
                                    <td>{{ date('d-m-Y', strtotime($data->created_at) )}}</td>
                                    <td>
                                      <?php $localite=DB::table('ml_localite')->where('id', $data->localite)->first(); ?>
                                      {{  $localite->nom }}
                                    </td>
                                    <td>{{ $data->prenom_pere .' '. $data->nom_pere }}</td>
                                    <td>{{ $data->prenom_mere .' '. $data->nom_mere }}</td>

                                    <td>
                                        @if($data->statut==0)
                                            <span class="badge badge-warning">Incomplet</span>
                                        @endif

                                        @if($data->statut==1)
                                            <span class="badge badge-success">En Traitement</span>
                                        @endif

                                        @if($data->statut==2)
                                            <span class="badge badge-success">Validé</span>
                                        @endif

                                        

                                    </td>
                                    <td>
                                        <a href="" class="text-warning mr-2"
                                            onclick="event.preventDefault();
                                            document.getElementById('details-form-{{ $data->id }}').submit();">
                                            <i class="icon-file-text"></i>Détails
                                            <form id="details-form-{{ $data->id }}" action="{{ url('admin/details-declaration/'.$data->id)}}" method="get" style="display: none;">
                                               
                                            </form>
                                        </a>
                                        <a href="" class="text-success mr-2"
                                            onclick="event.preventDefault();
                                            document.getElementById('modif-form-{{ $data->id }}').submit();">
                                            <i class="nav-icon i-Pen-2 font-weight-bold"></i>Modifier
                                            <form id="modif-form-{{ $data->id }}" action="{{ url('admin/modification-declaration/'.$data->id)}}" method="get" style="display: none;">
                                               
                                            </form>
                                        </a>
                                        <!-- <a href="{{ route('children.delete',$data) }}" class="text-danger mr-2">
                                            <i class="nav-icon i-Close-Window font-weight-bold"></i>Supprimer
                                        </a> -->
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')

<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/acat.js')}}"></script>
    <script src="{{asset('assets/js/choix.js')}}"></script>

@endsection
