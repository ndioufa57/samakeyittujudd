@extends('layouts.master')

@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Modification déclaration'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach
                        @endif
                        <div class="card-title mb-3">Cartographie </div>

                        <?php

use App\Models\Declaration;

                        //$host="37.59.107.124";
                        $host="localhost";
                        $baseSms="skj";
                        $userSms="root";
                        $mdpSms="";

                        try {
                                $bdd = new PDO('mysql:host='.$host.';dbname='.$baseSms, $userSms, $mdpSms);
                        } catch(PDOException $e){
                                die('Erreur: ' . $e->getmessage());
                        }?>


                            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.full.min.js" defer></script>
                            <?php

                            // $sql='SELECT COUNT(id) FROM `declarations` WHERE 1';
                            // $activites = $activite->executerReq($sql);

                            // Declaration::where('statut','=',1)->where('departement','=',1)->count();
                             
                            $nDeclarations = Declaration::all()->count();
                            $nDeclarationsT = Declaration::where('statut','=',1)->count();
                            $nDeclarationsEC = Declaration::where('statut','=',0)->count();






                            $nDeclarationsDiourbel = Declaration::where('departement','=',6)->count();
                            $nDeclarationsTDiourbel = Declaration::where('statut','=',1)->where('departement','=',6)->count();
                            $nDeclarationsECDiourbel =Declaration::where('statut','=',0)->where('departement','=',6)->count();






                            $nDeclarationsKanel = Declaration::where('departement','=',27)->count();
                            $nDeclarationsTKanel = Declaration::where('statut','=',1)->where('departement','=',27)->count();
                            $nDeclarationsECKanel =Declaration::where('statut','=',0)->where('departement','=',27)->count();

                            // $dataPointsKanel = array(
                            //     array("label"=> "Activités", "y"=> $nDeclarationsKanel),
                            //     array("label"=> "Collectes", "y"=> $nDeclarationsTKanel),
                            //     array("label"=> "Alertes", "y"=> $nDeclarationsECKanel)
                            // );



                            //Ranerou

                            $nDeclarationsRanerou = Declaration::where('departement','=',28)->count();
                            $nDeclarationsTRanerou = Declaration::where('statut','=',1)->where('departement','=',28)->count();
                            $nDeclarationsECRanerou =Declaration::where('statut','=',0)->where('departement','=',28)->count();




                            //Matam

                            $nDeclarationsMatam = Declaration::where('departement','=',29)->count();
                            $nDeclarationsTMatam = Declaration::where('statut','=',1)->where('departement','=',29)->count();
                            $nDeclarationsECMatam =Declaration::where('statut','=',0)->where('departement','=',29)->count();

                            // $dataPointsMatam = array(
                            //  array("label"=> "Activités", "y"=> $nDeclarationsMatam),
                            //  array("label"=> "Collectes", "y"=> $nDeclarationsTMatam),
                            //  array("label"=> "Alertes", "y"=> $nDeclarationsECMatam)
                            // );


                            //Podor

                            $nDeclarationsPodor = Declaration::where('departement','=',32)->count();
                            $nDeclarationsTPodor = Declaration::where('statut','=',1)->where('departement','=',32)->count();
                            $nDeclarationsECPodor =Declaration::where('statut','=',0)->where('departement','=',32)->count();



                            //Dagana

                            $nDeclarationsDagana = Declaration::where('departement','=',31)->count();
                            $nDeclarationsTDagana = Declaration::where('statut','=',1)->where('departement','=',31)->count();
                            $nDeclarationsECDagana =Declaration::where('statut','=',0)->where('departement','=',31)->count();



                            //SaintLouis

                            $nDeclarationsSaintLouis= Declaration::where('departement','=',30)->count();
                            $nDeclarationsTSaintLouis = Declaration::where('statut','=',1)->where('departement','=',30)->count();
                            $nDeclarationsECSaintLouis =Declaration::where('statut','=',0)->where('departement','=',30)->count();



                            //Louga


                            $nDeclarationsLouga= Declaration::where('departement','=',24)->count();
                            $nDeclarationsTLouga = Declaration::where('statut','=',1)->where('departement','=',24)->count();
                            $nDeclarationsECLouga =Declaration::where('statut','=',0)->where('departement','=',24)->count();



                            //Kebemer

                            $nDeclarationsKebemer= Declaration::where('departement','=',26)->count();
                            $nDeclarationsTKebemer = Declaration::where('statut','=',1)->where('departement','=',26)->count();
                            $nDeclarationsECKebemer =Declaration::where('statut','=',0)->where('departement','=',26)->count();


                            //Tivaouane

                            $nDeclarationsTivaouane = Declaration::where('departement','=',42)->count();
                            $nDeclarationsTTivaouane = Declaration::where('statut','=',1)->where('departement','=',42)->count();
                            $nDeclarationsECTivaouane =Declaration::where('statut','=',0)->where('departement','=',42)->count();


                            //Thies

                            $nDeclarationsThies = Declaration::where('departement','=',41)->count();
                            $nDeclarationsTThies = Declaration::where('statut','=',1)->where('departement','=',41)->count();
                            $nDeclarationsECThies =Declaration::where('statut','=',0)->where('departement','=',41)->count();


                            //Rufisque

                            $nDeclarationsRufisque = Declaration::where('departement','=',1)->count();
                            $nDeclarationsTRufisque = Declaration::where('statut','=',1)->where('departement','=',1)->count();
                            $nDeclarationsECRufisque =Declaration::where('statut','=',0)->where('departement','=',1)->count();

                            //Pikine

                            $nDeclarationsPikine = Declaration::where('departement','=',3)->count();
                            $nDeclarationsTPikine = Declaration::where('statut','=',1)->where('departement','=',3)->count();
                            $nDeclarationsECPikine =Declaration::where('statut','=',0)->where('departement','=',3)->count();

                            //Dakar

                            $nDeclarationsDakar = Declaration::where('departement','=',2)->count();
                            $nDeclarationsTDakar = Declaration::where('statut','=',1)->where('departement','=',2)->count();
                            $nDeclarationsECDakar =Declaration::where('statut','=',0)->where('departement','=',2)->count();


                            //Tambacounda

                            $nDeclarationsTambacounda = Declaration::where('departement','=',39)->count();
                            $nDeclarationsTTambacounda = Declaration::where('statut','=',1)->where('departement','=',39)->count();
                            $nDeclarationsECTambacounda =Declaration::where('statut','=',0)->where('departement','=',39)->count();


                            //Bounkiling

                            $nDeclarationsBounkiling = Declaration::where('departement','=',35)->count();
                            $nDeclarationsTBounkiling = Declaration::where('statut','=',1)->where('departement','=',35)->count();
                            $nDeclarationsECBounkiling =Declaration::where('statut','=',0)->where('departement','=',35)->count();


                            //Bignona

                            $nDeclarationsBignona = Declaration::where('departement','=',44)->count();
                            $nDeclarationsTBignona = Declaration::where('statut','=',1)->where('departement','=',44)->count();
                            $nDeclarationsECBignona =Declaration::where('statut','=',0)->where('departement','=',44)->count();


                            //Velingara

                            $nDeclarationsVelingara = Declaration::where('departement','=',21)->count();
                            $nDeclarationsTVelingara = Declaration::where('statut','=',1)->where('departement','=',21)->count();
                            $nDeclarationsECVelingara =Declaration::where('statut','=',0)->where('departement','=',21)->count();


                            //Foundiougne

                            $nDeclarationsFoundiougne = Declaration::where('departement','=',10)->count();
                            $nDeclarationsTFoundiougne = Declaration::where('statut','=',1)->where('departement','=',10)->count();
                            $nDeclarationsECFoundiougne =Declaration::where('statut','=',0)->where('departement','=',10)->count();


                            //Fatick

                            $nDeclarationsFatick = Declaration::where('departement','=',8)->count();
                            $nDeclarationsTFatick = Declaration::where('statut','=',1)->where('departement','=',8)->count();
                            $nDeclarationsECFatick =Declaration::where('statut','=',0)->where('departement','=',8)->count();

                            //NioroduRip

                            $nDeclarationsNioroduRip = Declaration::where('departement','=',17)->count();
                            $nDeclarationsTNioroduRip = Declaration::where('statut','=',1)->where('departement','=',17)->count();
                            $nDeclarationsECNioroduRip =Declaration::where('statut','=',0)->where('departement','=',17)->count();


                            //Mbour

                            $nDeclarationsMbour = Declaration::where('departement','=',40)->count();
                            $nDeclarationsTMbour = Declaration::where('statut','=',1)->where('departement','=',40)->count();
                            $nDeclarationsECMbour =Declaration::where('statut','=',0)->where('departement','=',40)->count();


                            //Kaolack

                            $nDeclarationsKaolack = Declaration::where('departement','=',16)->count();
                            $nDeclarationsTKaolack = Declaration::where('statut','=',1)->where('departement','=',16)->count();
                            $nDeclarationsECKaolack =Declaration::where('statut','=',0)->where('departement','=',16)->count();


                            //Mbacke

                            $nDeclarationsMbacke = Declaration::where('departement','=',5)->count();
                            $nDeclarationsTMbacke = Declaration::where('statut','=',1)->where('departement','=',5)->count();
                            $nDeclarationsECMbacke =Declaration::where('statut','=',0)->where('departement','=',5)->count();

                            //Bambey

                            $nDeclarationsBambey = Declaration::where('departement','=',7)->count();
                            $nDeclarationsTBambey = Declaration::where('statut','=',1)->where('departement','=',7)->count();
                            $nDeclarationsECBambey =Declaration::where('statut','=',0)->where('departement','=',7)->count();


                            //Ziguinchor

                            $nDeclarationsZiguinchor = Declaration::where('departement','=',45)->count();
                            $nDeclarationsTZiguinchor = Declaration::where('statut','=',1)->where('departement','=',45)->count();
                            $nDeclarationsECZiguinchor =Declaration::where('statut','=',0)->where('departement','=',45)->count();

                            //Oussouye

                            $nDeclarationsOussouye = Declaration::where('departement','=',43)->count();
                            $nDeclarationsTOussouye = Declaration::where('statut','=',1)->where('departement','=',43)->count();
                            $nDeclarationsECOussouye =Declaration::where('statut','=',0)->where('departement','=',43)->count();

                            //Guinguineo

                            $nDeclarationsGuinguineo = Declaration::where('departement','=',15)->count();
                            $nDeclarationsTGuinguineo = Declaration::where('statut','=',1)->where('departement','=',15)->count();
                            $nDeclarationsECGuinguineo =Declaration::where('statut','=',0)->where('departement','=',15)->count();

                            //Linguere

                            $nDeclarationsLinguere = Declaration::where('departement','=',25)->count();
                            $nDeclarationsTLinguere = Declaration::where('statut','=',1)->where('departement','=',25)->count();
                            $nDeclarationsECLinguere =Declaration::where('statut','=',0)->where('departement','=',25)->count();


                            //Salemata

                            $nDeclarationsSalemata = Declaration::where('departement','=',20)->count();
                            $nDeclarationsTSalemata = Declaration::where('statut','=',1)->where('departement','=',20)->count();
                            $nDeclarationsECSalemata =Declaration::where('statut','=',0)->where('departement','=',20)->count();


                            //Saraya

                            $nDeclarationsSaraya = Declaration::where('departement','=',19)->count();
                            $nDeclarationsTSaraya = Declaration::where('statut','=',1)->where('departement','=',19)->count();
                            $nDeclarationsECSaraya =Declaration::where('statut','=',0)->where('departement','=',19)->count();

                            //Kedougou

                            $nDeclarationsKedougou = Declaration::where('departement','=',18)->count();
                            $nDeclarationsTKedougou = Declaration::where('statut','=',1)->where('departement','=',18)->count();
                            $nDeclarationsECKedougou =Declaration::where('statut','=',0)->where('departement','=',18)->count();


                            //Goudomp

                            $nDeclarationsGoudomp =  Declaration::where('departement','=',33)->count();
                            $nDeclarationsTGoudomp = Declaration::where('statut','=',1)->where('departement','=',33)->count();
                            $nDeclarationsECGoudomp =Declaration::where('statut','=',0)->where('departement','=',33)->count();

                            //Sedhiou

                            $nDeclarationsSedhiou = Declaration::where('departement','=',34)->count();
                            $nDeclarationsTSedhiou = Declaration::where('statut','=',1)->where('departement','=',34)->count();
                            $nDeclarationsECSedhiou =Declaration::where('statut','=',0)->where('departement','=',34)->count();

                            //Kolda

                            $nDeclarationsKolda = Declaration::where('departement','=',23)->count();
                            $nDeclarationsTKolda = Declaration::where('statut','=',1)->where('departement','=',23)->count();
                            $nDeclarationsECKolda =Declaration::where('statut','=',0)->where('departement','=',23)->count();

                            //MedinaYoroFoulah

                            $nDeclarationsMedinaYoroFoulah = Declaration::where('departement','=',22)->count();
                            $nDeclarationsTMedinaYoroFoulah = Declaration::where('statut','=',1)->where('departement','=',22)->count();
                            $nDeclarationsECMedinaYoroFoulah =Declaration::where('statut','=',0)->where('departement','=',22)->count();

                            //Koungheul

                            $nDeclarationsKoungheul = Declaration::where('departement','=',14)->count();
                            $nDeclarationsTKoungheul = Declaration::where('statut','=',1)->where('departement','=',14)->count();
                            $nDeclarationsECKoungheul =Declaration::where('statut','=',0)->where('departement','=',14)->count();

                            //Birkelane

                            $nDeclarationsBirkelane = Declaration::where('departement','=',11)->count();
                            $nDeclarationsTBirkelane = Declaration::where('statut','=',1)->where('departement','=',11)->count();
                            $nDeclarationsECBirkelane =Declaration::where('statut','=',0)->where('departement','=',11)->count();


                            //Kaffrine

                            $nDeclarationsKaffrine = Declaration::where('departement','=',12)->count();
                            $nDeclarationsTKaffrine = Declaration::where('statut','=',1)->where('departement','=',12)->count();
                            $nDeclarationsECKaffrine =Declaration::where('statut','=',0)->where('departement','=',12)->count();


                            //MalemHodar

                            $nDeclarationsMalemHodar = Declaration::where('departement','=',13)->count();
                            $nDeclarationsTMalemHodar = Declaration::where('statut','=',1)->where('departement','=',13)->count();
                            $nDeclarationsECMalemHodar =Declaration::where('statut','=',0)->where('departement','=',13)->count();


                            //Gossas

                            $nDeclarationsGossas = Declaration::where('departement','=',9)->count();
                            $nDeclarationsTGossas = Declaration::where('statut','=',1)->where('departement','=',9)->count();
                            $nDeclarationsECGossas =Declaration::where('statut','=',0)->where('departement','=',9)->count();


                            //Koumpentoum

                            $nDeclarationsKoumpentoum = Declaration::where('departement','=',38)->count();
                            $nDeclarationsTKoumpentoum = Declaration::where('statut','=',1)->where('departement','=',38)->count();
                            $nDeclarationsECKoumpentoum =Declaration::where('statut','=',0)->where('departement','=',38)->count();


                            //Goudiry

                            $nDeclarationsGoudiry = Declaration::where('departement','=',37)->count();
                            $nDeclarationsTGoudiry = Declaration::where('statut','=',1)->where('departement','=',37)->count();
                            $nDeclarationsECGoudiry =Declaration::where('statut','=',0)->where('departement','=',37)->count();


                            //Bakel

                            $nDeclarationsBakel = Declaration::where('departement','=',36)->count();
                            $nDeclarationsTBakel = Declaration::where('statut','=',1)->where('departement','=',36)->count();
                            $nDeclarationsECBakel =Declaration::where('statut','=',0)->where('departement','=',36)->count();

                            //Guediawaye

                            $nDeclarationsGuediawaye = Declaration::where('departement','=',4)->count();
                            $nDeclarationsTGuediawaye = Declaration::where('statut','=',1)->where('departement','=',4)->count();
                            $nDeclarationsECGuediawaye =Declaration::where('statut','=',0)->where('departement','=',4)->count();

                                
                            ?>



                            <script>




                             

                            </script>

                            <script >

                            $(document).ready(function(){



                            });


                                
                            </script>

                            <section class="testimonials text-center ">
                              
                                  <div class="col-lg-12 ">

                                        <style >
                                        
                                            .exemple{

                                            display: flex;
                                            margin: 2rem auto

                                            }

                                            .exemple path{

                                            fill: #ccc;
                                            stroke: #ffffff;
                                            stroke-width: 1 /*on separe chaque region avec une ligne blanche de 1 pixel */
                                            transition: .6s fill;
                                            }

                                            .exemple path:hover{


                                            cursor: pointer;
                                            fill: #4fade8 !important;
                                            }

                                            element.style {
                                                display: block;
                                                height: 400px;
                                                width: 526px;
                                            }


                                            .boutonDept{
                                                margin: 1px;

                                            }



                                            


                                        </style>
                                        
                                    <div class="row">    


                                        <div class="col-lg-6 " id="mondiv"  >


                                            <svg class=exemple   version="1.1" id="carte" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 1381.9 1026" style="enable-background:new 0 0 1381.9 1026;" xml:space="preserve">


                                                <style type="text/css">
                                                    .st0{stroke:#000000;stroke-miterlimit:10;}

                                                    

                                                    /*path:hover{
                                                        fill: #D3D3D3;
                                                    }*/
                                                    path:hover {

                                                      fill: red;
                                                    }

                                                    

                                                    

                                                </style>

                                                <g>

                                                <path data-val="Kanel"   id="dataKanel" class="btn-load" data-chart="dataKanel" title="<strong><u> Kanel </u> </strong> <br> Total: <?= $nDeclarationsKanel ?>  <br> Validées: <?= $nDeclarationsTKanel ?> <br> Encours: <?= $nDeclarationsECKanel ?>"  d="M901.8,536.8c19.5-14.8,37.7-28.4,55.6-42.5c5.8-4.6,11.2-5,18.4-3.7c24.9,4.4,49.1,12.4,75.4,8.4
                                                    c23.6-3.6,37.6-15.9,45.1-37.3c4.1-11.6,7.6-23.5,11.1-35.3c0.6-1.9,0.9-4.5,0.1-6.2c-5-11.4-10.2-22.7-15.7-33.9
                                                    c-0.8-1.6-3-2.6-4.6-3.9c-10-8.2-26-11.1-25.7-28.7c0-0.3-1.1-0.5-1.9-0.9c-2.3,1.2-4.7,2.4-6.7,3.4c-4.3-4.1-8.3-7.6-11.8-11.5
                                                    c-0.9-1.1-1.1-3.8-0.4-5.1c0.7-1.3,2.9-2.5,4.4-2.5c2,0,3.9,1.4,6.8,2.5c2.3-10.3-4.4-12.9-10.8-15.1c-6.5-2.2-14.3-4.2-12.6-12.6
                                                    c1.9-9.3-3.8-16-4.9-25c-4.9,2-9,4.2-13.5,5.5c-8.8,2.5-19.5-5.4-16.3-12.2c5-10.6-0.1-14.4-9.4-17.1c-2.4,4.7-4.9,9.4-7.3,14.2
                                                    c-6.2,12.8-11.3,25.6-28.8,28.2c-6.5,1-14,8.6-17.1,15c-7.4,15.2-19.5,25.6-31.3,36.5c-2.8,2.6-6.2,5.9-7,9.4
                                                    c-1.6,6.6-4.4,11.5-10.3,14.8c-6.1,3.4-5.1,6.4,0.1,10.5c8.3,6.5,15.3,14.9,23.9,20.8c15.1,10.4-2.9,18.7-0.9,28.1
                                                    c0.2,0.9-0.1,1.9-0.3,2.8c-1.1,4.9-1.1,10.5-3.7,14.5c-4.6,7.1-6.1,13-4.5,22.1C900.6,498,900.5,516.5,901.8,536.8z"/>


                                                <path  data-val="Ranerou"  id="dataRanerou" class="btn-load" data-chart="dataRanerou" title="<strong><u> Ranerou </u> </strong> <br> Total: <?= $nDeclarationsRanerou ?>  <br> Validées: <?= $nDeclarationsTRanerou ?> <br> Encours: <?= $nDeclarationsECRanerou ?>"  d="M911.6,149.9c-1.3,2.8-3.6,5.8-3.9,9c-1,10.1-1.7,20.2-1.7,30.3c0,6.5-0.9,11.1-8.4,12.1c-1.6,0.2-3.8,2.1-4.4,3.7
                                                    c-3.5,9.6-7,19.3-9.7,29.2c-1.7,6.1-5.1,7.4-11,7.5c-34.9,0.9-69.8,2.2-104.7,3.6c-2,0.1-4.5,1-5.9,2.4
                                                    c-11.9,11.6-26.2,21.6-26.6,39.5c-11.8,2.9-22.7,5.5-33.7,8.2c4.8,14.9,1.9,19.6-11.8,22.5c-2.4,0.5-5.3,3.8-6.1,6.4
                                                    c-4.6,15.2-8.8,30.4-12.5,45.8c-0.7,2.9,0.8,7.7,3.1,9.5c8.8,7.1,18.8,12.7,27.4,20c3.9,3.3,6.2,9,8.3,14c1.8,4.2,1.8,9.3,3.8,13.4
                                                    c2.3,4.8,5.5,9.3,9.1,13.3c8.3,9.3,10,17.8,2.4,29.3c-8.1,12.1-0.9,25.9,13.1,30.8c21.8,7.7,43.7,15.3,65,24.3
                                                    c15,6.3,29.3,13.9,46.3,11.6c1.5-0.2,3.1,0.4,4.7,0.4c13.8,0,27.7,0,42.3,0c-1.1-13.5-2.9-27-3.1-40.5c-0.2-10.7-6.5-21.2-0.5-32
                                                    c2.7-4.8,4.4-10.1,6.1-15.3c1.4-4.5,0.9-10.1,3.5-13.7c7.5-10.5,4.2-17-5.5-23.5c-5.3-3.5-8.9-9.5-13.8-13.8
                                                    c-3.6-3.2-8-5.5-10.6-7.3c6.6-10.9,12.1-20.4,18.2-29.6c1.7-2.5,5.1-3.8,7.1-6.2c8.8-9.8,18.7-19,25.9-30
                                                    c7.1-10.8,14.4-19.6,27.6-22.6c4.7-1.1,10.4-3.8,12.8-7.5c5.4-8.4,9.1-17.9,13.2-27.1c0.8-1.7,1.1-4.9,0.1-5.9c-1.2-1.2-4.5-1.8-6-1
                                                    c-9.9,5.3-10.7-3.9-12.7-8.7c-0.9-2.1,3.2-6.2,6-11c-2.8-2.4-7.5-5.9-11.6-10.1c-2.1-2.1-5.3-6.4-4.6-7.5c5.1-7.8-4.3-13-2.9-20.4
                                                    c0.8-4.3-1.8-11.4-5.2-13.8c-11.1-7.7-10.1-17.9-8.2-29C925.8,157.1,919.4,153.9,911.6,149.9z"/>

                                                <path data-val="Matam"  id="dataMatam" class="btn-load" data-chart="dataMatam" title="<strong><u> Matam </u> </strong> <br> Total: <?= $nDeclarationsMatam ?>  <br> Validées: <?= $nDeclarationsTMatam ?> <br> Encours: <?= $nDeclarationsECMatam ?>" d="M852.5,138.3c-2.9,7.1-4,17.8-9,20.1c-12,5.5-20.3,13.9-28.5,23.4c-16.3,18.8-33.1,37.2-50.8,57.1c11.3,0,20.6,0.3,30,0
                                                    c25-0.8,49.9-1.8,74.9-2.9c2.6-0.1,6.5-1.5,7.5-3.5c3.9-8.3,9.4-16.9,9.7-25.6c0.4-10,5.4-11.8,13.2-12.7c0-18.1,0-35.5,0-51.6
                                                    c-16-8.7-26.1,6.4-39.9,4.9C857.6,145,855.1,141.8,852.5,138.3z"/>

                                                <path data-val="Podor"  id="dataPodor" class="btn-load" data-chart="dataPodor" title="<strong><u> Podor </u> </strong> <br> Total: <?= $nDeclarationsPodor ?>  <br> Validées: <?= $nDeclarationsTPodor ?> <br> Encours: <?= $nDeclarationsECPodor ?>" d="M799.9,95c-2.7-2.2-4.2-4.5-5.9-4.7c-9-0.7-12.9-8-18.5-13.2c-2.3-2.1-4.8-4.1-7.1-6.2c-7.5-7-15-14-22.4-21.2
                                                    c-2.3-2.2-4-6.3-6.5-6.9c-2.1-0.5-5,3.1-8.3,5.4c-1.4-4.5-2.7-8.9-3.9-12.7c-4,0.4-7.9,0.7-11.9,1.1c0.4-13.8,0.4-13.8-13-15.2
                                                    c-0.9-0.1-1.9-0.5-2.8-0.3c-5.3,1.1-10.6,2.9-16,3.5c-5.4,0.6-11,0-16.5-0.2c-7-0.2-17.2,2.5-20.3-1.1c-6.4-7.4-15.9-3.6-17.2-2
                                                    c-7.6,9.2-7.1-1.5-9.8-3.4c-5,5-17.4,4.9-23.8,1c-6.7,1.6-11.6,2.8-16.1,3.9c-2.8-4.9-5.3-9.2-7.9-13.6c-1.9,1.3-2.2,1.4-2.4,1.7
                                                    c-0.8,1.4-1.5,2.8-2.2,4.2c-5,10.6-10.1,10.9-17.4,1.7c-5.8-7.2-7.2-0.9-8.2,0.8c2.1,5,3.4,8.2,4.7,11.3c-3.9,1.3-7.7,2.9-11.7,3.6
                                                    c-2.2,0.4-6.1-1.9-6.9-1c-8.5,9.5-17.9,6-27.7,3.3c-1.8-0.5-7.3,4.2-7,5.6c2.8,11.9-4.5,17.8-12.9,22.9c-6.6,4-10.4,8.8-8.4,16.9
                                                    c1,4,2.4,8.2,1.8,12c-0.5,3.4-3.5,6.5-4.2,7.7c3.3,4,7.5,6.7,7.6,9.6c0.7,13,0.2,26.1,0.2,39.2c0,5.9,0,11.9,0,19.3
                                                    c36.2,0,69.9,0.4,103.6-0.2c11.9-0.2,22.3,2.1,32.8,7.6c5.4,2.8,11.9,3.3,17.9,5c5.1,1.4,11.7,1.3,15,4.6
                                                    c12.2,12.3,23.4,25.6,34.9,38.6c1.4,1.6,2.8,3.5,4.6,4.5c8.7,5.1,17.9,9.5,26.4,15.1c8.2,5.4,19.8,7.1,23,21.1
                                                    c23.5-26.4,45.2-52,68.3-76.3c9.3-9.7,14.6-24.4,28.7-28.4c13.8-3.9,12.7-14.7,15.6-26.8c-9.3,3.6-16.6,6.5-25,9.8
                                                    c-2.2-4.9-4.9-10.8-8.7-19.2c-3.3-2-9.6-5.8-17.4-10.6c-0.1-0.6,0.7-5.3-1.2-6.9C788.3,100.1,792,97.7,799.9,95z"/>

                                                <path data-val="Dagana"  id="dataDagana" class="btn-load" data-chart="dataDagana" title="<strong><u> Dagana </u> </strong> <br> Total: <?= $nDeclarationsDagana ?>  <br> Validées: <?= $nDeclarationsTDagana ?> <br> Encours: <?= $nDeclarationsECDagana ?>" d="M473.1,106c-11,3.7-11.1-4.3-8.3-7.4c5-5.7,3.5-11.1,2.2-16.5c-2.4-10.3,1.3-17,10.3-21.6c1.7-0.8,3.3-1.9,4.8-3.1
                                                    c6.5-5.1,9-11,4.2-18.7c-14.3,4.9-14.3,4.9-24.1-8.5c-3.6,3.3-9.2,6-10.3,9.9c-2.5,8.7-5.5,11.5-14.2,8.9c-3.5-1.1-7.1-2-10.9-3.1
                                                    c0.3,7.1-12.5,14.8-19.8,13c-12.8-3.1-25.5-7.3-38.4-8.7c-9.5-1-18.5,5-28.7,5.5c-5.6,0.3-9.6,3.2-13.7-2.6
                                                    c-2.9-4.1-6.3-7.8-9.9-12.2c-1.7,0.5-5.2,0.6-6.9,2.3c-6,6-12.1,8.3-20.5,5.2c-1.8-0.7-6.6,2.9-7.8,5.5c-2.9,6.3-4.1,13.4-6.8,19.9
                                                    c-3,7.2-7.2,13.8-10.4,20.9c-0.8,1.7-0.2,4.3,0.4,6.4c2.2,8.2-3.6,16.2-12.1,18c-3,0.6-6.8,3.9-7.5,6.6c-1.2,4.6-0.3,9.8-0.3,17.8
                                                    c9.1-4.5,15.9-7.8,24.5-12c0.5,8.7,1.1,17.7,1.6,26.9c7.5,1,15.5,2.4,23.5,3.2c7.3,0.7,15.8,3.2,21.9,0.6c18-7.6,37-13.8,51.2-28.5
                                                    c4.1-4.2,9.3-7.2,14.4-11.1c17.3,19,34.1,37.6,51.3,55.9c2.5,2.7,6.8,4.8,10.5,5c7.2,0.4,14.5-1.1,21.7-1.2c5.3,0,7.8-2.2,7.5-7.3
                                                    c-0.4-7.2-1.5-14.4-1.4-21.5C471.2,138.4,472.2,123.2,473.1,106z"/>

                                                <path data-val="SaintLouis"  id="dataSaintLouis" class="btn-load" data-chart="dataSaintLouis" title="<strong><u> Saint-Louis </u> </strong> <br> Total: <?= $nDeclarationsSaintLouis ?>  <br> Validées: <?= $nDeclarationsTSaintLouis ?> <br> Encours: <?= $nDeclarationsECSaintLouis ?>" d="M224.9,207.5c11.1,0,21.8,1.3,31.9-0.4c8-1.3,15.7-5.7,23-9.8c6-3.4,11.1-8.3,16.6-12.6c4.1-3.2,6.4-6.3,1.9-11.6
                                                    c-6-7.3-13.3-9.6-22.1-9.1c-8,0.5-13.3-0.8-10.4-11.1c1.3-4.7,0.2-10.1,0.2-13.5c-5.7,1.7-9.7,3.2-13.9,4
                                                    c-4.8,0.9-21.1,11.4-21.6,14.7c-0.9,6.6-1.6,13.2-2.4,19.7C227,187.2,226,196.6,224.9,207.5z"/>

                                                <path data-val="Louga"  id="dataLouga" class="btn-load" data-chart="dataLouga" title="<strong><u> Louga </u> </strong> <br> Total: <?= $nDeclarationsLouga ?>  <br> Validées: <?= $nDeclarationsTLouga ?> <br> Encours: <?= $nDeclarationsECLouga ?>" d="M248.5,254.8c10-0.1,17.9,2,22.6,15c4,11,13.4,21.3,22.8,28.8c6.8,5.4,17.5,6.4,26.7,7.8c10.7,1.6,21.9,0.7,32.6,2.4
                                                    c10.7,1.7,21.1,5.3,31.3,8c7.5-12.7,14.5-23.9,20.9-35.5c13.3-24.3,33.7-45.8,31.7-76.5c-0.4-6.5,1.2-14.9-2.2-19
                                                    c-16.4-19.7-34.1-38.2-51.7-57.4c-24.4,20.6-49,38.1-80.6,41.7c7.3,7.9,6.2,13.1-2.9,18.4c-4.8,2.8-8.7,7.3-13,11
                                                    c-1.3,1.1-2.4,2.8-3.9,3.3c-13,4.7-25.7,10.5-40.2,8.1c-4.6-0.8-9.5-0.1-12.5-0.1c-4.6,9.7-7.7,18.7-12.8,26.5
                                                    c-4.5,6.9-2.8,10.8,3.4,15.1c6.5-2.1,13.4-4.2,22.5-7.1C244,246.7,246.1,250.5,248.5,254.8z"/>

                                                <path data-val="Kebemer"  id="dataKebemer" class="btn-load" data-chart="dataKebemer" title="<strong><u> Kébémer </u> </strong> <br> Total: <?= $nDeclarationsKebemer ?>  <br> Validées: <?= $nDeclarationsTKebemer ?> <br> Encours: <?= $nDeclarationsECKebemer ?>" d="M279.4,336.5c1.4,3,2.2,4.3,2.6,5.8c2.5,9.3,3,19.9,7.9,27.7c6.9,11,11.7,22.2,14.5,34.6c0.4,1.6,2.9,4,4.5,4
                                                    c11.6,0.1,23.4,1,34.7-0.9c14.2-2.3,28.1-7,41.5-10.6c0-24.2,0-49.3,0-74.1c-11.2-2.9-21.7-6.5-32.4-8.3c-10.2-1.7-20.7-2-31.1-2.1
                                                    c-24.5-0.4-39.9-13.9-50.6-34.4c-2.3-4.4-3.2-10.8-6.8-12.9c-5.6-3.2-12.9-3.5-20.9-5.4c-0.2-1.3-0.8-5.2-1.1-7.1
                                                    c-8.1,2.1-15.7,4.2-23.6,6.2c-2.1-2.2-4.1-4.4-6.5-7c-8.7,14-16.9,27.3-25.6,41.2c3,1.4,6.3,1.9,7.1,3.6c1.2,2.4,1.6,6.4,0.4,8.4
                                                    c-3.3,5.4-7.8,10.1-11.7,15.2c-5.9,7.6,1.2,11.7,4.5,16.9c1.6,2.5,3.1,7.4,4.3,7.2c11.4-1.2,13.3,7.1,16.6,14.8
                                                    c0.8,1.8,6.5,3.8,7.1,3.2c6.5-7.4,12.9-15.1,18.6-23.3c4.2-6,7.3-12.8,11-19.3c6.5,3.7,10.8,5.9,15.1,6.1c8.9,0.2,11.9,3.2,11,12.6
                                                    c-0.2,1.9,0.8,3.9,1.7,8.3C275.4,342.3,276.9,340.1,279.4,336.5z"/>

                                                <path data-val="Tivaouane"  id="dataTivaouane" class="btn-load" data-chart="dataTivaouane" title="<strong><u> Tivaouane </u> </strong> <br> Total: <?= $nDeclarationsTivaouane ?>  <br> Validées: <?= $nDeclarationsTTivaouane ?> <br> Encours: <?= $nDeclarationsECTivaouane ?>" d="M247.5,326.7c-0.7-0.8-1.4-1.6-2.1-2.5c-2.6,5.2-5.2,10.3-7.8,15.5c-0.8,1.7-1.6,3.4-2.7,4.9c-4.4,5.5-8.9,11-13.5,16.3
                                                    c-2.1,2.4-5.5,6.6-7.1,6.1c-3.3-1-6.6-4.2-8.4-7.3c-3.2-5.8-3.6-13.6-13.8-11.9c-1.8,0.3-4.7-4.7-6.8-7.5c-1.1-1.4-0.9-4.2-2.1-5
                                                    c-7.1-4.5-5.4-10.2-2.2-15.7c3.7-6.5,8-12.8,12.4-19.7c-1.6-0.8-4.5-2.4-8-4.2c-5.3,8.3-10.8,16.4-15.7,24.8
                                                    c-17,29.5-37,56.7-60.4,81.3c-6.7,7-10.7,13.3-3.7,22.4c2.2,2.9,3.8,6.8,4.3,10.4c1.2,8.5,11.7,8.4,12.5,7.4c6-6.7,13.4-5.5,20.7-6
                                                    c9.5-0.7,19.1-1.5,28.5-2.8c3.4-0.5,7.1-1.8,9.8-4c12.2-9.8,23.3-20.9,39.6-24.2c8-1.6,15.5-5.4,23.2-7.9c2.6-0.9,5.8-2,8.2-1.3
                                                    c14.1,4.5,30.4,2.9,42,14.9c1.3,1.4,4.9,2.1,6.4,1.3c1.4-0.7,2.4-4.4,1.8-6.1c-3.9-10.5-8.3-20.9-12.6-31.2c-1-2.3-2.6-4.4-3.2-6.8
                                                    c-2-7.8-3.7-15.6-5.4-22.7c-3.8,0.9-5.9,2.2-7.8,1.8c-11.1-2.3-0.2-14.6-8.4-17.1C259.3,328.7,253.4,327.7,247.5,326.7z"/>

                                                <path data-val="Thies"  id="dataThies" class="btn-load" data-chart="dataThies" title="<strong><u> Thiès </u> </strong> <br> Total: <?= $nDeclarationsThies ?>  <br> Validées: <?= $nDeclarationsTThies ?> <br> Encours: <?= $nDeclarationsECThies ?>" d="M101.3,417.4c-6.2,4.3-10.5,7.4-14.3,10c2.4,7.5,3.1,15.8,7.3,21.6c6.1,8.5,0.3,28.7,21.4,23.3c0.2-0.1,0.5,0.3,0.8,0.5
                                                    c10.3,7.6,21.3,14.3,26.5,27.3c3.5,8.7,8.6,10,16.1,4.5c7.7-5.6,16.9-10.6,17.8-22.1c0.1-1.5,1.9-4.1,3-4.1c6.5-0.3,13.9-2,19.3,0.5
                                                    c7.1,3.3,10,0.6,11.2-4.7c2-8.9,6.1-18.7-3.8-26.2c-0.9-0.7-2-2.7-1.6-3.2c5-7.9-2.8-14.2-1.7-21.2c-8.6-2.4-13.6,2.1-19,6.9
                                                    c-2.9,2.6-7.3,4.5-11.2,5c-6.1,0.8-13-1.4-18.4,0.7c-9.6,3.6-21-1.1-29.4,7.7c-3.5,3.7-16.3-2.4-17.8-7.4
                                                    C105.8,430.4,103.7,424.5,101.3,417.4z"/>

                                                <path data-val="Rufisque"  id="dataRufisque" class="btn-load" data-chart="dataRufisque" title="<strong><u> Rufisque </u> </strong> <br> Total: <?= $nDeclarationsRufisque ?>  <br> Validées: <?= $nDeclarationsTRufisque ?> <br> Encours: <?= $nDeclarationsECRufisque ?>" d="M57.6,463.5c11.9,8.5,23.7,16.8,35.1,25c8.8-20.6-0.5-39-9-57.8c-10.6,5.6-20.3,10.6-29.4,15.4
                                                    C55.5,452.7,56.7,458.7,57.6,463.5z"/>

                                                <path data-val="Pikine"  id="dataPikine" class="btn-load" data-chart="dataPikine" title="<strong><u> Pikine </u> </strong> <br> Total: <?= $nDeclarationsPikine ?>  <br> Validées: <?= $nDeclarationsTPikine ?> <br> Encours: <?= $nDeclarationsECPikine ?>" d="M45.4,449.1c1.6-0.7,3.3-1.4,5.1-2.2c1.6,5,3,9.5,4.5,14.2c-8.6,1-14.9,1.7-21.4,2.4c-1.2-2.5-2.4-5-3.7-7.8
                                                    c1.4-0.6,2.8-1.2,4.2-1.8l2,1.3h1.4h1.2l2.6-0.8l1.1-1l2-0.5l0.6-1l0.9-1.7L45.4,449.1z"/>

                                                <path data-val="Dakar"  id="dataDakar" class="btn-load" data-chart="dataDakar" title="<strong><u> Dakar </u> </strong> <br> Total: <?= $nDeclarationsDakar ?>  <br> Validées: <?= $nDeclarationsTDakar ?> <br> Encours: <?= $nDeclarationsECDakar ?>"   d="M24.8,475.9c0.9-1.6,2.6-3.4,2.8-5.3c0.4-4.1,0.1-8.2,0.1-12.4c-4.6,0.3-9.2,0.3-13.7,1.2
                                                    c-0.7,0.1-1.8,5.1-0.8,6.2C16.3,469.2,20.3,472.1,24.8,475.9z"/>

                                                <path data-val="Tambacounda"  id="dataTambacounda" class="btn-load" data-chart="dataTambacounda" title="<strong><u> Tambacounda </u> </strong> <br> Total: <?= $nDeclarationsTambacounda ?>  <br> Validées: <?= $nDeclarationsTTambacounda ?> <br> Encours: <?= $nDeclarationsECTambacounda ?>"  d="M828.9,537.5c11.2,2.3,23.2,2.5,34.7,3.5c9.5,0.8,19.1-0.1,28.6,0.4c2.5,0.1,7,2.7,7,4.2c0,7.5,1.1,16.2-2.3,22.1
                                                    c-6.7,11.9-16.8,21.8-24.2,33.3c-5.3,8.2-9.6,17.4-12.4,26.8c-1.9,6.2-1.5,13.6-0.1,20.1c2,9.7,9.4,13.1,19.7,10.9
                                                    c4.2,11.6,19.1,11.8,26.2,0.6c7.8,6.9,16.2,13.4,23.4,21c9.1,9.6,6.5,17.9-5.5,23.4c-5.2,2.4-10.4,5.8-14.1,10.1
                                                    c-1.8,2.1-1.4,8.8,0.5,10.6c5.3,5,11.8,8.9,18.2,12.7c2.6,1.5,6,1.5,7.2,1.8c2.1,9.8,4.1,18.8,6,27.9c0.8-1,1.6-1.9,2.4-2.9
                                                    c4,0,8-0.2,11.9,0.1c4.2,0.3,7.6,1.9,11.2-3.4c4.7-7,22-7.4,27.1-0.7c3.8,5,4.9,12.2,6.5,18.6c1.5,6,2.8,7.5,8.3,2.8
                                                    c7.9-6.7,16.9-12.1,25.2-18.4c3.6-2.8,6.2-7,9.8-9.7c7.4-5.5,14.9-10.9,23-15.3c2.3-1.3,7.3-0.3,9.6,1.5c6.7,5,12.5,11.2,19,16.5
                                                    c4.9,4.1,10.7,7,15.1,11.5c6.1,6.3,11.9,13,16.3,20.5c2.6,4.4,2.6,10.6,3.3,16c0.5,3.7,0.1,7.4,0.1,11.3c-6.3,4.9-12,7-20,2
                                                    c-7.4-4.7-20.9,6.2-23.2,14.2c-1.6,5.6-8.1,10-13,14.3c-6.1,5.3-12.7,9.9-20.4,15.6c-4.9-3.3-12.7-7.2-18.9-12.8
                                                    c-8.2-7.3-16-3.6-24-1.3c-5.2,1.5-10.1,4.9-15.3,5.6c-3.9,0.6-8.4-1.3-12.3-2.9c-7.8-3.2-13.6-1.9-17.6,6c-0.7,1.3-2.9,1.9-4.2,2.6
                                                    l4.3,2.5l3.1,2.2l2.5,2.8l2,2.3l2.6,4.1l2.8,3.3l3.4,0.3l2.8,0.9l1,5.4l0.7,2.1l-2,1.7l0.4,3.7l0.1,1.8l0.1,1.8l1.3,1.4l2,1v1.7
                                                    l-0.3,2.1l-3.3,1.8c1.9,0.3,3.5,6.3,4.3,9.8c1.6,7.4,2.4,14.9,3.8,24.1c-5.5,2.6-11.6,6.5-18.3,8.1c-4.4,1-9.8-2.5-14.4-1.9
                                                    c-13.6,1.9-26.2-3.5-39.4-4.6c-5.7-0.4-12-13.6-11.5-21.7c0.3-4.3-0.6-8.6-0.9-12.9c-0.1-1.6-0.6-3.7,0.1-4.8
                                                    c6.2-8.9,1.9-14.9-5.2-20.2c3.2-7.4-0.9-20-6-26.2c-7.6-9.2-18.8-17.2-14.1-32.5c2.2-7.1-7.4-19.4-15.4-23.3
                                                    c-0.5-7.4-0.9-14.2-1.4-22.4c-6,0.3-12.1,0.7-18.8,1.1c1.4-4.2,2.7-8,3.8-11.8c0.9-3.1,1.4-6.4,2.1-9.5c-4-0.7-8-1.4-12.1-1.9
                                                    c-0.7-0.1-1.6,0.9-3.5,2.1c1.4-6.1,2.6-11.3,3.8-16.4c-9.9-6.6-11.3,3.7-15,7.3c-6.2-4.5-11.5-8.9-17.5-12.3
                                                    c-3.1-1.8-7.6-3.2-10.8-2.3c-19.4,5.5-40.4,3.9-57.8,18.4c-20.4,17-48.8-1.7-54.3-19.7c-1.2-3.8-3.5-7.2-6.5-9.9l-3.1-2.3l13.5-11.5
                                                    v-24.9l7-1.8l3.2-1.4l3-0.2l2.5,0.2l3.5,1.1l3.2,1.9l1.2,2.3l2.7,3.2l1.2,3.7l4.7,7.1c0,0,4.7,20.5,28.9-8.7l2.8-9.9l2.5-4.2
                                                    l-0.5-10.6l-1.4-5.7l-3.9-0.5l-0.2-10.4l3.2-7.4l0.2-3.4l0.7-3.5l2.1-3l2.3-2.5l3.9-1.6l2.3-0.5l1.8-1.1l1.2-1.2h1.6l1.7,0.4
                                                    l1.9,0.1l2.5-0.2l1.6-0.9c0,0,0.8-1.6,0.8-1.8c0-0.2,0-1.9,0-1.9l-0.3-2.1l-1.1-4.3l-1.4-1.1h-1.5l-2.1-3v-3.9v-2.9v-5.1l0.7-6
                                                    l1.4-4.8l2.6-4.5c0,0,2.2-3.8,8.2-4.6l3.6-3.4l1.7-2.1l1.4-2.4l1-3.8l-0.7-4.8v-2.6l1-2.1l2.1-0.5h1.9l2.6-1.4l3.6-2.4l1-3.1
                                                    l-0.7-2.6l1-3.4l3.3-0.7l4.6,0.3l3.8-0.7l6.2-3.6L828.9,537.5z"/>

                                                <path data-val="Bounkiling"   id="dataBounkiling" class="btn-load" data-chart="dataBounkiling" title="<strong><u> Bounkiling </u> </strong> <br> Total: <?= $nDeclarationsBounkiling ?>  <br> Validées: <?= $nDeclarationsTBounkiling ?> <br> Encours: <?= $nDeclarationsECBounkiling ?>" d="M515.5,835.7c-1.6-7.1-4.8-13.6-14.7-16.2c-10.6-2.9-12.7-11.4-8.7-21.6c1.5-3.9,0.8-8.6,1-12c-5.1-1.6-8.4-2.5-11.6-3.6
                                                    c-13.1-4.3-26.2-12.9-38.7,1.9c-1.5,1.8-7.4,1.5-10.4,0.2c-8-3.3-15.5-4.3-23.8-1.2c-4.9,1.9-10.3,2.7-15.7,4.1
                                                    c-0.7,12.8-1.4,25.6-2.2,39c-11.2,0-22.8-2-24.4,13.3c2.5,1.3,4.9,2.5,8.6,4.3c-7.4,12.1-14.1,23.1-20.7,34.1l-0.4,1.2
                                                    c0,0-0.9,1.8,1.8,3.6h9.7l5.2-9.8l11.6-11.2l11.6-5.6l13.5,4.2l7.1-4.2l8.4-9.1l13.5-9.1l23.9-2.8h22.6h18.1l12.7,2.9l2.4,0.5v-1.8
                                                    L515.5,835.7z"/>

                                                <path data-val="Bignona"  id="dataBignona" class="btn-load" data-chart="dataBignona" title="<strong><u> Bignona </u> </strong> <br> Total: <?= $nDeclarationsBignona ?>  <br> Validées: <?= $nDeclarationsTBignona ?> <br> Encours: <?= $nDeclarationsECBignona ?>" d="M269.8,966.6c1.1-15.4,7.8-16,17.5-10.6c1.3,0.8,4-1.2,6.1-1.5c5-0.6,10.4-2.4,14.9-1.1c12.8,3.8,22.1-5.3,33-8
                                                    c1.2-0.3,1.8-4.2,2.3-6.6c1.8-8.2,4.6-16.4,5-24.7c0.4-10.5,9.8-20.7,1.2-31.5c-0.8-1,0-3.8,0.9-5.3c6.6-10.6,13.4-21.1,19.4-30.5
                                                    c-2.9-3.7-6.7-6.4-6.7-9.1c0-3.5,3.1-7,5.1-11.1c-59.4,0-117.6,0-176.3,0c-3.2,7.6-8.2,14.9-9.3,22.8c-2.9,20-2.6,40.5-6.5,60.3
                                                    c-2.5,12.8,1,24.8,0.3,37.1c-0.2,4.5,0.7,9,1.3,16.4c11.4-8.7,20.4-16.6,30.4-22.7c4.5-2.7,11.9-3.7,16.9-2.1
                                                    c8,2.5,15.3,7.4,22.5,12C254.6,954.7,260.9,960,269.8,966.6z"/>

                                                <path data-val="Velingara"  id="dataVelingara" class="btn-load" data-chart="dataVelingara" title="<strong><u> Vélingara </u> </strong> <br> Total: <?= $nDeclarationsVelingara ?>  <br> Validées: <?= $nDeclarationsTVelingara ?> <br> Encours: <?= $nDeclarationsECVelingara ?>" d="M743.8,904c2.1,4,14.9,8.9,17.5,6c1.8-2,2.2-6.5,1.3-9.3c-1.7-5.4-5.1-10.2-6.8-15.6c-0.3-0.9,4.8-3.3,7.2-5.2
                                                    c2.9-2.2,8.3-5.8,7.9-6.7c-2.6-5.7-0.4-14.4-10-16.2c-10.1-1.8-20.2-4.3-30-7.2c-2.2-0.6-4.7-4.2-5-6.6c-0.7-5.5,0.7-11.3-0.2-16.7
                                                    c-0.8-5.2-3.4-10-5.8-16.8c5.1,0,9.3-0.4,13.5,0.1c6.1,0.7,12.2,2.9,18.3,3.1c12.1,0.5,14.1-1.4,17.1-12.6
                                                    c14.5-2.9,28.8-6.3,43.3-8.5c16-2.5,27.3-13.5,27.9-28.8c0.4-10,3.9-11.8,12.5-7c-2.8,6.5-5.7,12.9-9.5,21.6
                                                    c7.9-0.4,13.8-0.6,20.7-0.9l2.9,10.3c-0.9,2.2-2,3.8-2.2,5.4c-0.2,2.6-0.8,6.9,0.4,7.7c16,9.8,15.9,9.7,13.4,30
                                                    c-0.3,2.8,1.7,6.4,3.7,8.8c4.7,5.7,10.8,10.4,14.8,16.4c2.9,4.4,6.3,11.8,4.5,15.5c-3.9,8.3,0.8,10.8,4.6,12.9
                                                    c-1,18.5-3,35.9,5.7,51.3c-62.9,0-125.7,0-188.6,0l8.7-7.2l1.8-12.6L743.8,904"/>

                                                <path data-val="Foundiougne"  id="dataFoundiougne" class="btn-load" data-chart="dataFoundiougne" title="<strong><u> Foundiougne </u> </strong> <br> Total: <?= $nDeclarationsFoundiougne ?>  <br> Validées: <?= $nDeclarationsTFoundiougne ?> <br> Encours: <?= $nDeclarationsECFoundiougne ?>" d="M286.1,631.6c4-3.7,6.9-5.9,9.1-8.7c1.9-2.3,4.2-5.2,4.2-7.9c0.1-7.7-16.4-19.5-24.4-18.5c-6.7,0.8-13.3,7.2-20.3,0.4
                                                    c-0.4-0.4-2.5,0.4-3.3,1.2c-12,10.4-24.5,20.4-35.7,31.7c-5.2,5.2-9,7.6-16,4.2c-6.4-3.1-17.2,2-16.8,8.4c1,13-3.8,27.7,8.2,37.8
                                                    c10.1-12.1,24.6-15.3,38.5-19.3c1-0.3,3.1,2.8,5.5,5.1c-5.5,2.5-9.4,4.2-13.3,6c-14.2,6.6-20.7,27-11.9,38.1
                                                    c2.9,3.6,6.5,6.8,10.2,9.4c3.5,2.5,7.7,5.7,11.6,5.7c30.1,0.5,60.2,0.3,90.4,0.2c2.2,0,4.5-0.4,6.7-0.6c-9.6-3.4-13.2-9.2-12.9-19.2
                                                    c0.2-7.4-2.3-13.7-10.6-18c-12.7-6.7-12.8-14.2-3-25c1.7-1.8,3.8-3.9,4.1-6.1C308.3,643,302.3,635,286.1,631.6z"/>

                                                <path data-val="Fatick"  id="dataFatick" class="btn-load" data-chart="dataFatick" title="<strong><u> Fatick </u> </strong> <br> Total: <?= $nDeclarationsFatick ?>  <br> Validées: <?= $nDeclarationsTFatick ?> <br> Encours: <?= $nDeclarationsECFatick ?>" d="M265.4,590.6c0.1-1,0.2-2,0.4-2.9c0.9-3.7,1.5-8,3.8-10.8c6.3-7.7,13.3-14.8,20.3-21.8c1.6-1.6,4.5-2.4,6.9-2.9
                                                    c10.4-2,20.9-3.7,31.4-5.6c0-1.3,0-2.6,0-3.9c-6.2-1-12.4-2.3-18.7-2.9c-9.6-1-11.6-3.4-8.1-11.9c2.8-6.8,6-13.5,9.7-21.7
                                                    c-5.4-1.8-12-6.6-16.1-5c-14.3,5.6-25.9-5.1-39-4.6c-6.8,0.3-13.6,0.6-22,1c0.1,19.8-12.5,31.4-29.8,38.3c-7.5,3-11.1,7.3-12.6,14.8
                                                    c-1.5,7.1-2.3,15.2-6.4,20.6c-3.8,5.1-3.1,7.7-0.4,11.9c1.3,2,2,4.3,2.2,4.6c-4.4,6.5-7.9,11.6-11.7,17.3
                                                    c12.9,9.2-1.7,20.1,1.5,30.9c10.7-7.5,21.4-7.7,31.4-2.6c10.9-10.7,20.4-21.4,31.4-30.1c5.2-4.1,10.6-11.9,19-8.3
                                                    C258.5,594.8,268.2,599.9,265.4,590.6z"/>

                                                <path data-val="NioroduRip"  id="dataNioroduRip" class="btn-load"  data-chart="dataNioroduRip" title="<strong><u> Nioro du rip </u> </strong> <br> Total: <?= $nDeclarationsNioroduRip ?>  <br> Validées: <?= $nDeclarationsTNioroduRip ?> <br> Encours: <?= $nDeclarationsECNioroduRip ?>" d="M306,662.9c-10.2,11-8.9,17,3.2,24.4c3.8,2.3,8.7,7.3,8.5,10.8c-0.5,13.8,5,22,18.3,25.1c3.3,0.8,6.6,2,9.9,2.1
                                                    c24.5,0.2,49.1,0.1,73.6,0.1c14,0,27.9,0,38.7,0c6.4-11.8,12-21.5,16.8-31.5c1.6-3.4,1.8-7.7,1.7-11.5c0-1.1-3.4-2.7-5.3-3
                                                    c-9.9-1.1-19.8-1.8-31.3-2.8c-1.5,1.9-4.7,6.1-8.2,10.6c-5.4-5.8-11.2-10.3-14.7-16.1c-7.4-12.5-20.2-14.8-31.6-14.3
                                                    c-16.8,0.8-33.3,6.6-50.1,8.7C326,666.9,315.2,673.3,306,662.9z"/>

                                                <path data-val="Mbour"  id="dataMbour" class="btn-load"  data-chart="dataMbour" title="<strong><u> Mbour </u> </strong> <br> Total: <?= $nDeclarationsMbour ?>  <br> Validées: <?= $nDeclarationsTMbour ?> <br> Encours: <?= $nDeclarationsECMbour ?>" d="M143,511.2c-4-24.3-20.2-34.6-43.7-37c-0.4,2.9-0.6,6.3-1.5,9.5c-5.2,18.5,9.1,29.4,16.5,43c0.4,0.7,1.5,1.1,2.4,1.5
                                                    c13,7.9,24.2,16.9,23.8,34.1c0,1.4,0.8,2.9,1.5,4.1c3.8,6.3,7.7,12.5,11.7,18.7c1.6,2.6,2.9,6.4,5.2,7.3c8.1,2.9,9.8,9.9,12.1,16.6
                                                    c1.3,3.7,2.1,7.6,3.4,12.1c7.1-7.8-1.5-12.4-2.4-18.1c3.6-5.2,7.3-10.4,11.1-16c-1.7-3.5-3.6-7.4-4.7-9.7c2.9-6.4,6.7-11.3,7.1-16.5
                                                    c1.3-17.3,11.5-27.9,26-33.1c17-6.1,16.9-19.7,19.9-32.5c-7.4-1.7-15.6-1.2-19.7-5.1c-9-8.5-18.6-10.5-29.9-9
                                                    C177.2,499.5,162.1,511.2,143,511.2z"/>

                                                <path data-val="Kaolack"  id="dataKaolack" class="btn-load"  data-chart="dataKaolack" title="<strong><u> Kaolack </u> </strong> <br> Total: <?= $nDeclarationsKaolack ?>  <br> Validées: <?= $nDeclarationsTKaolack ?> <br> Encours: <?= $nDeclarationsECKaolack ?>" d="M292.9,629.2c12.1,2,16.1,9.9,15.9,21.1c-0.1,3.4,0.5,7.1,1.7,10.3c0.8,2,3.2,5,4.8,4.9c6.7-0.4,13.4-1.5,20-2.7
                                                    c16.7-3,33.3-6.5,50.1-9.2c3.8-0.6,8.1,1,12,2c4.3,1.1,8.4,3.3,12.7,3.9c2.3,0.3,6-1,7-2.7c1-1.8,0.2-6.6-1.2-7.3
                                                    c-14.2-7.3-16.4-26.3-31.7-32.3c-0.5-0.2-1.3-0.8-1.3-1.3c-1.8-12.1-10-7-16.7-6.3c-2.4,0.2-4.9,0.3-7.4,0
                                                    c-15.7-1.4-22.8-17.3-36.6-22c-0.6-0.2-1.2-1.6-1.2-2.5c-0.1-7.4-0.3-14.9,0-22.3c0.2-3.8,1.3-7.5,2-11.9c-6.2,1-10.9,1.6-15.5,2.7
                                                    c-5.4,1.3-11.7,1.8-15.7,5c-7.5,6-13.9,13.4-20.1,20.8c-2,2.4-2.7,6.7-2.3,9.9c0.2,1.6,4.4,3.4,6.9,3.7c7.3,0.7,24.3,12.6,26.1,19.7
                                                    c0.6,2.6-0.5,6.2-1.9,8.7C298.7,624.2,295.8,626.3,292.9,629.2z"/>

                                                <path data-val="Mbacke"   id="dataMbacke" class="btn-load"  data-chart="dataMbacke" title="<strong><u> Mbacké </u> </strong> <br> Total: <?= $nDeclarationsMbacke ?>  <br> Validées: <?= $nDeclarationsTMbacke ?> <br> Encours: <?= $nDeclarationsECMbacke ?>" d="M328.4,412.9c0,3.3,0,6.4,0,9.5c-0.1,11.8-1,23.6-0.2,35.3c0.5,7.4,1.6,16.1,5.8,21.6c7.8,10.2,5.8,21.1,5.8,32.8
                                                    c3.2,0,6.3,0.5,9.2-0.1c8.4-1.7,16.7-3.6,24.9-5.7c14.4-3.6,19.9-12.5,18.1-27.3c-0.6-4.6-0.3-10.7,2.3-14.1
                                                    c5.8-7.7,13.4-14,20-21.1c1.3-1.4,1.6-3.9,1.9-4.8c-6.9-5.2-14.5-8.8-18.9-14.8c-4.5-6.2-5.9-14.6-9.7-24.7
                                                    c-8.4,2.7-16.3,5.8-24.5,7.6C352.2,409.4,341,410.8,328.4,412.9z"/>

                                                <path data-val="Bambey"  id="dataBambey" class="btn-load"  data-chart="dataBambey" title="<strong><u> Bambey </u> </strong> <br> Total: <?= $nDeclarationsBambey ?>  <br> Validées: <?= $nDeclarationsTBambey ?> <br> Encours: <?= $nDeclarationsECBambey ?>" d="M198.5,418.7c4.9,1.5,7.7,2.4,10.7,3.4c-5.1,8.2,6.9,15.8,0,23.6c-0.1,0.1,0.1,0.7,0.4,0.8c12.5,9.9,6.7,21.6,2.6,32.5
                                                    c-2.7,7.1,0.8,10.2,6.2,10.9c14.3,1.8,28.7,3.1,43.1,3.7c3.7,0.1,9-2.3,10.9-5.3c3.9-6.2,6-13.6,9.1-21.2
                                                    c-13.1-13.6-15.4-32.2-14.6-52c0.2-4.4,0.9-11.5-1.4-12.9c-4.7-2.8-11.8-4.7-16.9-3.3c-13.9,3.8-27.4,9.3-41,14.4
                                                    C204.9,414.3,202.6,416.3,198.5,418.7z"/>

                                                <path  data-val="Diourbel"  id="dataDiourbel" class="btn-load" data-chart="dataDiourbel" title="<strong><u> Diourbel </u> </strong> <br> Total: <?= $nDeclarationsDiourbel ?>  <br> Validées: <?= $nDeclarationsTDiourbel ?> <br> Encours: <?= $nDeclarationsECDiourbel ?> "  d="M337.2,506.8c-0.4-5.8,0-12.4-1.6-18.4c-2.7-9.6-7.9-18.5-9.9-28.2c-2.1-10.6-1.8-21.7-2.4-32.5c-0.2-4.6,0-9.3,0.1-14
                                                    c-6.1,0.1-12.6-1.3-18,0.6c-5.3,1.9-7.9,1-11.7-2.2c-6.1-5.2-12.3-11.4-24.4-6.3c1,10.3,2.6,21.2,3,32.1
                                                    c0.4,10.6,3.4,19.6,11.6,26.8c1.4,1.2,1.8,6.5,1,7c-6.9,4.3-9.1,10.6-9.6,18.1c-0.2,2.8-1,5.6-1.5,8.4c3.3,0.5,6.5,1.2,9.8,1.4
                                                    c6.1,0.3,12.5-1,18.2,0.5C313.4,502.9,323.7,511.6,337.2,506.8z"/>

                                                <path data-val="Ziguinchor"  id="dataZiguinchor" class="btn-load" data-chart="dataZiguinchor" title="<strong><u> Ziguinchor </u> </strong> <br> Total: <?= $nDeclarationsZiguinchor ?>  <br> Validées: <?= $nDeclarationsTZiguinchor ?> <br> Encours: <?= $nDeclarationsECZiguinchor ?> "  d="M367.9,988.1c-2.1-5.4-3.9-9.5-5.3-13.7c-2.1-6-5-12.1-5.5-18.3c-0.6-6.8-5.3-12.2-10.9-9.7c-14.1,6.2-27.6,16.1-44.3,8.2
                                                    c-0.7-0.3-2.2-0.2-2.8,0.3c-7.2,6.4-14.9,4.3-20.6,1.5c-4.5,4.4-8.1,10.6-11.5,10.5c-6.5-0.1-13-3.8-19.5-6.1
                                                    c-4.6-1.6-9.1-3.3-15.3-5.6c0.5,4.5,0.7,6.8,1,9.2c8.7-1.4,7,8.5,10.8,10.3c8.4,4,6.3,7.9,4.5,12.9c1.9,6.9,3.8,13.7,6,21.5
                                                    c4.9-2.1,9.1-4,13.4-5.8c15.4-6.7,28.2-18.8,47.4-17.9c13,0.6,25.9-3.3,38.8,2.6C357.7,989.5,362.9,988.1,367.9,988.1z"/>

                                                <path data-val="Oussouye"  id="dataOussouye" class="btn-load" data-chart="dataOussouye" title="<strong><u> Oussouye </u> </strong> <br> Total: <?= $nDeclarationsOussouye ?>  <br> Validées: <?= $nDeclarationsTOussouye ?> <br> Encours: <?= $nDeclarationsECOussouye ?> " d="M184.8,1014.2c0.8-0.7,1.6-1.5,2.3-2.2c6-1.1,11.9-3,18-3.1c14.7-0.5,29.4-0.2,45.1-0.2c-2.6-10.6-4.8-19.3-7.3-29.6
                                                    c-4.9-0.8-3.4-12.7-12.5-13c-0.5,0-0.6-3-1.4-4.2c-3.1-4.4-6.4-8.7-9.6-13.1c-3.7,3-7.2,6.2-11,8.9c-5.2,3.7-10.9,10.6-15.8,10.1
                                                    c-12.9-1.2-21.4,3.3-19.3,16C174.8,994.4,180.8,1004.2,184.8,1014.2z"/>

                                                <path data-val="Guinguineo"  id="dataGuinguineo" class="btn-load" data-chart="dataGuinguineo" title="<strong><u> Guinguinéo </u> </strong> <br> Total: <?= $nDeclarationsGuinguineo ?>  <br> Validées: <?= $nDeclarationsTGuinguineo ?> <br> Encours: <?= $nDeclarationsECGuinguineo ?> " d="M399.9,531c-6.9,7-12.1,16.1-7.8,27.4c1.1,2.8,0.1,6.7-0.8,9.8c-2.9,10.3-6.5,20.3-9.4,30.6c-1.2,4.4-2.6,6.4-7.9,7.4
                                                    c-16.7,3.2-29.5-1.8-39.3-15.5c-0.9-1.2-2.3-2.9-3.5-3c-11.7-0.6-9.4-10-8.9-16.2c0.6-7,3.3-14.2,6.7-20.3l2.3-0.4h2.3h3.4l3.8-1
                                                    l2.4-2.2l1.9-5.6l-0.4-2.5l-0.4-4.3l2.1-1.6h3.1l2.1,3.1l0.2,3.1v2.9l-0.4,2.1l7.6,1l2.7,1.7l1.6,1.9l2.7,1.4h4.7l4.9-6.2v-8.8
                                                    l7.8-4.8l8.6-4.2l10.4,1L399.9,531"/>

                                                <path data-val="Linguere"  id="dataLinguere" class="btn-load" data-chart="dataLinguere" title="<strong><u> Linguère </u> </strong> <br> Total: <?= $nDeclarationsLinguere ?>  <br> Validées: <?= $nDeclarationsTLinguere ?> <br> Encours: <?= $nDeclarationsECLinguere ?> " d="M440.8,189.7c0,5.3,0.4,8.9-0.1,12.3c-1.6,12.7,1.6,25.9-6.3,37.9c-9.7,14.8-18.8,30.2-27,45.9
                                                    c-7.3,14.1-19.4,26.3-19.7,43.5c-0.2,12-0.3,24.1,1.1,35.9c2,16.6-1.4,33.7,6.7,50c5.6,11.2,10.6,18.1,22.5,17
                                                    c9.7-0.9,18,1.7,26.6,5.4c3,1.3,7.4,2.3,9.9,1c8.1-4.2,12.5,0.3,16.9,5.7c7.5,9,14.7,18.3,22.6,27c3.2,3.5,7.6,7.1,12.1,8.3
                                                    c14,3.8,28.2,7.3,42.6,9.3c8.9,1.2,18.6,0.6,27.1-2c3.9-1.2,5.7-9,8.5-13.8c0.9-1.5,2.1-2.9,5.1-7c2.6,3.9,4.6,6.7,6.4,9.7
                                                    c5.1,8.5,13,12.2,22.4,14.2c8.6,1.9,17.3,4.3,25.5,7.5c4.8,1.9,8.8,5.8,15.1,10c23.9-5.7,24-5.7,32.3-14.6c3.9-4.2,6.9-8.9,14.2-8.5
                                                    c3.3,0.2,8-3.2,10.1-6.3c12.3-18,11.6-23.2-0.9-41.8c-5.1-7.6-14.3-13.9-10.3-25.8c0.7-2.1-3.4-6.6-6.3-8.8
                                                    c-9.2-7.2-18.5-14.2-28.4-20.5c-5.6-3.6-5.6-7.5-4.1-12.9c4.1-14.6,8.6-29.2,11.8-44c1.5-7.3,4.5-10.4,11.8-10.7
                                                    c7-0.3,10.2-4,8.4-11.3c-0.8-3.5-1.3-7-1.9-10.1c12-3,23.3-5.8,34.9-8.7c0.3-1.9,0.4-4.5,1-6.9c2.9-11.8,1.5-15.6-8.7-21.5
                                                    c-11.5-6.7-23.2-13.1-34.5-20.1c-3.8-2.3-7.1-5.9-9.8-9.4c-14.1-18.5-26-39.6-54-39.1c-3.4,0.1-7-2.7-10.4-4.4
                                                    c-8.5-4.2-16.5-8.5-26.8-8.2c-25.5,0.8-50.9,0.2-76.4,0.2c-10.4,0-20.9,0-31.2,0c-1.3,5.2-2.4,9.9-3.5,14.4
                                                    C464,188.7,452.9,189.2,440.8,189.7z"/>

                                                <path data-val="Salemata"  id="dataSalemata" class="btn-load" data-chart="dataSalemata" title="<strong><u> Salémata </u> </strong> <br> Total: <?= $nDeclarationsSalemata ?>  <br> Validées: <?= $nDeclarationsTSalemata ?> <br> Encours: <?= $nDeclarationsECSalemata ?> " d="M998.2,945.7 1005.2,945.7 1008,955.3 1003.1,962.8 1008,977.2 1017.1,980.6 1028.3,980.6 1033.2,971 1033.2,963.1 
                                                    1043.7,963.1 1054.9,971 1054.9,978.5 1066.8,980.6 1071.7,987.4 1083.6,987.4 1095.5,987.4 1103.9,987.4 1110.2,1002.5 
                                                    1116.5,998.4 1126.3,995 1134,995 1143.1,997.7 1150.1,1005.2 1150.1,993.6 1150.1,986.8 1143.1,986.8 1136.8,984.7 1122.8,975.5 
                                                    1122.1,967.6 1120.7,957.3 1117.9,945.7 1108.8,941.6 1108.8,931.3 1103.9,923.1 1090.6,923.1 1083.6,923.1 1078,929.3 
                                                    1068.2,926.5 1061.2,923.1 1054.9,917.6 1050,909.4 1043,909.4 1033.9,914.9 1024.8,909.4 1022,899.8 1013.6,903.9 1009.4,918.3 
                                                    1005.2,926.5 998.2,932 991.9,941.6z"/>

                                                <path data-val="Saraya"  id="dataSaraya" class="btn-load" data-chart="dataSaraya" title="<strong><u> Saraya </u> </strong> <br> Total: <?= $nDeclarationsSaraya ?>  <br> Validées: <?= $nDeclarationsTSaraya ?> <br> Encours: <?= $nDeclarationsECSaraya ?> " d="M1219.2,783.1l9.3,0.7v9l7.9-0.7l10.1-9l10.8-2.1h6.5l8.6,10.3h8.6l4.3-17.9h10.8l7.9,3.4h6.5l4.3,6.2l8.6,5.5l3.6,6.9
                                                    l6.5,3.4l2.9,13.1l5,10.3l2.2,9.6l5.7,6.9l7.2,0.7l7.2,9.6l3.6,13.8l9.3-0.7v16.5l-8.6,4.8l-2.9,9.6l7.2,4.8l4.3,20l-8.6,0.7l-5,8.3
                                                    l4.3,13.1l-0.7,16.5l7.2,14.5v10.3l-6.5,5.5l-9.3-1.4l-18.7-2.8l-10.1,4.1c0,0-12.2,4.8-14.4,4.8c-2.2,0-7.2,2.8-9.3,2.8
                                                    s-18.7,3.4-21.5,3.4c-2.9,0-17.9-5.5-17.9-5.5l2.9-6.2l13.6-20.7l12.2-14.5v-14.5l-9.3-10.3h-25.1h-16.5l-7.9-5.5
                                                    c0,0-4.3-7.6-5.7-9.6c-1.4-2.1-5.7-9-5.7-9h-11.5l-7.9-11.7l-6.5-13.1h-10.8l-10.8-12.4c0,0-11.5-3.4-13.6-3.4s-11.5,0-11.5,0v-7.6
                                                    v-9l-16.5-5.5l-5.7-8.3v-9l7.9-11.7l7.2-9.6l10.8,0.7l9.3,4.8l5-9.6l7.2-4.8h9.3l4.9-0.9l1.1-2.4l1.1-3.2l-3.8-1.9
                                                    c0,0-10.8-6.8-4.6-20.8C1184.9,762.8,1184.9,755,1219.2,783.1z"/>

                                                <path data-val="Kedougou"  id="dataKedougou" class="btn-load" data-chart="dataKedougou" title="<strong><u> Kédougou </u> </strong> <br> Total: <?= $nDeclarationsKedougou ?>  <br> Validées: <?= $nDeclarationsTKedougou ?> <br> Encours: <?= $nDeclarationsECKedougou ?> "  d="M1107.5,821.5h4.1l1.4,1.1l2.5,0.9l1,1.7l1.9,1.7h4.2l2-3.3c-0.5-4.5,2.6-2,2.6-2l1,12l6.2,8.7l3,1.3l4.1,0.7l3.2,1.1l4.3,2
                                                    l0.1,18.3l4.2-0.8h4.3l5.4-0.1l8.1,1.5l2.9,1.3l1.6,1l1.7,1.5l1.4,2l1.3,2l3.5,3.5l2.6,3.4l10.3-0.6l5.4,11.9l9.3,13.3l11-0.4
                                                    l10.7,17.9l6.7,5c1,1.1,3.9,1,3.9,1l38.2-0.4l8,7.1l1.2,3.4v5.5l-0.9,5.3l-1,2.9l-2.8,2.8l-3,3.8l-2.5,2.8l-3,3.4l-2.5,4.8l-1.9,3.1
                                                    l-1.6,2.4l-9.4,16.5l-2-1.1l-3.3-2.4l-2.9-0.7l-3.6,0.1l-2,1.3l-4.2,4.1l-3.6,0.4l-10,1.3l-1.6-3.2l-0.7-3.6l-2.3-0.6l-5.8-0.3
                                                    l-5.5,1.3l-4.8,2l-2.8,5.3l-4.5,3.1l-3.3,3.2l-1.7,0.6l-7.7,0.7l-8-0.1l-3.6,2.2l-3.5,1.8l-3.5,3.2l-3.3,2.5l-3.6,1h-3.6l-3.2-0.6
                                                    l-1.2-2.5l-1.3-2.4l-1-1.7v-3.6v-5.3v-4v-4.2v-2.7v-3.2h-1.4h-2.3l-2-0.7h-2.5h-1.9l-2.2-1.3l-2.2-1.1l-1.9-1.1l-3.5-2l-3.5-2.1
                                                    l-2.5-1.5l-2-1.3l-0.3-2l-0.3-3.1v-3.4l-0.3-3.4l-0.7-4.1l-0.9-3.1l-0.4-2.6l-1.2-3.9l-0.1-2.2v-1.8l-9.4-4.3l-0.4-8.8l-0.4-1.7
                                                    l-4.3-7.7l-2.3-1.1l-22.2,0.1l-4.3,5h-2.2l-3.8-1.1l-2.9-1l-5.4-2.7l-3.6-2.1l-2.8-2.4l-2-2.9l-1.4-2.5l-1.6-2.8l-2.2-1.5l-3.2-0.7
                                                    l-3.6,0.6l-3,1.3l-3.8,2.8l-3.3,1l-5.5-2.9l-1.3-1.5l-1.2-2.4l-1.2-4.1l-0.4-2.5l-0.6-1.5h-1.7l-3.3,1.5l-4.2,2.4l-2.8,2l-1.4,3.5
                                                    l-0.6,3.4l-0.6,4.2l-1.7,4.3l-2.9,3.9l-1.6,2.7l-2.3,2.2l-1.9,1v-3.8l-4.1-1.1v-2l0.1-1.8l-0.4-4.8c-0.3-6.6-3.8-10.6-3.8-10.6
                                                    l1.4-4.2v-4.6l-3.5-1l-1.2-3.1v-2.9l2.5-1.3v-1.4v-1.8v-3.9l-0.9-2.9l-1.3-2.8l-4.8-0.7l-3-2.4l-1.9-2.4c0,0-2.6-2.8-2.9-3.2
                                                    c-2.9-3.2-8.4-5.6-8.4-5.6l2.8-1.3c-0.1-3.8,6.7-5.1,6.7-5.1c3.1-1.1,13.9,5.7,13.9,5.7s15.7-0.2,18.5-5.5c0,0,11.1-9.3,24.8-3.6
                                                    l2.3,2.2l2,1.4l3.8,3.8l5.7,3.8l1.3,1.3l3.3,1l2,2.7h4.8l0.5-4.8l26.4-21.9c0,0,6-3.7,5.6-8.3
                                                    C1090.9,830.2,1098.5,817.9,1107.5,821.5z"/>


                                                <path data-val="Goudomp"  id="dataGoudomp" class="btn-load" data-chart="dataGoudomp" title="<strong><u> Goudomp </u> </strong> <br> Total: <?= $nDeclarationsGoudomp ?>  <br> Validées: <?= $nDeclarationsTGoudomp ?> <br> Encours: <?= $nDeclarationsECGoudomp ?> " d="M543.4,892c2.1,7,0.7,14.1-4,24.2c-2.3,4.9-3,10.5-4.8,17.1c-18.7,5-37.1,16.1-54.3,29.4c-5.7,4.4-12.2,7.7-18.6,11
                                                    c-9.3,4.8-19.2,8.3-28.1,13.8c-13.3,8.1-26.9,4.3-40.6,3.2c-5.3-0.4-11-0.8-15.7-3c-3.8-1.8-6.5-6.1-9.4-9.6c-1.5-1.8-2.8-4-3.6-6.2
                                                    c-1.7-4.6-3.1-9.4-4.7-14.1c-1.5-4.5-2.7-9.3-5-13.4l2.9,0.6l6,1.9h1.8l2.7-0.2l2.7-0.3l1.4-0.9l1.3-1.1l1.5-1.5l2.8-1.7l2-1.2
                                                    l2.2-1.1l2.9-0.9l1.6-0.7l1.1-0.6l4.2,0.4l5,0.1l4.1-0.4l4.9,0.1l3.9,0.1l0.6,2l0.9,3.4l0.9,5l1.5,2.7c0,0,0.7,8.9,13.3,11.5
                                                    l3.2-2.9l2.1-2.8l1.5-4.5l0.2-2l3.4-4.4l2.9-2.8l1.1-1h6.8l3.6-0.1l-0.1-21.8l-15.6-0.3l-0.1-12.4l1.6-6l5.7,0.1l8.8-3.4v-6h5.5
                                                    l5.5,3.1l7.6,7.3l0.7,1.1h0.9l0.7-0.1l0.4-2.1l5-16.4h8.1l11.6,7.1l29.7-0.3h3.5l5.5,0.9l3.9,0.2l2.4-0.8L543.4,892z"/>

                                                <path data-val="Sedhiou"  id="dataSedhiou" class="btn-load" data-chart="dataSedhiou" title="<strong><u> Sédhiou </u> </strong> <br> Total: <?= $nDeclarationsSedhiou ?>  <br> Validées: <?= $nDeclarationsTSedhiou ?> <br> Encours: <?= $nDeclarationsECSedhiou ?> " d="M541.7,887.7c-1.6-3.3-4-6.6-7.2-10.4c-2.1-2.5-3.9-7.3-3-10.1c2-6-0.4-8.7-5.2-9.9c-6.6-1.8-7.9-5.7-9.1-12.2
                                                    c-0.3-1.4-0.5-2.9-0.8-4.4c3.1-1-16.4-4.1-16.4-4.1h-23.7h-9.2h-6.6L452,838l-10.3,0.7l-5.1,0.9l-2.3,1.1l-3.3,2.4l-3.4,2.6l-2,1.2
                                                    l-2.8,2.3l-3.4,3.9l-2.2,3.9l-8.8,5.6l-1.6-0.8l-1.4-0.7l-5.7-1.8l-3.8-1.4l-2.8-0.1l-4.9,2.2l-5.1,3.1l-11.2,10.3l-2.8,4.7
                                                    l-1.8,3.7l-0.7,2.7l-2.6,0.5l-4.1-0.5h-3.7l-2.9-2.4c7.2,13.5-0.8,25.1-4.1,37.4c-1.6,6-2.3,12.5-1.9,18.7l0.1,0.9l13.1,5.2h8.3
                                                    l8.3-5.2l9.7-3.7h14.5h8.3l4.8,12.7l2.8,6.7c0,0,9,13.4,15.2-6l7.6-9h10.4v-20.1h-7.6h-8.3v-13.4l2.1-6.7h6.2l7.6-3v-6.7h6.9
                                                    l6.2,3.7l7.6,6.7l6.2-17.9h9l11.8,6.7h9.7h8.3h13.1l11,1.2l0.7-0.1l1,0.1l1-0.5L541.7,887.7z"/>

                                                <path data-val="Kolda"  id="dataKolda" class="btn-load" data-chart="dataKolda" title="<strong><u> Kolda </u> </strong> <br> Total: <?= $nDeclarationsKolda ?>  <br> Validées: <?= $nDeclarationsTKolda ?> <br> Encours: <?= $nDeclarationsECKolda ?> " d="M723.2,846.6c1.4,4.3,5.1,6.5,10.9,8c9.3,2.4,18.5,5,27.8,7.5c0.4-0.7,0.9-1.5,1.3-2.2c1.8,5.1,3.7,10.1,5,13.8
                                                    c-6.1,3.5-14.6,6.1-14.6,8.8c0,7.7,3.4,15.3,5.9,24.5c-4.1-1.6-9.5-3.6-14.8-5.6c-5.4,4.6-12.2,9.6-18,15.5
                                                    c-1.6,1.6-0.2,6.1-0.2,9.6c-11.3,11.6-26.6,7.5-40.9,7.6c-44.9,0.3-89.8,0.1-134.7,0.1c-11.9,0-14.2-4.3-9.6-15.2
                                                    c3-7.2,5.3-15,6-22.7c0.4-4.5-1.6-10.3-4.7-13.7c-5.3-5.9-9.9-10.9-7.8-19.9c0.5-2.1-4.9-5.4-7.3-8.3c-2.3-2.8-5-5.5-6.3-8.7l-0.7-4
                                                    l1.9-1l2.4-1.4l1.1-2.6l2.5-2.9h4.7h2.4l2.6-2h4.2h3h3.5h4l3.5-0.7l2.9-0.7l3-2l1.1-1.7h2.9l4.7-1.4h3.5h3.1l3.1-1h2.6h3l1.7-1.5
                                                    h3.4l1.5,1.6l0.5,2.9l0.1,5.1v4.4l-0.5,3.6l-0.4,6.7l1.5,3.1l3.6,3.5l6.5-0.4l9-0.2l2.7-0.4l1.9-0.4l-0.1,3.2v3.2l-0.5,3.7l3.6,1.4
                                                    l1.9,1.2l4.2,1.8l3,1.3l2.6,0.1l3.6,0.4l6.1,0.4l7,0.2l6-0.6h5.7h2l5.5-6.6l5.2-4l2.2-3.4l3.5-5.4l1.1-2.4l5.4-0.6l6.5-0.2l4.7,0.1
                                                    c0,0,4.5,0.1,5,0.1c0.5,0,3.1,0.9,3.1,0.9s2,0,3,0s7-0.2,7.7-0.2s4.9-0.9,4.9-0.9l2.4-1.8L723.2,846.6z"/>

                                                <path data-val="MedinaYoroFoulah"  id="dataMedinaYoroFoulah" class="btn-load" data-chart="dataMedinaYoroFoulah" title="<strong><u> medina yoro foulah </u> </strong> <br> Total: <?= $nDeclarationsMedinaYoroFoulah ?>  <br> Validées: <?= $nDeclarationsTMedinaYoroFoulah ?> <br> Encours: <?= $nDeclarationsECMedinaYoroFoulah ?> " d="M524,837.7l3.9-5.7h6.5l5.8-1.7h6.5h5.2l7.1-3.5l3.9-3.2h8.1l6.8-1.3h6.1l5.2-1.7h3.9l2.9,1.7v6.9v6.5v11.4l3.2,2.4h4.8h8.1
                                                    h8.1v5.3v6.7l7.4,3.4l9,2h10.4h12.9l11.6-13.1l7.8-10.1h17l10.2,0.5l14.7-1l1.9-1.2l-0.1-4.3c1.2-7.4-2.2-16-5.2-23.5
                                                    c-1-2.5-6.9-3-10.4-4.7c-3.7-1.8-7.1-4.2-10.7-6.1c-11.6-6.3-23.3-12.5-35-18.6c-0.7-0.4-2-0.2-2.8,0.1c-13.4,6-24-2.3-31.4-10.1
                                                    c-8.5-9-19.5-10.1-28.9-12.5c-19.4-4.9-34.9-14-47.3-29.1c-3.2-3.9-5.7-5.7-11.1-2.7c-9,4.8-15.6,10.9-16.4,21.5
                                                    c-1.3,17.2-12.6,25.7-26.4,31.6c-0.9,5.4-0.7,11.2-2.9,15.6c-4.4,8.7-0.6,14.9,6.4,16.7c12,3,14.9,11.2,17.4,19.9l1.2,3.8
                                                    C519.4,839.7,520.4,841,524,837.7z"/>

                                                <path data-val="Koungheul"   id="dataKoungheul" class="btn-load" data-chart="dataKoungheul" title="<strong><u> Koungheul </u> </strong> <br> Total: <?= $nDeclarationsKoungheul ?>  <br> Validées: <?= $nDeclarationsTKoungheul ?> <br> Encours: <?= $nDeclarationsECKoungheul ?> " d="M570.5,494.3c9.5-1.2,17.6-6.9,20-22c13.8,26,41.5,21.1,63,32.5c-0.8,15.6-1.5,32.3-2.8,48.9c-0.5,6.1,1,10.2,5,15.2
                                                    c4.2,5.3,5.3,13.4,6.5,20.5c1.2,6.9-0.3,14.4,1.4,21.2c2.8,11.4-2.9,29.8-11.3,37.9c-7.1,6.9-12,15.8-18.7,23.1
                                                    c-2.5,2.8-7.3,3.6-11.1,5c-5,1.8-10.2,3.1-16.9,5.2c-15-3.5-32.3-8.3-50-11.1c-1.8-0.3-3.7,0.1-5.6,0.8l-3.1,3.2v-2.6v-2.1
                                                    c0,0,0-1.4,0-2.3s0-7.2,0-8.8c0-1.6,5.1-11.4,5.1-11.4V642c0,0,0.7-6.3,11.4-15.1c10.7-8.8,7.3-6.3,10.2-8.4
                                                    c2.9-2.1,4.6-8.4,4.6-8.4s1.5-5.6,1.5-7.7c0-2.1,0-7.4,0-10s-2.4-6.7-2.4-6.7l-6.8-7.2l-5.4-4.9l-3.9-5.1v-6.3v-8.8v-8.1l1.5-6.5
                                                    l4.3-6.5V525V516v-22L570.5,494.3z"/>

                                                <path data-val="Birkelane"  id="dataBirkelane" class="btn-load" data-chart="dataBirkelane" title="<strong><u> Birkelane </u> </strong> <br> Total: <?= $nDeclarationsBirkelane ?>  <br> Validées: <?= $nDeclarationsTBirkelane ?> <br> Encours: <?= $nDeclarationsECBirkelane ?> " d="M437,676.1c-0.9,1.1-1.7,2.5-2.5,4.3c-0.6,1.4-1.8,2.6-3.4,4.8c-3.7-4-7.3-7.6-10.6-11.5c-3.6-4.3-7-8.3-1.6-14.2
                                                    c4.5-4.9,0.2-8.6-3-12.4c-6-7.2-11.5-14.7-17.6-21.9c-2-2.4-4.7-4.4-7.5-5.8c-7.6-4.1-10.5-10.1-8-18.3c2.8-8.9,5.8-17.7,8.7-26.5
                                                    l0.8-3.5l1.5-1.5h5.8v7.5l11,5.8l4.7,2.8l2.6,8.1h7.9l3.1,6.3l3.7,5l4,4.8l5.5,3.5l2.4,4.5l5.5,3.5l2.1,6.8l5.5,5.8l6.2,9.8v7.5
                                                    l-3.7,4.5l-7,3.5l-3.4,2.3l-4.6,5.8l-4,3.9l-2,4.8L437,676.1z"/>

                                                <path data-val="Kaffrine"  id="dataKaffrine" class="btn-load" data-chart="dataKaffrine" title="<strong><u> Kaffrine </u> </strong> <br> Total: <?= $nDeclarationsKaffrine ?>  <br> Validées: <?= $nDeclarationsTKaffrine ?> <br> Encours: <?= $nDeclarationsECKaffrine ?> " d="M396,563.9c-0.6-1.7,0-3.7-0.4-5.4c-3.1-12.1,1.1-19.6,9.3-29.2c7.4-8.6,18.3-11.6,25.3-18.8c8.9-9.1,19.5-11.1,30.1-12.3
                                                    c4.7-2.4,3.8,1.2,3.8,1.2l-0.9,7.8c-0.1,3.4,1.4,5.4,1.4,5.4l10.5,8.3l-0.2,6.7l4.5,5.8l-0.4,12.9l8.9-0.3V560l-13.1,0.2l-0.8,33h24
                                                    l-0.9,34.6l4.5-0.1l2.2-0.8l2,0.3l4.1,2.1l3.5,1.2h3.4l3.9-0.2h5.3l4.8-0.3l3.9-0.3l5.4-0.4l3.1,0.1l8.3,5.5l-0.9,2l-0.7,4.8v4.6
                                                    l-3.9,8.7l-2.8,4.1l1.1,15.3c-1.9,1.1-3.9,2.2-5.7,3.1c-5.1,2.5-9.9,5.8-15.1,8.1c-2.3,1-5.6,1.4-8,0.6c-18.4-5.9-18.4-6.1-33.6,2
                                                    c-2.6-11.4-4.8-12.3-16.2-10.7c-4.6,0.6-10.2-0.7-14.1-3.1c-1.8-1.1-3.4-1.9-4.9-2.3l-2.6-1.2l2.1-2.9l1.9-2.5l1.4-1.7l2.2-1.7
                                                    l1.6-2.2l7.8-3.5l2.5-1.5l1.2-2.5l1-2.7l-0.4-3.2v-3.9l-0.6-1.7l-3-4l-1.6-2.7l-3-4.4l-3.1-3.1l-2.1-6.4l-5.5-4.1l-2.1-4.6l-4.2-2.4
                                                    l-5.4-4.4l-3.4-5.2l-2.4-3.9l-2.4-4.5l-6.2,0.5l-2.2-7.1l-15.7-8.3l-0.4-8.2l-5.8-0.2l-1.7,0.7C394.6,568.5,397.2,566.9,396,563.9z"
                                                    />

                                                <path data-val="MalemHodar"  id="dataMalemHodar" class="btn-load" data-chart="dataMalemHodar" title="<strong><u> Malem-Hodar </u> </strong> <br> Total: <?= $nDeclarationsMalemHodar ?>  <br> Validées: <?= $nDeclarationsTMalemHodar ?> <br> Encours: <?= $nDeclarationsECMalemHodar ?> " d="M554.1,630.4c2.9-3.3,8.8-9.1,8.8-9.1l6.4-6.2c10.8-4.6,6.2-22.4,6.2-22.4v-5.2l-5.2-5l-8.5-9.2l-3.4-3l-0.2-7.1v-12.8v-5.7
                                                    l1.8-5.5l3.2-8v-36.8c-2.6-0.2-5.3-0.7-7.9-1.3c-21.7-5-46-2.9-64-20.3c-1.2,3.2-2.5,5.9-3,8.6c-1.9,10.8-8,14.5-18.9,15.8
                                                    c-3.3,0.5-4.8-0.3-4.8-0.3l0.5,2.4v12.4l11.2,8.9v7.1l4.6,5.3v5.9v5.9h7.9v8.3v8.3H477v6.5v8.9v5.8v9h5.9h5.9h11.9v8.9v14.8v10.6
                                                    h5.9l7.9,2.4h19.2H544l8.4,4.3L554.1,630.4z"/>

                                                <path fill="white"  id="" class="st1" d="M399.9,531"/>

                                                <path data-val="Gossas"  id="dataGossas" class="btn-load" data-chart="dataGossas" title="<strong><u> Gossas </u> </strong> <br> Total: <?= $nDeclarationsGossas ?>  <br> Validées: <?= $nDeclarationsTGossas ?> <br> Encours: <?= $nDeclarationsECGossas ?> " d="M408.6,522.5c5.6-5.2,11.7-10,19.3-13.5c11.5-5.3,21.3-13.8,35.1-13.1c3.4,0.2,7.3,0.1,10.3-1.3c3.8-1.8,7.5-4.6,10-8
                                                    c2.4-3.2,3.2-7.5,4.5-11.4c2.7-8-18.2-34.6-26-32.2c-8.6,2.7-15.2-0.7-22.6-3.2c-5.7-1.9-12-2.4-16.2-3.2
                                                    c-8.6,10.8-14.8,20.6-23.1,28.1c-5.8,5.2-5.2,10.1-4.5,16c2,16.5-5.1,23.2-14.9,27.9c-8.3,4-18.1,5.7-27.5,6.7
                                                    c-6.2,0.7-12.8-2.3-19.2-2.3c-7.8,0-17.1-1-23,2.7c-4.8,3-5.5,12.4-8.5,20.2c8.6,1.3,15.5,2,22.2,3.6c2.4,0.6,4.2,1.4,5.4,2.6l1,2.9
                                                    l-1.3,3c15.3,1.9,13-6.5,13-6.5v-7.9l4.9-1.9h4.3l2.3,4.9v7.2h6l5.6,4.2h5.6l2.8-4.6V535l7.4-5.1l9.3-4.6h6.5h6l2.4,0.1L408.6,522.5
                                                    z"/>

                                                <path data-val="Koumpentoum"  id="dataKoumpentoum" class="btn-load" data-chart="dataKoumpentoum" title="<strong><u> Koumpentoum </u> </strong> <br> Total: <?= $nDeclarationsKoumpentoum ?>  <br> Validées: <?= $nDeclarationsTKoumpentoum ?> <br> Encours: <?= $nDeclarationsECKoumpentoum ?> " d="M669.7,712.2c-8.2-2.7-17.8-2.5-25.8,1.9c-6.6,3.6-12.5,6.8-18.6,0.6c-2.9-2.9-5.1-6.7-6.8-10.4c-1.9-3.9-2.5-8.5-4.5-12.4
                                                    c-4.3-8.4-2.5-11.3,6.9-11.8c7.7-0.4,14.8-1,17.7-11.6c1.9-6.8,8.9-13.2,15.2-17.4c7.9-5.2,10.7-12.9,12-20.6
                                                    c1.4-7.9-0.8-16.4-1.1-24.7c-0.2-5.4,0.2-10.7,0.7-16.1c0.8-9.6-0.2-18.3-8.7-24.8c-2-1.6-3.5-5.3-3.3-8c0.6-12.6,2.1-25.2,2.6-37.8
                                                    c0.2-5.1,1.9-7.4,6.9-8.3c13.6-2.2,26.1-5.6,35.4-17.7c3.6-4.7,12-5.9,19.9-9.4c2.7,4.8,5.1,9.9,8.3,14.5c1.3,1.9,4,3,6.2,3.8
                                                    c25.8,9.9,51.4,20.6,77.5,29.6l10.5,3.6l-8.7,2.9h-6.2l-3.7,2.9v6.2l-5.8,3.9H793l-2.3,3.8v4.5v6l-4.3,4.8l-2.6,2.4l-5.1,2.7
                                                    l-5.4,7.2l-2.3,6.9c0,0,0,7.5,0,8.4c0,0.9,0,3.9,0,3.9v5.7l3.3,3.6h2.3l1.8,5.4l-2.6,3.6c0,0-2,0-4.9,0s0.3-3-2.8,0
                                                    c-3.1,3-3.3,1.5-3.3,1.5l-2.8,1.8l-4.3,1.5l-4.3,6.6l-1,5.1l-3.6,8.1l-1,12.9l2.8,2.1h2.3v6.3l-0.3,8.7l-3.3,4.2l-8.7,12.9
                                                    c0,0-10,10.8-18.7-7.2l-3.6-6.3l-4.3-2.1l-4.6-3l-6.9-0.3l-4.6,1.8l-9.7,2.4v5.7v12.2v6.1l-3.1,2.4l-4.3,3.3l-3.8,3.6L674,714
                                                    L669.7,712.2z"/>

                                                <path data-val="Goudiry"  id="dataGoudiry" class="btn-load" data-chart="dataGoudiry" title="<strong><u> Goudiry </u> </strong> <br> Total: <?= $nDeclarationsGoudiry ?>  <br> Validées: <?= $nDeclarationsTGoudiry ?> <br> Encours: <?= $nDeclarationsECGoudiry ?> " d="M1068.9,500.9c-13,5.1-27.8,7.7-41.5,7.4c-19.3-0.4-38.6-6.8-57.9-10.2c-2.7-0.5-6.4,0.2-8.6,1.8
                                                    c-17.8,13.3-35.4,26.9-52.8,40.8c-2,1.6-2.8,5.6-2.8,8.4c0.2,9.8,0.4,19.2-6.7,27.4c-8.7,10-16.7,20.5-24.8,31
                                                    c-1.1,1.4-1.7,3.3-2.2,5.1c-2.1,8.6-4.9,17.2-5.9,26c-0.6,5.2,0,12.2,3.1,15.6c2.7,2.9,9.7,1.8,14,2.4c4.1,3,8.4,8.6,12.3,8.3
                                                    c5.1-0.4,9.8-5.5,13-7.5c7.4,5.7,15,10.2,20.9,16.4c13.2,13.8,17.4,25.5-5.9,34.7c-3.8,1.5-7.1,5-9.6,8.4c-1,1.3,0.1,5.9,1.6,6.9
                                                    c8.9,5.6,18.3,10.5,26.2,14.9c1,7.6,1.9,15,2.6,20.6c4.7,0,8.1-1,10.4,0.2c5,2.5,9.3,2.3,12.3-2c6.6-9.4,15.3-5,23.3-3.8
                                                    c3.8,0.6,7.3,2.7,8.4,3.2c2.5,8.8,4.5,16,6.6,23.1c9.2-7,17.7-13.2,25.8-19.7c5.2-4.2,9.8-9.3,15.3-13c7.6-5.1,15.7-9.5,23.8-13.7
                                                    c1.4-0.7,4.6,0.4,6,1.8c12.2,11.9,28.4,19.1,38.7,33.1c2.7,3.7,4.3,8.7,7.7,11.5c7.8,6.5,12.4,13.9,11,25.8
                                                    c8.5-12.6,18.1-10,27.1-3.2c3.7-4.8,5.9-10.3,9.8-12.1c4.2-1.9,9.9-0.4,14.9-0.4c0.5-0.8,1.1-1.5,1.6-2.3
                                                    c-10.4-8.4-11.5-18.2-5.4-29.8l-3.2-4.9l-1.4-4.5l-1.2-3.3l-7.1-0.7l-9.5-0.7l-6.2-0.7l-7.1-0.9l-9.5-5l-5.7-6.2l1.4-7.3l0.9-6.4
                                                    l0.7-76.5l-1.2-5.2l-3-5.4l-4.8-2.5l-1.2-10.8l1.2-5.9l-4.3-3.3l-8,3.1l-9.2-1.9c0,0-7.6-3.8-7.3-8l2.4-67.5V525l-0.2-7.3l-0.2-7.1
                                                    l-3.1-5.7l-1.9-3.3h-4l-3.8-1.7l-2.6-3.6h-5h-3.1l-3.1,2.1L1068.9,500.9z"/>


                                                <path data-val="Bakel"   id="dataBakel" class="btn-load" data-chart="dataBakel" title="<strong><u> Bakel </u> </strong> <br> Total: <?= $nDeclarationsBakel ?>  <br> Validées: <?= $nDeclarationsTBakel ?> <br> Encours: <?= $nDeclarationsECBakel ?> " d="M1190.7,758.4c1.5,0.2,2.9,0.6,4.1,1.3c6.9,4.3,13.3,9.4,20.1,13.8c4.4,2.9,8.2,8,14.9,4.7c0.8-0.4,3.9,2,4.5,3.6
                                                    c0.9,2.3,0.5,5.1,0.7,7.7c8.9-4.7,17.2-8.7,25.1-13.4c6.3-3.7,7.4-17.9,2-23.2c-7.4-7.3-16.2-13.5-22.1-21.8
                                                    c-7.3-10.2-12.3-22.1-18.7-33.8c6.5-4.3,12.2-8.5,18.3-12.1c11.9-6.8,10.7-19.1,11.9-29.7c0.4-3.7-4.9-8.4-8.2-12.1
                                                    c-6.1-6.7-10.1-14.2-5.8-22.8c8.3-16.6,3.5-32.1-3.1-47.4c-0.9-2-4.2-3.3-6.6-4.2c-12.3-4.2-13.2-5.6-10.8-18.3
                                                    c-2.1-0.6-4.1-1.3-6.2-1.7c-9.4-1.9-17.7-4.5-16.5-17c0.2-2.2-2-4.5-2.1-6.8c-0.2-5.2-1.4-11.3,0.7-15.6c3.7-7.3,9.5-13.6,14.5-20.3
                                                    c-5.4-5.8-11.7-10.3-14.9-16.5c-6.8-12.8-18.3-20.1-30.1-25.8c-13.1-6.3-22.9-12.9-23.7-28.8c-0.2-4.4-3.5-11.7-6.5-12.3
                                                    c-10.9-2.3-17.4-10.7-25.9-16.1c-3-1.9-6.5-3.1-9.8-4.6c-0.6,0.7-1.3,1.3-1.9,2c4.8,8.6,10.3,16.9,14,25.9c2.5,6.1,4.5,13.7,3.2,20
                                                    c-2.4,12.2-6,24.4-11.2,35.6c-2.5,5.3-8.1,10.3-11.2,15.6l-13,9.1h10.4l4.3,3.7h5.9l4.1,7.7v5.4v3.7v12.3v10.4v8.6v9.1v12.7v7.7
                                                    v10.9v6.8v4.1c0,0-0.5,13.2,15.4,8.6l11.3-2.3l3.6,9.1v5.5v5.9l4.1,5l2.3,12.7v14.5v12.3v15.4v15v18.2v10.4c0,0,0.9,8.2,2.3,8.2
                                                    c1.4,0,13.1,4.1,13.1,4.1h5h10l9.1,2.7l3.6,6.8l3.2,4.5L1190.7,758.4z"/>

                                                <path data-val="Guediawaye"  id="dataGuediawaye" class="btn-load" data-chart="dataGuediawaye" title="<strong><u> Guédiawaye </u> </strong> <br> Total: <?= $nDeclarationsGuediawaye ?>  <br> Validées: <?= $nDeclarationsTGuediawaye ?> <br> Encours: <?= $nDeclarationsECGuediawaye  ?> " d="M34.4,453.5l1,0.8l0.6,0.3l0.5,0.3h1.1h0.9l1.7-0.4l1-0.5l0.6-0.6l0.2-0.4l0.7-0.3h1l0.5-0.2l0.5-0.7l0.2-0.4l0.3-0.7
                                                    l0.2-0.6v-0.3l-0.5-0.7C44.9,449.1,36.6,451.3,34.4,453.5z"/>

                                            </svg>

                                        

                                            

                                            
                                            
                                        </div>

                                    
                                        <!-- <div id="chartContainer" style="height: 370px; width: 100%;" ></div>
                                        <div id="diourbelChart" style="height: 370px; width: 100%;display: none;" ></div> -->

                                           
                                        <div class="col-lg-6" id="graphique">

                                            <canvas  id="myChart" width="400" height="400"></canvas> 
                                            
                                        </div>

                                    </div>

                                           <div   class="Général box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;"> Général </div>
                                           <div   class="Dakar box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;"> DAKAR </div>
                                           <div   class="Pikine  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;"> PIKINE</div>
                                           <div   class="Guediawaye  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">GUEDIAWAYE</div>
                                           <div   class="Mbacke  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">MBACKE</div>
                                           <div   class="Diourbel  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">DIOURBEL</div>
                                           <div   class="Bambey  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">BAMBEY</div>
                                           <div   class="Fatick  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">FATICK</div>
                                           <div   class="Gossas  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">GOSSAS</div>
                                           <div   class="Foundiougne  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">FOUNDIOUGNE</div>
                                           <div   class="Birkelane  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">BIRKELANE</div>
                                           <div   class="Kaffrine  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">KAFFRINE</div>
                                           <div   class="MalemHodar  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">MALEM HODDAR</div>
                                           <div   class="Koungheul  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">KOUNGHEUL</div>
                                           <div   class="Guinguineo  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">GUINGUINEO</div>
                                           <div   class="Kaolack  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">KAOLACK</div>
                                           <div   class="NioroduRip  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">NIORO DU RIP</div>
                                           <div   class="Kedougou  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">KEDOUGOU</div>
                                           <div   class="Saraya  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">SARAYA</div>
                                           <div   class="Salemata  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">SALEMATA</div>
                                           <div   class="Velingara  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">VELINGARA</div>
                                           <div   class="MedinaYoroFoulah  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">MEDINA YORO FOULAH</div>
                                           <div   class="Kolda  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">KOLDA</div>
                                           <div   class="Louga  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">LOUGA</div>
                                           <div   class="Linguere  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">LINGUERE</div>
                                           <div   class="Kebemer  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">KEBEMER</div>
                                           <div   class="Kanel  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">KANEL</div>
                                           <div   class="Ranerou  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">RANEROU</div>
                                           <div   class="Matam  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">MATAM</div>
                                           <div   class="SaintLouis  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">SAINT-LOUIS</div>
                                           <div   class="Dagana  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">DAGANA</div>
                                           <div   class="Podor  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">PODOR</div>
                                           <div   class="Goudomp  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">GOUDOMP</div>
                                           <div   class="Sedhiou  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">SEDHIOU</div>
                                           <div   class="Bounkiling  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">BOUNKILING</div>
                                           <div   class="Bakel  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">BAKEL</div>
                                           <div   class="Goudiry  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">GOUDIRY</div>
                                           <div   class="Koumpentoum  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">KOUMPENTOUM</div>
                                           <div   class="Tambacounda  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">TAMBACOUNDA</div>
                                           <div   class="Mbour  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">MBOUR</div>
                                           <div   class="Thies  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">THIES</div>
                                           <div   class="Tivaouane  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">TIVAOUANE</div>
                                           <div   class="Oussouye  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">OUSSOUYE</div>
                                           <div   class="Bignona  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">BIGNONA</div>
                                           <div   class="Ziguinchor  box" style="font-size: 25px;font-weight: bold;color: #a8a8a8;">ZIGUINCHOR</div>


                                <select  class="form-control col-lg-12  monselect select3" id="#hider">


                                      <option value="multiple">Sélectionner un département</option> 

                                      <option >  </option> 
                                      <option  data-chart="dataGeneral" value="Général" selected> Général </option>
                                      <option data-chart="dataPikine" value="Pikine"> PIKINE</button></option>
                                      <option data-chart="dataGuediawaye" value="Guediawaye" >GUEDIAWAYE</option>
                                      <option data-chart="dataMbacke" value="Mbacke">MBACKE</option>
                                      <option data-chart="dataDiourbel" value="Diourbel">DIOURBEL</option>
                                      <option data-chart="dataBambey" value="Bambey">BAMBEY</option>
                                      <option data-chart="dataFatick" value="Fatick">FATICK</option>
                                      <option data-chart="dataGossas" value="Gossas">GOSSAS</option>
                                      <option data-chart="dataFoundiougne" value="Foundiougne">FOUNDIOUGNE</option>
                                      <option data-chart="dataBirkelane" value="Birkelane">BIRKELANE</option>
                                      <option data-chart="dataKaffrine" value="Kaffrine">KAFFRINE</option>
                                      <option data-chart="dataMalemHodar" value="MalemHodar">MALEM HODDAR</option>
                                      <option data-chart="dataKoungheul" value="Koungheul">KOUNGHEUL</option>
                                      <option data-chart="dataGuinguineo" value="Guinguineo">GUINGUINEO</option>
                                      <option data-chart="dataKaolack" value="Kaolack">KAOLACK</option>
                                      <option data-chart="dataNioroduRip" value="NioroduRip">NIORO DU RIP</option>
                                      <option data-chart="dataKedougou" value="Kedougou">KEDOUGOU</option>
                                      <option data-chart="dataSaraya" value="Saraya">SARAYA</option>
                                      <option data-chart="dataSalemata" value="Salemata">SALEMATA</option>
                                      <option data-chart="dataVelingara" value="Velingara">VELINGARA</option>
                                      <option data-chart="dataMedinaYoroFoulah" value="MedinaYoroFoulah">MEDINA YORO FOULAH</option>
                                      <option data-chart="dataKolda" value="Kolda">KOLDA</option>
                                      <option data-chart="dataLouga" value="Louga">LOUGA</option>
                                      <option data-chart="dataLinguere" value="Linguere">LINGUERE</option>
                                      <option data-chart="dataKebemer" value="Kebemer">KEBEMER</option>
                                      <option data-chart="dataKanel" value="Kanel">KANEL</option>
                                      <option data-chart="dataRanerou" value="Ranerou">RANEROU</option>
                                      <option data-chart="dataMatam" value="Matam">MATAM</option>
                                      <option data-chart="dataSaintLouis" value="SaintLouis">SAINT-LOUIS</option>
                                      <option data-chart="dataDagana" value="Dagana">DAGANA</option>
                                      <option data-chart="dataPodor" value="Podor">PODOR</option>
                                      <option data-chart="dataGoudomp" value="Goudomp">GOUDOMP</option>
                                      <option data-chart="dataSedhiou" value="Sedhiou">SEDHIOU</option>
                                      <option data-chart="dataBounkiling" value="Bounkiling">BOUNKILING</option>
                                      <option data-chart="dataBakel" value="Bakel">BAKEL</option>
                                      <option data-chart="dataGoudiry" value="Goudiry">GOUDIRY</option>
                                      <option data-chart="dataKoumpentoum" value="Koumpentoum">KOUMPENTOUM</option>
                                      <option data-chart="dataTambacounda" value="Tambacounda" >TAMBACOUNDA</option>
                                      <option data-chart="dataMbour" value="Mbour">MBOUR</option>
                                      <option data-chart="dataThies" value="Thies">THIES</option>
                                      <option data-chart="dataTivaouane" value="Tivaouane">TIVAOUANE</option>
                                      <option data-chart="dataOussouye" value="Oussouye">OUSSOUYE</option>
                                      <option data-chart="dataBignona" value="Bignona">BIGNONA</option>
                                      <option data-chart="dataZiguinchor" value="Ziguinchor">ZIGUINCHOR</option>

                                </select>

                                 
                                
                            </section>

                            <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js'></script>
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

                                <script >
                                     function svgTip(opts) {
                                opts = opts || {};
                                opts.fontsize = opts.fontsize || '12px';
                                opts.padding = opts.padding || '.5rem';
                                opts.bgcolor = opts.bgcolor || '#000';
                                opts.color = opts.color || '#fff';
                                opts.opacity = opts.opacity || 0.6;
                                $.each($('[title]', 'svg'), function (id, obj) {
                                    var tt = $(this).attr('title'), that = $('<div class=\'svgtip\'>' + tt + '</div>');
                                    that.css({
                                        'position': 'absolute',
                                        'top': 0,
                                        'left': 0,
                                        'display': 'none',
                                        'background-color': opts.bgcolor,
                                        'color': opts.color,
                                        'padding': opts.padding,
                                        'font-size': opts.fontsize,
                                        'opacity': opts.opacity,
                                        'pointer-events': 'none'
                                    });
                                    $(this).hover(function (event) {
                                        that.css({
                                            'left': event.clientX,
                                            'top': event.clientY + $(window).scrollTop()
                                        });
                                        that.addClass('active');
                                        that.show();
                                    }, function () {
                                        that.hide();
                                        that.removeClass('active');
                                    });
                                    $(this).on('mousemove', function (event) {
                                        if (that.hasClass('active')) {
                                            that.css({
                                                'left': event.clientX,
                                                'top': event.clientY + $(window).scrollTop() - that.height()
                                            });
                                        }
                                    });
                                    $('body').append(that);
                                });
                            }
                            new svgTip({
                                fontsize: '11px',
                                padding: '10px'
                            });
                            function lll(msg) {
                                $('#debug').append(msg + '  ');
                            }


                            //chart général

                            Chart.defaults.global.legend.display = false;
                            Chart.defaults.global.tooltips.enabled = false;



                            var dataGeneral = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "Dataset #1",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [<?= $nDeclarations ?>, <?= $nDeclarationsT ?>, <?= $nDeclarationsEC ?>],
                              }]
                            };

                            var optionsGeneral = {

                                title: {
                                    display: true,
                                    text: 'Diagramme des déclarations',
                                    fontSize:25
                                },

                                legend: {
                                    display: false
                                 },
                              maintainAspectRatio: false,
                              scales: {
                                yAxes: [{
                                  stacked: true,
                                  gridLines: {
                                    display: true,
                                    color: "rgba(50, 83, 168)"
                                  }
                                }],
                                xAxes: [{
                                  gridLines: {
                                    display: true
                                  }
                                }]
                              }
                            };




                            //chart diourbel

                            var dataDiourbel = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "Dataset #1",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTDiourbel ?>, <?= $nDeclarationsECDiourbel ?> ,<?= $nDeclarationsDiourbel ?>],
                              }]
                            };

                            var optionsDiourbel = {

                                title: {
                                    display: true,
                                    text: 'Diourbel',
                                    fontSize:25
                                },

                                legend: {
                                    display: false
                                 },
                              maintainAspectRatio: false,
                              scales: {
                                yAxes: [{
                                  stacked: true,
                                  gridLines: {
                                    display: true,
                                    color: "rgba(50, 83, 168)"
                                  }
                                }],
                                xAxes: [{
                                  gridLines: {
                                    display: true
                                  }
                                }]
                              }
                            };

                            //chart kanel 

                            var dataKanel = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "Dataset #1",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTKanel ?>, <?= $nDeclarationsECKanel ?>,<?= $nDeclarationsKanel ?>],
                              }]
                            };



                            //chart Ranerou

                            var dataRanerou = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "Dataset #1",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTRanerou ?>, <?= $nDeclarationsECRanerou ?>,<?= $nDeclarationsRanerou ?>],
                              }]
                            };



                            //chart Matam

                            var dataMatam = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "Dataset #1",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTMatam ?>, <?= $nDeclarationsECMatam ?>,<?= $nDeclarationsMatam ?>],
                              }]
                            };


                            //chart Podor

                            var dataPodor = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "Dataset ",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTPodor ?>, <?= $nDeclarationsECPodor ?>,<?= $nDeclarationsPodor ?>],
                              }]
                            };



                            //Dagana

                            var dataDagana = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTDagana ?>, <?= $nDeclarationsECDagana ?>,<?= $nDeclarationsDagana ?>],
                              }]
                            };

                            //SaintLouis

                            var dataSaintLouis = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTSaintLouis ?>, <?= $nDeclarationsECSaintLouis ?>,<?= $nDeclarationsSaintLouis ?>],
                              }]
                            };



                            //Louga

                            var dataLouga = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTLouga ?>, <?= $nDeclarationsECLouga ?>,<?= $nDeclarationsLouga ?>],
                              }]
                            };


                            //Kebemer

                            var dataKebemer = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTKebemer ?>, <?= $nDeclarationsECKebemer ?>,<?= $nDeclarationsKebemer ?>],
                              }]
                            };



                            //Tivaouane

                            var dataTivaouane = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTTivaouane ?>, <?= $nDeclarationsECTivaouane ?>,<?= $nDeclarationsTivaouane ?>],
                              }]
                            };


                            //Thies 

                            var dataThies = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTThies ?>, <?= $nDeclarationsECThies ?>,<?= $nDeclarationsThies ?>],
                              }]
                            };


                            //Rufisque

                            var dataRufisque = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTRufisque ?>, <?= $nDeclarationsECRufisque ?>,<?= $nDeclarationsRufisque ?>],
                              }]
                            };


                            //Pikine

                            var dataPikine = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTPikine ?>, <?= $nDeclarationsECPikine ?>,<?= $nDeclarationsPikine ?>],
                              }]
                            };


                            //Dakar

                            var dataDakar = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTDakar ?>, <?= $nDeclarationsECDakar ?>,<?= $nDeclarationsDakar ?>],
                              }]
                            };


                            //Tambacounda

                            var dataTambacounda = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTTambacounda ?>, <?= $nDeclarationsECTambacounda ?>,<?= $nDeclarationsTambacounda ?>],
                              }]
                            };


                            //Bounkiling

                            var dataBounkiling = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTBounkiling ?>, <?= $nDeclarationsECBounkiling ?>,<?= $nDeclarationsBounkiling ?>],
                              }]
                            };



                            //Bignona

                            var dataBignona = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTBignona ?>, <?= $nDeclarationsECBignona ?>,<?= $nDeclarationsBignona ?>],
                              }]
                            };


                            //Velingara

                            var dataVelingara = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTVelingara ?>, <?= $nDeclarationsECVelingara ?>,<?= $nDeclarationsVelingara ?>],
                              }]
                            };


                            //Foundiougne

                            var dataFoundiougne = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTFoundiougne ?>, <?= $nDeclarationsECFoundiougne ?>,<?= $nDeclarationsFoundiougne ?>],
                              }]
                            };

                            //Fatick

                            var dataFatick = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTFatick ?>, <?= $nDeclarationsECFatick ?>,<?= $nDeclarationsFatick ?>],
                              }]
                            };


                            //NioroduRip

                            var dataNioroduRip = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTNioroduRip ?>, <?= $nDeclarationsECNioroduRip ?>,<?= $nDeclarationsNioroduRip ?>],
                              }]
                            };


                            //Mbour

                            var dataMbour = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTMbour ?>, <?= $nDeclarationsECMbour ?>,<?= $nDeclarationsMbour ?>],
                              }]
                            };

                            //Kaolack

                            var dataKaolack = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTKaolack ?>, <?= $nDeclarationsECKaolack
                                 ?>,<?= $nDeclarationsKaolack ?>],
                              }]
                            };


                            //Mbacke

                            var dataMbacke = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTMbacke ?>, <?= $nDeclarationsECMbacke
                                 ?>,<?= $nDeclarationsMbacke?>],
                              }]
                            };


                            //Bambey

                            var dataBambey = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTBambey ?>, <?= $nDeclarationsECBambey
                                 ?>,<?= $nDeclarationsBambey ?>],
                              }]
                            };


                            //Ziguinchor

                            var dataZiguinchor = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTZiguinchor ?>, <?= $nDeclarationsECZiguinchor
                                 ?>,<?= $nDeclarationsZiguinchor ?>],
                              }]
                            };

                            //Oussouye

                            var dataOussouye = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTOussouye ?>, <?= $nDeclarationsECOussouye
                                 ?>,<?= $nDeclarationsOussouye ?>],
                              }]
                            };


                            //Guinguineo

                            var dataGuinguineo = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTGuinguineo ?>, <?= $nDeclarationsECGuinguineo
                                 ?>,<?= $nDeclarationsGuinguineo ?>],
                              }]
                            };

                            //Linguere

                            var dataLinguere = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTLinguere ?>, <?= $nDeclarationsECLinguere
                                 ?>,<?= $nDeclarationsLinguere ?>],
                              }]
                            };


                            //Salemata

                            var dataSalemata = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTSalemata ?>, <?= $nDeclarationsECSalemata
                                 ?>,<?= $nDeclarationsSalemata ?>],
                              }]
                            };

                            //Saraya

                            var dataSaraya = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTSaraya ?>, <?= $nDeclarationsECSaraya
                                 ?>,<?= $nDeclarationsSaraya ?>],
                              }]
                            };


                            //Kedougou

                            var dataKedougou = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTKedougou ?>, <?= $nDeclarationsECKedougou
                                 ?>,<?= $nDeclarationsKedougou ?>],
                              }]
                            };


                            //Goudomp

                            var dataGoudomp = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTGoudomp ?>, <?= $nDeclarationsECGoudomp
                                 ?>,<?= $nDeclarationsGoudomp ?>],
                              }]
                            };

                            //Sedhiou

                            var dataSedhiou = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTSedhiou ?>, <?= $nDeclarationsECSedhiou
                                 ?>,<?= $nDeclarationsSedhiou ?>],
                              }]
                            };

                            //Kolda

                            var dataKolda = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTKolda ?>, <?= $nDeclarationsECKolda
                                 ?>,<?= $nDeclarationsKolda ?>],
                              }]
                            };

                            //MedinaYoroFoulah

                            var dataMedinaYoroFoulah = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTMedinaYoroFoulah ?>, <?= $nDeclarationsECMedinaYoroFoulah
                                 ?>,<?= $nDeclarationsMedinaYoroFoulah ?>],
                              }]
                            };

                            //Koungheul

                            var dataKoungheul = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTKoungheul ?>, <?= $nDeclarationsECKoungheul
                                 ?>,<?= $nDeclarationsKoungheul ?>],
                              }]
                            };

                            //Birkelane

                            var dataBirkelane = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTBirkelane ?>, <?= $nDeclarationsECBirkelane
                                 ?>,<?= $nDeclarationsBirkelane ?>],
                              }]
                            };

                            //Kaffrine

                            var dataKaffrine = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTKaffrine ?>, <?= $nDeclarationsECKaffrine
                                 ?>,<?= $nDeclarationsKaffrine ?>],
                              }]
                            };

                            //MalemHodar

                            var dataMalemHodar = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTMalemHodar ?>, <?= $nDeclarationsECMalemHodar
                                 ?>,<?= $nDeclarationsMalemHodar ?>],
                              }]
                            };

                            //Gossas

                            var dataGossas = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTGossas ?>, <?= $nDeclarationsECGossas
                                 ?>,<?= $nDeclarationsGossas ?>],
                              }]
                            };


                            //Koumpentoum

                            var dataKoumpentoum = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTKoumpentoum ?>, <?= $nDeclarationsECKoumpentoum
                                 ?>,<?= $nDeclarationsKoumpentoum ?>],
                              }]
                            };

                            //Goudiry

                            var dataGoudiry = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTGoudiry ?>, <?= $nDeclarationsECGoudiry
                                 ?>,<?= $nDeclarationsGoudiry ?>],
                              }]
                            };

                            //Bakel

                            var dataBakel = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTBakel ?>, <?= $nDeclarationsECBakel
                                 ?>,<?= $nDeclarationsBakel ?>],
                              }]
                            };

                            //Guediawaye

                            var dataGuediawaye = {
                              labels: [ "Total", "Validées" ,"En cours"],
                              datasets: [{
                                label: "",

                                backgroundColor: ["rgba(3, 165, 252,0.2)", "rgba(252, 3, 45,0.2)", "rgba(26, 107, 10,0.2)"],
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 2,
                                hoverBackgroundColor: "rgba(50, 83, 168,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [ <?= $nDeclarationsTGuediawaye ?>, <?= $nDeclarationsECGuediawaye
                                 ?>,<?= $nDeclarationsGuediawaye ?>],
                              }]
                            };



                            // Chart.Bar('chartDiourbel', {
                            //   options: optionsDiourbel,
                            //   data: dataDiourbel
                            // });

                            // Chart.Bar('chartGeneral', {
                            //   options: optionsGeneral,
                            //   data: dataGeneral
                            // });

                            var ctx = $("#myChart");
                            var myChart;
                            function createChart(teste){
                              if(myChart)
                              { 
                                myChart.destroy();
                              }
                                myChart = new Chart(ctx, {
                                    type: 'bar',
                                    data: teste,
                                    options: optionsGeneral,
                                }); 
                            }

                            createChart(dataGeneral);

                            $('.btn-load').on('click', function(){
                              var foo = eval($(this).data('chart'));
                                console.log(foo);

                                createChart(foo);
                            });



                            $(function () {
                              $('[data-toggle="tooltip"]').tooltip()
                            })


                            $('#search').keyup(function() {

                                var value = $(this).val();
                                var exp = new RegExp(value);

                                $('button').each(function() {
                                    var isMatch = exp.test($(this).data('title'));
                                    $(this).toggle(isMatch);
                                });
                            });




                            $(document).ready(function() {
                              
                              $(function () {
                              $(".select3").select2();
                            });

                            $('.monselect').on('change', function(){
                              var foo = eval($('option:selected').data('chart'));
                              console.log(foo);

                              createChart(foo);
                            });


                            });

                            $(document).ready(function(){
                                $(".monselect").change(function(){
                                    $(this).find("option:selected").each(function(){
                                        var optionValue = $(this).attr("value");
                                        if(optionValue){
                                            $(".box").not("." + optionValue).hide();
                                            $("." + optionValue).show();
                                        } else{
                                            $(".box").hide();
                                        }
                                    });
                                }).change();
                            });

                            $(document).ready(function(){
                                $('.btn-load').click(function(){ 
                                    $('.monselect').val($(this).data('val')).trigger('change');
                                })
                            });

                            $(document).ready(function(){


                                $(".btn-load").click(function() {
                                    $(".btn-load").css("fill","");
                                    $(this).css("fill","#4fade8");

                                });
                            });


                            $(document).ready(function(){
                                $('.monselect').on('change', function(){
                              
                                  if ($('option:selected').data('chart')) {
                                    //alert($('option:selected').data('chart'));

                                    var testeur=$('option:selected').data('chart');

                                    //$('path').data('chart').css("fill", "red");
                                    
                                    $('path').css("fill", "#ccc");
                                    $('#'+testeur).css("fill", "#4fade8");

                                    // $("path").on({
                                //         mouseenter: function(){
                                //            $(path).css("fill", "#4fade8");
                                //         },
                               
                                //         mouseleave: function(){
                                //            $(path).not('#'+testeur).css("fill", "#ccc");
                                //         },
                               
                                //      });

                                }

                                

                              
                            });
                            });


                            // $(document).ready(function(){

                            //          $("path").on({
                            //             mouseenter: function(){
                            //                $(this).css("background-color", "gray");
                            //             },
                               
                            //             mouseleave: function(){
                            //                $(this).css("background-color", "red");
                            //             },
                               
                            //          });

                            //       });



                                </script>

                            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
                            <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" /> -->


                        
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/acat.js')}}"></script>
    <script src="{{asset('assets/js/choix.js')}}"></script>
@endsection