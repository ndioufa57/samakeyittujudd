@extends('layouts.master')

@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Modification utilisateur'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach
                        @endif
                        <div class="card-title mb-3">Modification  utilisateur</div>

                        <form method="POST" action="/admin/update-utilisateur-saving/{{ $user->id }}" enctype="multipart/form-data" >

                            @csrf
                            @method('put')
                            
                            <div class="form-group row 2 ">
                                <div class="col-md-6 " id="prenom">
                                    <label for="" class="col-form-label">Prénom</label>
                                    <input type="text" name="prenom" class="form-control" value="{{$user->prenom}}" />
                                </div>
                                <div class="col-md-6">
                                    <label for="" class="col-form-label">Nom</label>
                                    <input type="text" name="nom" class="form-control" value="{{$user->name}}" required/>
                                </div>
                            </div>
                            <div class="form-group row">

                                    
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Telephone</label>
                                        <input type="number" name="telephone" class="form-control" value="{{$user->telephone}}" required/>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Email</label>
                                        <input type="text" name="email" class="form-control" value="{{$user->email}}" required/>
                                    </div>

                                   
                            </div>

                            <div class="form-group row">
                                    
                            
                            
                            
                                    
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Adresse</label>
                                        <input type="text" name="adresse" class="form-control" value="{{$user->adresse}}" required/>
                                        
                                    </div>

                                    <div class="col-md-6 edit-type-wrapper" id="test44">
                                        <label for="" class="col-form-label">Profil</label>
                                        <select class="form-control" name="profil" required> 
                                          <!-- <option>--Sélectionner le profil</option> -->

                                          <?php

                                              use App\Models\Region;
                                              
                                              use App\Models\Profil;
                                              
                                              
                                              $listeProfil=Profil::all();

                                              if ($user->profil =="2") {

                                                 $profil = "Admin";
                                               }
                                               elseif ($user->profil =="3") {
                                                 $profil = "Déclarant";
                                               }
                                               elseif ($user->profil =="4") {
                                                $profil = "Agent d'état civil";
                                               }
                                               elseif ($user->profil =="5") {
                                                $profil = "Officier d'état civil";
                                               }
                                              
                                              
                                             
                                           ?>
                                            <option value="{{$user->profil}}" selected> Prodil actuel : {{$profil}}</option>
                                            
                                            <option value="2">Admin </option>
                                            <option value="3">Déclarant </option>
                                            <option value="4">Agent d'état civil </option>
                                            <option value="5">Officier d'état civil </option>
                                        </select>

                                    </div>
                            </div>

                            <div class="form-group row">
                                    
                            
                                    <div class=" col-md-6 ">
                                      <label for="" class="col-form-label">CNI</label>
                                      <input type="text" name="cni" class="form-control" value="{{$user->cni}}" min="0"  />
                                    </div>
                                    

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="photo">Photo</label>
                                        <input name="photo" type="file" class="form-control form-control-rounded" id="photo" placeholder="Web address">
                                    </div>

                              </div>
                              
                              <div class="form-group row">

                                    
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="active">Statut</label>
                                        <select name="active" class="form-control form-control-rounded">
                                            <option value="0" <?php if ($user->active == 0) { echo "selected";} ?> >Inactive</option>
                                            <option value="1" <?php if ($user->active == 1) { echo "selected";} ?>> Active</option>

                                        </select>
                                    </div>
                              </div>
                            <div class="form-group row 4 box">
                                    
                                    <div class=" col-md-6 ">
                                      <label for="" class="col-form-label" >Region</label>
                                      <select class="form-control" name="region" id="region">
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_r}}" >  Région actuelle : {{$dataLoc->nom_r}}</option>
                                        @endforeach
                                          <?php $reg=Region::all(); ?>

                                          @foreach($reg as $dataReg)
                                            <option value="{{$dataReg->id}}">
                                              {{$dataReg->nom}}
                                            </option>
                                          @endforeach
                                      </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Departement</label>
                                      <select class="form-control" name="departement" id="departement" >
                                         
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_d}}">  Departement actuel : {{$dataLoc->nom_d}}</option>
                                        @endforeach
                                    </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Commune</label>
                                      <select class="form-control" name="commune" id="commune" >
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_c}}">  Commune actuelle : {{$dataLoc->nom_c}} </option>
                                        @endforeach
                                      </select>
                                    </div>

                                    <div class="col-md-6">
                                      <label for="" class="col-form-label">Localité</label>
                                      <select class="form-control" name="localite" id="localite" >
                                        
                                        @foreach($localisation as $dataLoc)
                                          <option value="{{$dataLoc->id_l}}">  Localité actuelle : {{$dataLoc->nom_l}} </option>
                                        @endforeach
                                    </select>
                                    </div>
                                    

                                    

                                    
                                    
                            </div>
                            <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="" class="col-form-label">Login </label>
                                        <input type="text" name="login" class="form-control" value="{{$user->username}}" required/>
                                    </div>
                                    
                                    
                            </div>

                            <a href="/admin/utilisateur" class="btn btn-danger" data-dismiss="modal">retour à la liste</a>
                            <button type="submit" class="btn btn-primary">Enregistrer</button>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/acat.js')}}"></script>
    <script src="{{asset('assets/js/choix.js')}}"></script>
@endsection