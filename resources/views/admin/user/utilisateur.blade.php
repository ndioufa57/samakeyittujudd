@extends('layouts.master')

@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection
@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Les utilisateurs'])


    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form method="post" action="/admin/ajoutUtilisateur" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
                @csrf

                <div class="card ">
                  <div class="card-header card-header-primary">
                    <h4 class="card-title">Ajout d'un utilisateur</h4>
                  </div>
                  <div class="card-body ">
                    <div class="form-group row 2 ">
                            <div class="col-md-6 " id="prenom">
                                <label for="" class="col-form-label">Prénom</label>
                                <input type="text" name="prenom" class="form-control" />
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Nom</label>
                                <input type="text" name="nom" class="form-control" required/>
                            </div>
                    </div>
                    <div class="form-group row">

                            
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Telephone</label>
                                <input type="number" name="telephone" class="form-control" required/>
                            </div>

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Email</label>
                                <input type="text" name="email" class="form-control" required/>
                            </div>

                            
                    </div>

                    <div class="form-group row">
                            
                    
                    
                    
                            
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Adresse</label>
                                <input type="text" name="adresse" class="form-control" required/>
                                
                            </div>

                            <div class="col-md-6 edit-type-wrapper" id="test44">
                                <label for="" class="col-form-label">Profil</label>
                                <select class="form-control" name="profil" required> 
                                  <!-- <option>--Sélectionner le profil</option> -->

                                  <?php

                                      use App\Models\Region;
                                      
                                      use App\Models\Profil;
                                      
                                      
                                      $listeProfil=Profil::all();
                                      
                                     
                                   ?>
                                    <option value="2">Admin </option>
                                    <option value="3">Déclarant </option>
                                    <option value="4">Agent d'état civil </option>
                                    <option value="5">Officier d'état civil </option>
                                </select>

                            </div>
                    </div>

                    <div class="form-group row">
                            
                    
                            <div class=" col-md-6 ">
                              <label for="" class="col-form-label">CNI</label>
                              <input type="text" name="cni" class="form-control" min="0"  />
                            </div>

                             <div class="col-md-6 form-group mb-3">
                                <label class="col-form-label">Photo</label>
                                <input name="photo" type="file" class="form-control" id="photo" placeholder="Web address">
                            </div>
                            

                      </div>
                      
                      <div class="form-group row">

                           
                            <div class="col-md-6 form-group mb-3">
                                <label class="col-form-label">Statut</label>
                                <select name="active" class="form-control ">
                                    <option value="0" >Inactive</option>
                                    <option value="1"> Active</option>

                                </select>
                            </div>
                      </div>
                    <div class="form-group row 4 box">
                            
                            <div class=" col-md-6 ">
                              <label for="" class="col-form-label" >Region</label>
                              <select class="form-control" name="region" id="region">
                                  <option value="">--Sélectionner la région</option>

                                  <?php $reg=Region::all(); ?>

                                  @foreach($reg as $dataReg)
                                    <option value="{{$dataReg->id}}">
                                      {{$dataReg->nom}}
                                    </option>
                                  @endforeach
                              </select>
                            </div>

                            <div class="col-md-6">
                              <label for="" class="col-form-label">Departement</label>
                              <select class="form-control" name="departement" id="departement" >
                                <option value="">--Sélectionnez le département</option>
                            </select>
                            </div>

                            <div class="col-md-6">
                              <label for="" class="col-form-label">Commune</label>
                              <select class="form-control" name="commune" id="commune" >
                                <option value="">--Sélectionnez la commune</option>
                              </select>
                            </div>

                            <div class="col-md-6">
                              <label for="" class="col-form-label">Localité</label>
                              <select class="form-control" name="localite" id="localite" >
                                <option value="">--Choisissez la localité</option>
                            </select>
                            </div>

                            

                            

                            
                            
                    </div>
                    <div class="form-group row">
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Login </label>
                                <input type="text" name="login" class="form-control" required/>
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Mot de passe </label>
                                <input type="password" name="password" class="form-control" required/>
                            </div>
                            
                    </div>
                  </div>
                  <div class="card-footer ml-auto mr-auto">
                    <a class="btn btn-danger" data-dismiss="modal">Annuler</a>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                  </div>
                </div>
          </form>
        </div>
      </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card text-left">
                    <div class="card-header text-right bg-transparent">
                        <!--<a href="" type="button" class="btn btn-primary btn-md m-1">
                             <i class="i-Add text-white mr-2"></i> Ajouter un administrateur</a> -->

                        <a href="#addadminprofile" >
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                             <i class="i-Add text-white mr-2"></i> Ajouter un utilisateur
                            </button>
                        </a>
                    </div>
                    <div class="card-body">
                      @if (session('success'))
                        <div class="alert alert-success" role="alert">
                          <strong>{{session('success')}}</strong>
                        </div>
                      @endif
                        <h4 class="card-title mb-3">Liste des utilisateurs</h4>

                        <div class="table-responsive">
                            <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Nom</th>
                                        <th>Statut</th>
                                        <th>Cni</th>
                                        <th>Photo</th>
                                        <th>Profil</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->prenom .' '. $user->name }}</td>

                                            <td>
                                                @if ($user->active)
                                                    <span class="badge badge-success">Active</span>
                                                @else
                                                <span class="badge badge-danger">Inactive</span>
                                                @endif
                                            </td>
                                            <td>{{ $user->cni }}</td>
                                            <td>

                                                
                                                    <img src="{{ asset('assets/images/faces/'.$user->photo) }}" class=" rounded-circle avatar-sm-table m-auto"  alt="">
                                               
                                            </td>
                                            <td>
                                                {{ $user->profil }}
                                            </td>
                                            <td>
                                                

                                               <!-- <a href="" class="text-success mr-2">
                                                    <i class="nav-icon i-Pen-2 font-weight-bold"></i> Editer
                                                </a> -->

                                                <a href="" class="text-warning mr-2"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('details-form-{{ $user->id }}').submit();">
                                                    <i class="icon-file-text"></i>Détails
                                                    <form id="details-form-{{ $user->id }}" action="{{ url('admin/details-utilisateur/'.$user->id)}}" method="get" style="display: none;">
                                                       
                                                    </form>
                                                </a>

                                                <a href="" class="text-success mr-2"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('modif-form-{{ $user->id }}').submit();">
                                                    <i class="nav-icon i-Pen-2 font-weight-bold"></i>Modifier
                                                    <form id="modif-form-{{ $user->id }}" action="{{ url('admin/modification-utilisateur/'.$user->id)}}" method="get" style="display: none;">
                                                       
                                                    </form>
                                                </a>


 
                                                

                                                

                                                @if ($user->active==0)
                                                    <a href="" class="text-warning mr-2"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('delete-form-{{ $user->id }}').submit();">
                                                    <i class="nav-icon i-Close-Window font-weight-bold"></i>Activer
                                                    <form id="delete-form-{{ $user->id }}" action="/admin/activerUser/{{$user->id}}" onsubmit="return confirm('Confirmez-vous la désactivation');" method="post" style="display: none;">
                                                        @csrf @method('post')
                                                    </form>
                                                </a>
                                                @else
                                                    <a href="" class="text-danger mr-2"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('delete-form-{{ $user->id }}').submit();">
                                                    <i class="nav-icon i-Close-Window font-weight-bold"></i>Désactiver
                                                    <form id="delete-form-{{ $user->id }}" action="/admin/deleteUser/{{$user->id}}" onsubmit="return confirm('Confirmez-vous la désactivation');" method="post" style="display: none;">
                                                        @csrf @method('post')
                                                    </form>
                                                </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script src="{{asset('assets/js/acat.js')}}"></script>
    <script src="{{asset('assets/js/choix.js')}}"></script>
@endsection

