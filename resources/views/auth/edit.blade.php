@extends('layouts.master')

@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Les utilisateurs'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach

                        @endif
                        <div class="card-title mb-3">Ajouter un administrateur</div>
                        <form method="POST" action="{{ route('admin.update',$admin) }}" enctype="multipart/form-data" >
                            @csrf
                            @method('patch')
                            {{ $admin }}
                            <div class="row">
                                <div class="col-md-6 form-group mb-3">
                                    <label for="firstname">Prénom</label>
                                    <input value="{{ $admin->firstname }}" name="firstname" type="text" class="form-control form-control-rounded" placeholder="Entrez le prénom">
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label for="lastname">Nom</label>
                                    <input value="{{ $admin->lastname }}" name="lastname" type="text" class="form-control form-control-rounded"  placeholder="Enter le  nom">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="username">Username</label>
                                    <input value="{{ $admin->username }}"  name="username" type="text" class="form-control form-control-rounded" placeholder="Entrez le username">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="email">Email address</label>
                                    <input value="{{ $admin->email }}"  name="email" type="email" class="form-control form-control-rounded" id="exampleInputEmail2"  placeholder="Enter email">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="cni">Cni</label>
                                    <input value="{{ $admin->cni }}"  type="number" name="cni" class="form-control form-control-rounded" id="cni" placeholder="Enter phone">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="active">Statut</label>
                                    <select name="active" class="form-control form-control-rounded">
                                        @if ($admin->active == 0)
                                            <option selected value="0" >Inactive</option>
                                            <option value="1"> Active</option>
                                        @else
                                            <option value="0" >Inactive</option>
                                            <option selected value="1"> Active</option>
                                        @endif


                                    </select>
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="role_id">Role</label>
                                    <select name="role_id" class="form-control form-control-rounded">
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}" >{{ $role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary">Ajouter un administrateur</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
