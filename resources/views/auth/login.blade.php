<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> LOGIN</title>
    <meta name="author" content="Codeconvey" />

    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Stylish Login Page CSS -->
    <link rel="stylesheet" href="{{ asset('css/login-page.css') }}">

    <!--Only for demo purpose - no need to add.-->
    <link rel="stylesheet" href="{{ asset('css/demo.css') }}" />

</head>
<body>

    <header class="ScriptHeader">
        <div class="rt-container">
            <div class="col-rt-12">
                <div class="rt-heading">
                    <h1 class="rt"></h1>
                </div>
            </div>
        </div>
    </header>

    <section>
        <div class="rt-container">
            <div class="col-rt-12">
                <div class="Scriptcontent">

                <!-- Stylish Login Page Start -->
                <form class="codehim-form" method="POST" action="{{ route('login') }}">
                    @csrf
                        <div class="form-title">
                            <div class="user-icon gr-bg">
                            <i class="fa fa-user"></i>
                        </div>
                    <h2> Login</h2>
                    </div>
                    <label for="email"><i class="fa fa-envelope"></i> Email:</label>
                    <input type="email" id="email" class="cm-input" name="email" value="{{ old('email') }}"
                    required autocomplete="email"  placeholder="Enter your email adress">

                    <label for="pass"><i class="fa fa-lock"></i> Password:</label>
                    <input id="pass" type="password" class="cm-input" placeholder="Enter your password"
                    name="password" required autocomplete="current-password">
                    <button type="submit" class="btn-login  gr-bg">Login</button>
                </form>
                </div>
            </div>
        </div>
    </section>



    <!-- Analytics -->

	</body>
</html>
