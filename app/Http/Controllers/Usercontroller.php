<?php

namespace App\Http\Controllers;

use App\Models\Mairie;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class Usercontroller extends Controller
{
    public function index(){
    	$users=DB::select('select u.prenom,u.name,u.id,u.adresse,u.email,u.active,u.photo,u.cni,p.profil from users u , profil p   WHERE u.profil=p.id AND u.profil !=1' );
    	$mairies=Mairie::all();

		return view('admin.user.utilisateur',compact('users','mairies'));
    }

    public function store (Request $request){

        $user=new User;
       
        
        $user->name=$request->input('nom');
        $user->prenom=$request->input('prenom');
        
        $user->telephone=$request->input('telephone');
        //$user->mairie=$request->input('mairie');
        $user->email=$request->input('email');
        $user->adresse=$request->input('adresse');
        $user->profil=$request->input('profil');
        $user->region=$request->input('region');
        $user->departement=$request->input('departement');
        $user->commune=$request->input('commune');
        $user->localite=$request->input('localite');
        $user->cni=$request->input('cni');
        $user->active=$request->input('active');
        $user->username=$request->input('login');
        $user->password=Hash::make($request->input('password'));


        $data = $request->input('photo');
		   	$file = $request->file('photo');
            $destinationPath = 'assets/images/faces/';
            //$originalFile = $file->getClientOriginalName();
            $filename=$imageName = time() .'.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
		        
    	$user->photo=$filename;



        //try {
          $user->save();
          
           // return redirect('/admin/utilisateur')->with('success','Utilisateur ajouté avec succès');
        //} catch (\Exception $e) {
          
           // return redirect('/admin/utilisateur')->with('status','Utilisateur non ajouté .Vérifiez si vous avez bien rentré les données.');

        //}

       return redirect('/admin/utilisateur')->with('success','Utilisateur ajouté avec succès');
     }


    public function update(Request $request ,$id){
   
        $user= User::findOrFail($id);
        $mairies=Mairie::all();

        $localisation=DB::select('select r.id as id_r,r.nom as nom_r,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from profil t , users u ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c WHERE 1 AND t.id=u.profil AND u.id="'.$id.'" AND u.region=r.id AND u.departement=d.id AND u.localite=l.id AND u.commune=c.id');
        
        return view('admin.user.updateUtilisateur',compact('user','mairies','localisation'));
        
    }



    public function updateSaving(Request $request ,$id){
   
   		$user= User::find($id);
        $user->name=$request->input('nom');
        $user->prenom=$request->input('prenom');
        
        $user->telephone=$request->input('telephone');
        //$user->mairie=$request->input('mairie');
        $user->email=$request->input('email');
        $user->adresse=$request->input('adresse');
        $user->profil=$request->input('profil');
        $user->region=$request->input('region');
        $user->departement=$request->input('departement');
        $user->commune=$request->input('commune');
        $user->localite=$request->input('localite');
        $user->cni=$request->input('cni');
        $user->active=$request->input('active');
        $user->username=$request->input('login');
        

        if (!empty($request->file('photo'))) {


        	$data = $request->input('photo');
		   	$file = $request->file('photo');
            $destinationPath = 'assets/images/faces/';
            //$originalFile = $file->getClientOriginalName();
            $filename=$imageName = time() .'.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
		        
    		$user->photo=$filename;
        }

        

        $user->update();

        return redirect('/admin/utilisateur')->with('success','Utilisateur modifié avec succès');

    }

    public function details(Request $request ,$id){
   
        $user= User::findOrFail($id);
        $mairies=Mairie::all();

        $localisation=DB::select('select r.id as id_r,r.nom as nom_r,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from profil t , users u ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c WHERE 1 AND t.id=u.profil AND u.id="'.$id.'" AND u.region=r.id AND u.departement=d.id AND u.localite=l.id AND u.commune=c.id');
        
        return view('admin.user.detailsUtilisateur',compact('user','mairies','localisation'));
        
    }


    public function delete(Request $request ,$id){
   
        $user= User::findOrFail($id);
        $user->active=0;
        $user->update();

        return Redirect::back()->with('success','Utilisateur désactivé  avec succès');
    }

    public function activer(Request $request ,$id){
   
        $user= User::findOrFail($id);
        $user->active=1;
        $user->update();

        return Redirect::back()->with('success','Utilisateur activé  avec succès');
    }
}
