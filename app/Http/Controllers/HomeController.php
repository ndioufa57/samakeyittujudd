<?php
namespace App\Http\Controllers;

use App\Models\Children;
use App\Models\Declaration;
use App\Models\Mairie;
use App\Models\Region;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    use AuthorizesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    //     $this->middleware('role:super', ['only'=>'show']);
    //     $this->adminModel = config('multiauth.models.admin');
    // }

    public function index()
    {
        $childrens = Children::inRandomOrder();
        $admins = User::inRandomOrder();
        $regions = Region::inRandomOrder();
        //$mairies = Mairie::inRandomOrder();

        $nombreEnfants=Declaration::all()->count();
        $nombreUtilisateurs=User::all()->count();

        $declaration=Declaration::all();
        $mairies=Mairie::all();
        return view('admin.home',compact('admins','declaration','regions','mairies','nombreEnfants','nombreUtilisateurs'));
    }

    public function show()
    {
        $admins = Admin::where('id', '!=', auth()->id())->get();

        return view('auth.show', compact('admins'));
    }

    public function showChangePasswordForm()
    {
        return view('auth.passwords.change');
    }

    public function changePassword(Request $request)
    {
        $data = $request->validate([
            'oldPassword'   => 'required',
            'password'      => 'required|confirmed',
        ]);
        auth()->user()->update(['password' => bcrypt($data['password'])]);

        return redirect(route('admin.home'))->with('message', 'Your password is changed successfully');
    }
}
