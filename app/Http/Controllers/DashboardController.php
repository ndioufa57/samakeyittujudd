<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
     use AuthorizesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    //     $this->middleware('role:super', ['only'=>'show']);
    //     $this->adminModel = config('multiauth.models.admin');
    // }

    public function index()
    {
        $childrens = Children::all();
        $admins = Admin::all();
        $regions = Region::all();
        $mairies = Mairie::all();
        return view('admin.home',compact('admins','childrens','regions','mairies'));
    }

    public function show()
    {
        $admins = Admin::where('id', '!=', auth()->id())->get();

        return view('auth.show', compact('admins'));
    }

    public function showChangePasswordForm()
    {
        return view('auth.passwords.change');
    }

    public function changePassword(Request $request)
    {
        $data = $request->validate([
            'oldPassword'   => 'required',
            'password'      => 'required|confirmed',
        ]);
        auth()->user()->update(['password' => bcrypt($data['password'])]);

        return redirect(route('admin.home'))->with('message', 'Your password is changed successfully');
    }
}
