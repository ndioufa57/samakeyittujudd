<?php

namespace App\Http\Controllers;

use App\Models\Declaration;
use App\Models\Mailer;
use App\Models\Mairie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DeclarationController extends Controller
{
    public function index(){
    	// $users=DB::select('select u.prenom,u.name,u.id,u.adresse,u.email,u.active,u.photo,u.cni,p.profil from users u , profil p   WHERE u.profil=p.id AND u.profil !=1' );
    	$declaration=Declaration::all();
    	$mairies=Mairie::all();

    	

		

        if (Auth::user()->profil=='1' Or Auth::user()->profil=='2'  ) {

           return view('admin.declaration.declaration',compact('declaration','mairies'));

        }elseif(Auth::user()->profil=='3'){

         return view('declarant.declaration',compact('declaration','mairies'));

        }elseif(Auth::user()->profil=='4'){

         return view('agent.declaration',compact('declaration','mairies'));

        }elseif(Auth::user()->profil=='5'){

         return view('officier.declaration',compact('declaration','mairies'));
        }
    }

    public function store (Request $request){

        $declaration=new Declaration;
       
        
        $declaration->registre=$request->input('registre');
        $declaration->prenom=$request->input('prenom');
        
        $declaration->nom=$request->input('nom');
        $declaration->genre=$request->input('genre');
        $declaration->region=$request->input('region');
        $declaration->departement=$request->input('departement');
        $declaration->commune=$request->input('commune');
        $declaration->localite=$request->input('localite');
        $declaration->hopital=$request->input('hopital');
        $declaration->prenom_pere=$request->input('prenom_pere');
        $declaration->nom_pere=$request->input('nom_pere');

        $declaration->prenom_mere=$request->input('prenom_mere');
        $declaration->nom_mere=$request->input('nom_mere');
        // $declaration->mairie=$request->input('mairie');

        $declaration->cni_mere=$request->input('cni_mere');
        $declaration->cni_pere=$request->input('cni_pere');
        $declaration->telephone_mere=$request->input('telephone_mere');

        $declaration->district=$request->input('district');


        

    if (!empty($request->file('certificat'))) {

        $data = $request->input('certificat');
		   	$file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename=$imageName = time() .'.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
		        
    	$declaration->certificat=$filename;

    }


    	



        //try {
          $declaration->save();
          
           // return redirect('/admin/utilisateur')->with('success','Utilisateur ajouté avec succès');
        //} catch (\Exception $e) {
          
           // return redirect('/admin/utilisateur')->with('status','Utilisateur non ajouté .Vérifiez si vous avez bien rentré les données.');

        //}

       return redirect('/admin/declaration')->with('success','Utilisateur ajouté avec succès');
     }


     public function storeDeclarant (Request $request){

        $declaration=new Declaration;
       
        $declaration->genre=$request->input('genre');
        $declaration->region=$request->input('region');
        $declaration->departement=$request->input('departement');
        $declaration->commune=$request->input('commune');
        $declaration->localite=$request->input('localite');
        $declaration->hopital=$request->input('hopital');
        $declaration->prenom_pere=$request->input('prenom_pere');
        $declaration->nom_pere=$request->input('nom_pere');

        $declaration->prenom_mere=$request->input('prenom_mere');
        $declaration->nom_mere=$request->input('nom_mere');
        // $declaration->mairie=$request->input('mairie');

        $declaration->cni_mere=$request->input('cni_mere');
        $declaration->cni_pere=$request->input('cni_pere');
        $declaration->telephone_mere=$request->input('telephone_mere');

       // $declaration->district=$request->input('district');


        


        $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename=$imageName = time() .'.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
                
        $declaration->certificat=$filename;
          $declaration->save();



        //   $emails=DB::select('select email from users where  profil=4 AND  departement="'.$request->input('departement').'"');

        //   $lemessage='Bonjour vous avez la déclaration de "'.$request->input('prenom_mere').'" '.  $request->input('nom') .'" ('.  $request->input('cni_mere') .'" ) à traiter.'

        

        // // foreach ($emails as $data) {
        //      Mail::send('emails.contact',
        //      array(
                
                 
        //          'objet' => "Déclaration à remplir",
        //          'msg' => $lemessage,
        //      ), function($message) use ($request)
        //        {
        //           $message->from('contact@ehodcorp.com');
        //           $message->subject($request->objet);
                  
        //           $emails=DB::select('select email from annuaire where domaine="'.$request->input('domaine').'"');

        //           foreach ($emails as $data) {

        //             $message->to($data->email);

        //           }
        //        });

        // $mail=new Mailer;
        // $mail->objet="Déclaration à remplir";
        // $mail->message=$lemessage;
        // $mail->save();
         

       return redirect('/declarant/declaration')->with('success','Utilisateur ajouté avec succès');
     }


     public function update(Request $request ,$id){
   
        $declaration= Declaration::findOrFail($id);
        $mairies=Mairie::all();

        $mairieActuel=DB::table('mairies')->where('id', $declaration->id)->first();

        $localisation=DB::select('select r.id as id_r,r.nom as nom_r,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from declarations dp ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c WHERE 1  AND dp.region=r.id AND dp.departement=d.id AND dp.localite=l.id AND dp.commune=c.id');
        
        


        if (Auth::user()->profil=='1' Or Auth::user()->profil=='2'  ) {

           return view('admin.declaration.updateDeclaration',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='3'){

         return view('declarant.updateDeclaration',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='4'){

         return view('agent.updateDeclaration',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='5'){

         return view('officier.updateDeclaration',compact('declaration','mairies','localisation','mairieActuel'));
        }


        
    }


    public function updateSaving(Request $request ,$id){
   
        $declaration= Declaration::find($id);
        
       
        
        $declaration->registre=$request->input('registre');
        $declaration->prenom=$request->input('prenom');
        
        $declaration->nom=$request->input('nom');
        $declaration->genre=$request->input('genre');
        $declaration->region=$request->input('region');
        $declaration->departement=$request->input('departement');
        $declaration->commune=$request->input('commune');
        $declaration->localite=$request->input('localite');
        $declaration->hopital=$request->input('hopital');
        $declaration->prenom_pere=$request->input('prenom_pere');
        $declaration->nom_pere=$request->input('nom_pere');

        $declaration->prenom_mere=$request->input('prenom_mere');
        $declaration->nom_mere=$request->input('nom_mere');
        // $declaration->mairie=$request->input('mairie');

        $declaration->cni_mere=$request->input('cni_mere');
        $declaration->cni_pere=$request->input('cni_pere');
        $declaration->telephone_mere=$request->input('telephone_mere');
        $declaration->statut=$request->input('statut');

        $declaration->district=$request->input('district');


        
    if (!empty($request->file('certificat'))) {

        $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename=$imageName = time() .'.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
                
        $declaration->certificat=$filename;

    }

        $declaration->update();

        return redirect('/admin/declaration')->with('success','Déclaration mise à jour  avec succès');

    }



    public function updateSavingDeclarant(Request $request ,$id){
   
        $declaration= Declaration::find($id);
        
       
        
        $declaration->genre=$request->input('genre');
        $declaration->region=$request->input('region');
        $declaration->departement=$request->input('departement');
        $declaration->commune=$request->input('commune');
        $declaration->localite=$request->input('localite');
        $declaration->hopital=$request->input('hopital');
        $declaration->prenom_pere=$request->input('prenom_pere');
        $declaration->nom_pere=$request->input('nom_pere');

        $declaration->prenom_mere=$request->input('prenom_mere');
        $declaration->nom_mere=$request->input('nom_mere');
        // $declaration->mairie=$request->input('mairie');

        $declaration->cni_mere=$request->input('cni_mere');
        $declaration->cni_pere=$request->input('cni_pere');
        $declaration->telephone_mere=$request->input('telephone_mere');


        
    if (!empty($request->file('certificat'))) {

        $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename=$imageName = time() .'.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
                
        $declaration->certificat=$filename;

    }

        $declaration->update();

        return redirect('/declarant/declaration')->with('success','Déclaration mise à jour  avec succès');

    }


    public function updateSavingAgent(Request $request ,$id){
   
        $declaration= Declaration::find($id);
        
       
        
        $declaration->registre=$request->input('registre');
        $declaration->prenom=$request->input('prenom');
        $declaration->nom=$request->input('nom');
        $declaration->delivrance=$request->input('delivrance');
        $declaration->statut=$request->input('statut');
        $declaration->district=$request->input('district');
        $declaration->agent_responsable=Auth::user()->id;

    

        $declaration->update();

        return redirect('/agent/declaration')->with('success','Déclaration mise à jour  avec succès');

    }




    public function updateSavingOfficier(Request $request ,$id){
   
        $declaration= Declaration::find($id);
        
       
        
        $declaration->registre=$request->input('registre');
        $declaration->prenom=$request->input('prenom');
        
        $declaration->nom=$request->input('nom');
        $declaration->genre=$request->input('genre');
        $declaration->region=$request->input('region');
        $declaration->departement=$request->input('departement');
        $declaration->commune=$request->input('commune');
        $declaration->localite=$request->input('localite');
        $declaration->hopital=$request->input('hopital');
        $declaration->prenom_pere=$request->input('prenom_pere');
        $declaration->nom_pere=$request->input('nom_pere');

        $declaration->prenom_mere=$request->input('prenom_mere');
        $declaration->nom_mere=$request->input('nom_mere');
        // $declaration->mairie=$request->input('mairie');

        $declaration->cni_mere=$request->input('cni_mere');
        $declaration->cni_pere=$request->input('cni_pere');
        $declaration->telephone_mere=$request->input('telephone_mere');
        $declaration->statut=$request->input('statut');

        $declaration->district=$request->input('district');


        
    if (!empty($request->file('certificat'))) {

        $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename=$imageName = time() .'.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
                
        $declaration->certificat=$filename;

    }

        $declaration->update();

        return redirect('/officier/declaration')->with('success','Déclaration mise à jour  avec succès');

    }




    public function details(Request $request ,$id){
   
        $declaration= Declaration::findOrFail($id);
        $mairies=Mairie::all();

        $mairieActuel=DB::table('mairies')->where('id', $declaration->id)->first();

        $localisation=DB::select('select r.id as id_r,r.nom as nom_r,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from declarations dp ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c WHERE 1  AND dp.region=r.id AND dp.departement=d.id AND dp.localite=l.id AND dp.commune=c.id');

        if (Auth::user()->profil=='1' Or Auth::user()->profil=='2'  ) {

           return view('admin.declaration.detailsDeclaration',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='3'){

         return view('declarant.detailsDeclaration',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='4'){

         return view('agent.detailsDeclaration',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='5'){

         return view('officier.detailsDeclaration',compact('declaration','mairies','localisation','mairieActuel'));
        }
        
        
        
    }


    public function pdf(Request $request ,$id){
   
        $declaration= Declaration::findOrFail($id);
        $mairies=Mairie::all();

        $mairieActuel=DB::table('mairies')->where('id', $declaration->id)->first();

        $localisation=DB::select('select r.id as id_r,r.nom as nom_r,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from declarations dp ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c WHERE 1  AND dp.region=r.id AND dp.departement=d.id AND dp.localite=l.id AND dp.commune=c.id');

        if (Auth::user()->profil=='1' Or Auth::user()->profil=='2'  ) {

           return view('pdf',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='3'){

         return view('declarant.detailsDeclaration',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='4'){

         return view('agent.detailsDeclaration',compact('declaration','mairies','localisation','mairieActuel'));

        }elseif(Auth::user()->profil=='5'){

         return view('pdf',compact('declaration','mairies','localisation','mairieActuel'));
        }
        
        
        
    }
}
